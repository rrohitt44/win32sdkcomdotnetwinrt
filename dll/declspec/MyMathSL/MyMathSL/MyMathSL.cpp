// MyMathSL.cpp : Defines the functions for the static library.
//
#include<Windows.h>
#include"MyMathSL.h"

BOOL WINAPI DllMain(HMODULE hDll, DWORD dwReason, LPVOID lpReserved)
{
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:

		break;

	case DLL_THREAD_ATTACH:

		break;

	case DLL_THREAD_DETACH:

		break;

	case DLL_PROCESS_DETACH:

		break;
	}

	return TRUE;
}



extern "C" __declspec(dllexport) int MakeSquare(int num)
{
	return (num * num);
}
