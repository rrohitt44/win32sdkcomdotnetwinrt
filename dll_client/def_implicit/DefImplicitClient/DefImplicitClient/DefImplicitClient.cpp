// headers
#include<Windows.h>
#include"resources.h"
#include"MyMathDef.h"

// link library by code way - implicitly; you can also add this by proj prop -> linker -> commandline and put lib file name there
//#pragma comment(lib, "MYMATHDL.lib")
// For this project, this library is added in the command line option

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("MyApp");
	HWND hwnd; // handle to window
	MSG msg; // message

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // callback function
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, TEXT("MYICON"));
	wndclass.hCursor = LoadCursor(hInstance, TEXT("CUSTCURSOR"));
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, TEXT("CUSTICONSM"));

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(
		szAppName, // lpClassName
		TEXT("My Application"), // lpWindowName
		WS_OVERLAPPEDWINDOW, // dwStyle
		CW_USEDEFAULT, // x
		CW_USEDEFAULT, // y
		CW_USEDEFAULT, // width
		CW_USEDEFAULT, // height
		NULL, // hWndParent
		NULL, // hMenu
		hInstance,
		NULL // lpParam
	);

	// show window
	ShowWindow(hwnd, iCmdShow);

	// update window
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return ((int)msg.wParam);
}

// WndProc: callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// local variable declarations
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rc;
	TCHAR str[255];
	int i, j;
	i = 40;

	// code
	switch (iMsg)
	{
	case WM_CREATE: // on window creation; entered only once and at the first time only here
		// call dll function makeSquare
		j = MakeSquare(i);
		wsprintf(str, TEXT("square of %d is %d."), i, j);
		MessageBox(hwnd, str, TEXT("RESULT"), MB_OK | MB_ICONINFORMATION);
		DestroyWindow(hwnd);
		break;
	case WM_LBUTTONDOWN: // mouse left click

		break;
	case WM_PAINT: // to draw anything
	{
		// start drawing
		GetClientRect(hwnd, &rc); // get client area
		hdc = BeginPaint(hwnd, &ps);
		SetTextColor(hdc, RGB(255, 128, 0)); // orange
		SetBkColor(hdc, RGB(255, 255, 255)); // gray
		//DrawText(hdc, str, -1, &rc, DT_SINGLELINE | DT_CENTER | DT_VCENTER); // Draw text
		// end painting
		EndPaint(hwnd, &ps);
	}
	break;
	case WM_KEYDOWN: // on any key down
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE: // on escape destroy the window
			DestroyWindow(hwnd);
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}