#pragma once

#include<Windows.h>

class IMultiplication :public IUnknown
{
public:
	// IMultiplication specific method declarations pure virtual
	virtual HRESULT __stdcall MultiplicationOfTwoIntegers(int, int, int*) = 0;
};

class IDivision : public IUnknown
{
public:
	// IDivision specific method declarations
	virtual HRESULT __stdcall DivisionOfTwoIntegers(int, int, int*) = 0;
};


// CLSID's
// CLSID of MultiplicationDivision component - {29942F1B-EAE8-4274-94DF-9DA1F809A1BE}
const CLSID CLSID_MultiplicationDivision =
{ 0x29942f1b, 0xeae8, 0x4274, 0x94, 0xdf, 0x9d, 0xa1, 0xf8, 0x9, 0xa1, 0xbe };

// IID of IMultiplication interface - {4BEA2941-3DEE-4AAC-AC84-06EAC5CE1D2B}
const IID IID_IMultiplication =
{ 0x4bea2941, 0x3dee, 0x4aac, 0xac, 0x84, 0x6, 0xea, 0xc5, 0xce, 0x1d, 0x2b };

// IID of IDivision interface - {4BE80CBB-5091-46AA-90E4-D1C087DC185B}
const IID IID_IDivision =
{ 0x4be80cbb, 0x5091, 0x46aa, 0x90, 0xe4, 0xd1, 0xc0, 0x87, 0xdc, 0x18, 0x5b };


