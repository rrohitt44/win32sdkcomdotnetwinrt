#pragma once

#include<Windows.h>

class ISum : public IUnknown
{
public:
	// ISum specific method declarations
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int*) = 0; // pure virtual function
};

class ISubtract : public IUnknown
{
public:
	// ISubtract specific method declarations
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int*) = 0; // pure virtual function
};

// CLSID of SumSubtract Component - {EAC86BF7-E16A-44B4-89C8-5D1B971D2869}
const CLSID CLSID_SumSubtract = { 0xeac86bf7, 0xe16a, 0x44b4, 0x89, 0xc8, 0x5d, 0x1b, 0x97, 0x1d, 0x28, 0x69 };

// IID of Isum Inteface - {29F867E9-F01A-4D26-8C32-F8F6895ED4E1}
const IID IID_ISum = { 0x29f867e9, 0xf01a, 0x4d26, 0x8c, 0x32, 0xf8, 0xf6, 0x89, 0x5e, 0xd4, 0xe1 };

// IID of ISubtract Interface - {8AC768DC-FCBD-49C5-BBF7-71B6903AC918}
const IID IID_ISubtract = { 0x8ac768dc, 0xfcbd, 0x49c5, 0xbb, 0xf7, 0x71, 0xb6, 0x90, 0x3a, 0xc9, 0x18 };


