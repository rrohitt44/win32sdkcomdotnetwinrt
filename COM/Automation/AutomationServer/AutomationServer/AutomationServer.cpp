// headers
#include<Windows.h>
#include<stdio.h> // for swprintf_s()
#include"AutomationServer.h"


// class declarations
class CMyMath :public IMyMath
{
private:
	long m_cRef;
	ITypeInfo* m_pITypeInfo=NULL;
public:
	// constructor
	CMyMath(void);
	// destructor
	~CMyMath(void);

	// IUnknown specific method declarations (inherited)
	HRESULT __stdcall QueryInterface(REFIID, void**);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);

	// IDispatch specific method declarations (inherited)
	HRESULT __stdcall GetTypeInfoCount(UINT*);
	HRESULT __stdcall GetTypeInfo(UINT, LCID, ITypeInfo**);
	HRESULT __stdcall GetIDsOfNames(REFIID, LPOLESTR*, UINT, LCID, DISPID*);
	HRESULT __stdcall Invoke(DISPID, REFIID, LCID, WORD, DISPPARAMS*, VARIANT*, EXCEPINFO*, UINT*);

	// IMultiplication specific method declarations (inherited)
	HRESULT __stdcall SumOfTwoIntegers(int, int, int*);

	// IDivision specific method declarations (inherited)
	HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int*);

	// custom methods
	HRESULT InitInstance(void);
};


class CMyMathClassFactory :public IClassFactory
{
private:
	long m_cRef;
public:
	// constructor
	CMyMathClassFactory(void);
	// destructor
	~CMyMathClassFactory(void);

	// IUnknown specific method declarations (inherited)
	HRESULT __stdcall QueryInterface(REFIID, void**);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);

	// IClassFactory specific method declarations (inherited)
	HRESULT __stdcall CreateInstance(IUnknown*, REFIID, void**);
	HRESULT __stdcall LockServer(BOOL);
};

// global DLL handle
HMODULE ghModule = NULL;

// global variable declarations
long glNumberOfActiveComponents = 0; // number of active components
long glNumberOfServerLocks = 0; // number of locls on this dll

// {B0CCD8F6-8B8E-441B-BFA8-4A30B23904DB}
const GUID LIBID_AutomationServer = { 0xb0ccd8f6, 0x8b8e, 0x441b, 0xbf, 0xa8, 0x4a, 0x30, 0xb2, 0x39, 0x4, 0xdb };

// DllMain
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD dwReason, LPVOID Reserved)
{
	switch (dwReason)
	{
	case DLL_PROCESS_ATTACH:

		break;

	case DLL_PROCESS_DETACH:

		break;
	}

	return(TRUE);
}

// implementation of CMyMath's constructor
CMyMath::CMyMath(void)
{
	m_pITypeInfo = NULL;
	m_cRef = 1; // hardcoded initialization to anticipate possible failure of QueryInterface
	InterlockedIncrement(&glNumberOfActiveComponents); // increment global counter
}


// implementation of CMyMath's destructor
CMyMath::~CMyMath(void)
{
	InterlockedDecrement(&glNumberOfActiveComponents); // decrement global counter
}


// implementation of CMyMath's IUnknown's methods
HRESULT CMyMath::QueryInterface(REFIID riid, void** ppv)
{
	if (riid == IID_IUnknown)
	{
		*ppv = static_cast<IMyMath*>(this);
	}
	else if (riid == IID_IDispatch)
	{
		*ppv = static_cast<IMyMath*>(this);
	}
	else if (riid == IID_IMyMath)
	{
		*ppv = static_cast<IMyMath*>(this);
	}
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}

	reinterpret_cast<IUnknown*>(*ppv)->AddRef();

	return(S_OK);
}

ULONG CMyMath::AddRef(void)
{
	InterlockedIncrement(&m_cRef);
	return(m_cRef);
}

ULONG CMyMath::Release(void)
{
	InterlockedDecrement(&m_cRef);

	if (m_cRef == 0)
	{
		m_pITypeInfo->Release();
		m_pITypeInfo = NULL;
		delete(this);
		return(0);
	}
	return(m_cRef);
}

// implementation of CMyMath's methods
HRESULT CMyMath::SumOfTwoIntegers(int num1, int num2, int* pSum)
{
	*pSum = num1 + num2;
	return(S_OK);
}


HRESULT CMyMath::SubtractionOfTwoIntegers(int num1, int num2, int* pSubtraction)
{
	*pSubtraction = num1 - num2;
	return(S_OK);
}

HRESULT CMyMath::InitInstance(void)
{
	// function declarations
	void ComErrorDescriptionString(HWND, HRESULT);

	// variable declarations
	HRESULT hr;
	ITypeLib* pITypeLib = NULL;

	// code
	if (m_pITypeInfo == NULL)
	{
		hr = LoadRegTypeLib(LIBID_AutomationServer,
			1, 0,
			0x00,
			&pITypeLib);

		if (FAILED(hr))
		{
			ComErrorDescriptionString(NULL, hr);
			return(hr);
		}

		hr = pITypeLib->GetTypeInfoOfGuid(IID_IMyMath, &m_pITypeInfo);

		if (FAILED(hr))
		{
			ComErrorDescriptionString(NULL, hr);
			pITypeLib->Release();
			return(hr);
		}

		pITypeLib->Release();
	}
	return(S_OK);
}

// implementation of CMyMathClassFactory's constructor
CMyMathClassFactory::CMyMathClassFactory(void)
{
	m_cRef = 1; // hardcoded initialization to anticipate possible failure of QueryInterface
}

// implementation of CMyMathClassFactory's destructor
CMyMathClassFactory::~CMyMathClassFactory(void)
{
	// no code
}

// implementation of CMyMathClassFactory's IClassFactory's IUnknown's methods
HRESULT CMyMathClassFactory::QueryInterface(REFIID riid, void** ppv)
{
	if (riid == IID_IUnknown)
	{
		*ppv = static_cast<IClassFactory*>(this);
	}
	else if (riid == IID_IClassFactory)
	{
		*ppv = static_cast<IClassFactory*>(this);
	}
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}

	reinterpret_cast<IUnknown*>(*ppv)->AddRef();
	return(S_OK);
}

ULONG CMyMathClassFactory::AddRef(void)
{
	InterlockedIncrement(&m_cRef);
	return(m_cRef);
}

ULONG CMyMathClassFactory::Release(void)
{
	InterlockedDecrement(&m_cRef);

	if (m_cRef == 0)
	{
		delete(this);
		return(0);
	}
	return(m_cRef);
}

// implementation of CMyMathClassFactory's IClassFactory's methods
HRESULT CMyMathClassFactory::CreateInstance(IUnknown* pUnkOuter, REFIID riid, void** ppv)
{
	// variable declarations
	CMyMath* pCMyMath = NULL;
	HRESULT hr;

	if ((pUnkOuter != NULL) && (riid != IID_IUnknown))
	{
		return(CLASS_E_NOAGGREGATION);
	}

	// create instance of co class
	pCMyMath = new CMyMath();

	if (pCMyMath == NULL)
	{
		return(E_OUTOFMEMORY);
	}

	hr = pCMyMath->InitInstance();

	// get requested interface
	hr = pCMyMath->QueryInterface(riid, ppv);

	pCMyMath->Release(); // anticipate possible failure of QueryInterface()
	return(hr);
}

HRESULT CMyMathClassFactory::LockServer(BOOL fLock)
{
	if (fLock)
	{
		InterlockedIncrement(&glNumberOfServerLocks);
	}
	else
	{
		InterlockedDecrement(&glNumberOfServerLocks);
	}

	return(S_OK);
}

// implementation of IDispatch's methods
HRESULT CMyMath::GetTypeInfoCount(UINT* pCountTypeInfo)
{
	// as we have type library it is 1, else 0
	*pCountTypeInfo = 1;

	return(S_OK);
}

HRESULT CMyMath::GetTypeInfo(UINT iTypeInfo, LCID lcid, ITypeInfo** ppITypeInfo)
{
	*ppITypeInfo = NULL;

	if (iTypeInfo != 0)
	{
		return(DISP_E_BADINDEX);
	}

	m_pITypeInfo->AddRef();

	*ppITypeInfo = m_pITypeInfo;

	return(S_OK);
}


HRESULT CMyMath::GetIDsOfNames(REFIID riid, LPOLESTR* rgszNames, UINT cNames, LCID lcid, DISPID* rgDispId)
{
	return(DispGetIDsOfNames(m_pITypeInfo, rgszNames, cNames, rgDispId));
}

HRESULT CMyMath::Invoke(DISPID dispIdMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS* pDispParams,
	VARIANT* pVarResult, EXCEPINFO* pExcepInfo, UINT* puArgErr)
{
	// variable declarations
	HRESULT hr;

	hr = DispInvoke(this, m_pITypeInfo, dispIdMember,
		wFlags, pDispParams, pVarResult, pExcepInfo, puArgErr);

	return(hr);
}
// implementation of exported functions from this Dll
extern "C" HRESULT __stdcall DllGetClassObject(REFCLSID rclsid, REFIID riid, void** ppv)
{
	// variable declarations
	CMyMathClassFactory* pCMyMathClassFactory = NULL;
	HRESULT hr;

	// code
	if (rclsid != CLSID_MyMath)
	{
		return(CLASS_E_CLASSNOTAVAILABLE);
	}

	// create class factory
	pCMyMathClassFactory = new CMyMathClassFactory;

	if (pCMyMathClassFactory == NULL)
	{
		return(E_OUTOFMEMORY);
	}

	hr = pCMyMathClassFactory->QueryInterface(riid, ppv);

	// anticipate possible failure of QueryInterface()
	pCMyMathClassFactory->Release();
	return(hr);
}

extern "C" HRESULT __stdcall DllCanUnloadNow(void)
{
	if ((glNumberOfActiveComponents == 0) * (glNumberOfServerLocks == 0))
	{
		return(S_OK);
	}
	else
	{
		return(S_FALSE);
	}
}

void ComErrorDescriptionString(HWND hwnd, HRESULT hr)
{
	// variable declarations
	TCHAR* szErrorMessage = NULL;
	TCHAR str[255];

	if (FACILITY_WINDOWS == HRESULT_FACILITY(hr))
	{
		hr = HRESULT_CODE(hr);

		if (FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
			NULL, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR)&szErrorMessage, 0, NULL) != 0)
		{
			//swprintf_s(str, TEXT("%#x : %s"), hr, szErrorMessage);
			LocalFree(szErrorMessage);
		}
		else
		{
			//swprintf_s(str, TEXT("[Could not find a description for error # %#x.]\n"), hr);
		}
		MessageBox(hwnd, szErrorMessage, TEXT("COM Error"), MB_OK);
	}
}