// headers
#include<Windows.h>
#include<stdio.h> // for swprintf_s()
#include"AutomationServer.h"

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);


// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("MyApp");
	HWND hwnd; // handle to window
	MSG msg; // message
	TCHAR str[255] = TEXT("Entering into the app");
	HRESULT hr;

	// code

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // callback function
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(
		szAppName, // lpClassName
		TEXT("My Application"), // lpWindowName
		WS_OVERLAPPEDWINDOW, // dwStyle
		CW_USEDEFAULT, // x
		CW_USEDEFAULT, // y
		CW_USEDEFAULT, // width
		CW_USEDEFAULT, // height
		NULL, // hWndParent
		NULL, // hMenu
		hInstance,
		NULL // lpParam
	);

	// show window
	ShowWindow(hwnd, iCmdShow);

	// update window
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return ((int)msg.wParam);
}

// WndProc: callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void SafeInterfaceRelease(void);
	void ComErrorDescriptionString(HWND hwnd, HRESULT hr);

	// local variable declarations
	IDispatch* pIDispatch = NULL;
	TCHAR str[255];
	HRESULT hr;
	DISPID dispid;
	OLECHAR *szFunctionName1 = const_cast<wchar_t *>(L"SumOfTwoIntegers");
	OLECHAR *szFunctionName2 = const_cast<wchar_t*>(L"SubtractionOfTwoIntegers");

	VARIANT vArg[2], vRet;
	DISPPARAMS param = { vArg, 0, 2, NULL };
	int n1, n2;

	// code
	switch (iMsg)
	{
	case WM_CREATE: // on window creation; entered only once and at the first time only here
		// COM Initialization
		hr = CoInitialize(NULL);
		if (FAILED(hr))
		{
			ComErrorDescriptionString(hwnd, hr);
			MessageBox(NULL, TEXT("COM initialization failed.\nProgram will exit now."), TEXT("COM Error"), MB_OK);
			DestroyWindow(hwnd);
			exit(0);
		}

		// fill the str
		hr = CoCreateInstance(CLSID_MyMath, NULL,
			CLSCTX_INPROC_SERVER, 
			IID_IDispatch, (void**)& pIDispatch);
		if (FAILED(hr))
		{
			ComErrorDescriptionString(hwnd, hr);
			MessageBox(hwnd, TEXT("Component Can not be Created"), TEXT("COM Error"), MB_OK | MB_ICONERROR);
			DestroyWindow(hwnd);
			exit(0);
		}

		// *** Common code for both IMyMath->SumOfTwoInterfers() and IMyMath->SubtractionOfTwoIntegers()
		n1 = 75;
		n2 = 25;

		// as DISPPARAMS rgvarg member receives parameters in reverse order
		VariantInit(vArg);
		vArg[0].vt = VT_INT;
		vArg[0].intVal = n2;
		vArg[1].vt = VT_INT;
		vArg[1].intVal = n1;
		param.cArgs = 2;
		param.cNamedArgs = 0;
		param.rgdispidNamedArgs = NULL;

		// reverse order of paramters
		param.rgvarg = vArg;

		// return value
		VariantInit(&vRet);

		// *** Code for iMyMath->SumOfTwoIntegers() ***
		hr = pIDispatch->GetIDsOfNames(IID_NULL, &szFunctionName1,
			1, GetUserDefaultLCID(), &dispid);
		if (FAILED(hr))
		{
			ComErrorDescriptionString(hwnd, hr);
			MessageBox(NULL, TEXT("Can not get id for SumOfTwoIntegers"), TEXT("COM Error"), MB_OK | MB_ICONERROR | MB_TOPMOST);

			pIDispatch->Release();

			DestroyWindow(hwnd);
			exit(0);
		}

		hr = pIDispatch->Invoke(dispid, 
			IID_NULL, 
			GetUserDefaultLCID(), 
			DISPATCH_METHOD,
			&param, &vRet, 
			NULL, NULL);
		if (FAILED(hr))
		{
			ComErrorDescriptionString(hwnd, hr);
			MessageBox(hwnd, TEXT("Can not invoke function"), TEXT("COM Error"), MB_OK | MB_ICONERROR);
			pIDispatch->Release();
			DestroyWindow(hwnd);
			exit(0);
		}
		else
		{
			wsprintf(str, TEXT("%d + %d = %d"), n1, n2, vRet.lVal);
			MessageBox(hwnd, str, TEXT("SumOfTwoIntegers"), MB_OK);
		}


		// *** Code for iMyMath->SubtractionOfTwoIntegers() ***
		hr = pIDispatch->GetIDsOfNames(IID_NULL, &szFunctionName2,
			1, GetUserDefaultLCID(), &dispid);
		if (FAILED(hr))
		{
			ComErrorDescriptionString(hwnd, hr);
			MessageBox(NULL, TEXT("Can not get id for SubtractionOfTwoIntegers"), TEXT("COM Error"), MB_OK | MB_ICONERROR | MB_TOPMOST);

			pIDispatch->Release();

			DestroyWindow(hwnd);
			exit(0);
		}

		hr = pIDispatch->Invoke(dispid,
			IID_NULL,
			GetUserDefaultLCID(),
			DISPATCH_METHOD,
			&param, &vRet,
			NULL, NULL);
		if (FAILED(hr))
		{
			ComErrorDescriptionString(hwnd, hr);
			MessageBox(hwnd, TEXT("Can not invoke function"), TEXT("COM Error"), MB_OK | MB_ICONERROR);
			pIDispatch->Release();
			DestroyWindow(hwnd);
			exit(0);
		}
		else
		{
			wsprintf(str, TEXT("%d - %d = %d"), n1, n2, vRet.lVal);
			MessageBox(hwnd, str, TEXT("SubtractionOfTwoIntegers"), MB_OK);
		}

		// clean up
		VariantClear(vArg);
		VariantClear(&vRet);
		pIDispatch->Release();
		pIDispatch = NULL;

		// exit application
		DestroyWindow(hwnd);
		break;
	case WM_LBUTTONDOWN:

		break;
	case WM_KEYDOWN: // on any key down
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE: // on escape destroy the window
			DestroyWindow(hwnd);
			break;
		}
		break;
	case WM_DESTROY:
		CoUninitialize();
		PostQuitMessage(0);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}




void ComErrorDescriptionString(HWND hwnd, HRESULT hr)
{
	// variable declarations
	TCHAR* szErrorMessage = NULL;
	TCHAR str[255];

	if (FACILITY_WINDOWS == HRESULT_FACILITY(hr))
	{
		hr = HRESULT_CODE(hr);

		if (FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
			NULL, hr, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			(LPTSTR)& szErrorMessage, 0, NULL) != 0)
		{
			//swprintf_s(str, TEXT("%#x : %s"), hr, szErrorMessage);
			LocalFree(szErrorMessage);
		}
		else
		{
			//swprintf_s(str, TEXT("[Could not find a description for error # %#x.]\n"), hr);
		}
		MessageBox(hwnd, szErrorMessage, TEXT("COM Error"), MB_OK);
	}
}