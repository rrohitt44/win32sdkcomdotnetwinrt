#pragma once

#include<Windows.h>

class IMyMath :public IDispatch
{
public :
	// pure virtual
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int*) = 0;

	// pure virtual
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int*) = 0;
};


// CLSID of MyMath Component : {6F743B5B-233B-470B-A0E1-1A24AA8BE290}
const CLSID CLSID_MyMath = { 0x6f743b5b, 0x233b, 0x470b, 0xa0, 0xe1, 0x1a, 0x24, 0xaa, 0x8b, 0xe2, 0x90 };

// IID of ISum Interface : {10CAFBE1-0455-4E9E-8987-958A210B5164}
const IID IID_IMyMath = { 0x10cafbe1, 0x455, 0x4e9e, 0x89, 0x87, 0x95, 0x8a, 0x21, 0xb, 0x51, 0x64 };

