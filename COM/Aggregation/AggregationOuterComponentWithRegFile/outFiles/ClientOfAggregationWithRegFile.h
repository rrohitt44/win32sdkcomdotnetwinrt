#pragma once

#include<Windows.h>

class ISum : public IUnknown
{
public:
	// ISum specific method declarations
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int*) = 0; // pure virtual function
};

class ISubtract : public IUnknown
{
public:
	// ISubtract specific method declarations
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int*) = 0; // pure virtual function
};

class IMultiplication :public IUnknown
{
public:
	// IMultiplication specific method declarations pure virtual
	virtual HRESULT __stdcall MultiplicationOfTwoIntegers(int, int, int*) =0;
};

class IDivision :public IUnknown
{
public:
	// IDivision specific method declarations pure virtual
	virtual HRESULT __stdcall DivisionOfTwoIntegers(int, int, int*) = 0;
};

// CLSID of SumSubtract Component - {D8873370-3471-454C-BD80-A345AB7E0C2A}
const CLSID CLSID_SumSubtract = { 0xd8873370, 0x3471, 0x454c, 0xbd, 0x80, 0xa3, 0x45, 0xab, 0x7e, 0xc, 0x2a };

// IID of Isum Inteface - {54041451-9450-4EF2-97A9-2DC7211D1A76}
const IID IID_ISum = { 0x54041451, 0x9450, 0x4ef2, 0x97, 0xa9, 0x2d, 0xc7, 0x21, 0x1d, 0x1a, 0x76 };

// IID of ISubtract Interface - {064C2AAF-4D7D-43C6-91FB-C4C5A22426A3}
const IID IID_ISubtract = { 0x64c2aaf, 0x4d7d, 0x43c6, 0x91, 0xfb, 0xc4, 0xc5, 0xa2, 0x24, 0x26, 0xa3 };

// CLSID of MultiplicationDivision component - {483CC103-8CE8-434B-887D-A8EB160C392F}
const CLSID CLSID_MultiplicationDivision = { 0x483cc103, 0x8ce8, 0x434b, 0x88, 0x7d, 0xa8, 0xeb, 0x16, 0xc, 0x39, 0x2f };

// IID of IMultiplication Interface - {32F734B1-E992-4BBB-A96B-0CB3E78837FD}
const IID IID_IMultiplication = { 0x32f734b1, 0xe992, 0x4bbb, 0xa9, 0x6b, 0xc, 0xb3, 0xe7, 0x88, 0x37, 0xfd };

// IID of IDivision Interface - {B5649DAC-DD1B-40EE-9553-54E544940815}
const IID IID_IDivision = { 0xb5649dac, 0xdd1b, 0x40ee, 0x95, 0x53, 0x54, 0xe5, 0x44, 0x94, 0x8, 0x15 };



