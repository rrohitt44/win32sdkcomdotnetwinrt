#pragma once

#include<Windows.h>

class ISum : public IUnknown
{
public:
	// ISum specific method declarations
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int*) = 0; // pure virtual function
};

class ISubtract : public IUnknown
{
public:
	// ISubtract specific method declarations
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int*) = 0; // pure virtual function
};

// CLSID of SumSubtract Component - {D8873370-3471-454C-BD80-A345AB7E0C2A}
const CLSID CLSID_SumSubtract = { 0xd8873370, 0x3471, 0x454c, 0xbd, 0x80, 0xa3, 0x45, 0xab, 0x7e, 0xc, 0x2a };

// IID of Isum Inteface - {54041451-9450-4EF2-97A9-2DC7211D1A76}
const IID IID_ISum = { 0x54041451, 0x9450, 0x4ef2, 0x97, 0xa9, 0x2d, 0xc7, 0x21, 0x1d, 0x1a, 0x76 };

// IID of ISubtract Interface - {064C2AAF-4D7D-43C6-91FB-C4C5A22426A3}
const IID IID_ISubtract = { 0x64c2aaf, 0x4d7d, 0x43c6, 0x91, 0xfb, 0xc4, 0xc5, 0xa2, 0x24, 0x26, 0xa3 };


