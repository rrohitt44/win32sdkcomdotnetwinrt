#pragma once

// headers
#include<Windows.h>

// class declarations
class IMultiplication :public IUnknown
{
public:
	// IMultiplication specific method declarations pure virtual
	virtual HRESULT __stdcall MultiplicationOfTwoIntegers(int, int, int*) =0;
};

class IDivision :public IUnknown
{
public:
	// IDivision specific method declarations pure virtual
	virtual HRESULT __stdcall DivisionOfTwoIntegers(int, int, int*) = 0;
};

// CLSID of MultiplicationDivision component - {483CC103-8CE8-434B-887D-A8EB160C392F}
const CLSID CLSID_MultiplicationDivision = { 0x483cc103, 0x8ce8, 0x434b, 0x88, 0x7d, 0xa8, 0xeb, 0x16, 0xc, 0x39, 0x2f };

// IID of IMultiplication Interface - {32F734B1-E992-4BBB-A96B-0CB3E78837FD}
const IID IID_IMultiplication = { 0x32f734b1, 0xe992, 0x4bbb, 0xa9, 0x6b, 0xc, 0xb3, 0xe7, 0x88, 0x37, 0xfd };

// IID of IDivision Interface - {B5649DAC-DD1B-40EE-9553-54E544940815}
const IID IID_IDivision = { 0xb5649dac, 0xdd1b, 0x40ee, 0x95, 0x53, 0x54, 0xe5, 0x44, 0x94, 0x8, 0x15 };

