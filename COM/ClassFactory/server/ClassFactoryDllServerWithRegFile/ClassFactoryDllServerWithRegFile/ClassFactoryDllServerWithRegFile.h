#pragma once

#include<Windows.h>

class ISum : public IUnknown
{
public:
	// ISum specific method declarations
	virtual HRESULT __stdcall SumOfTwoIntegers(int, int, int*) = 0; // pure virtual function
};

class ISubtract : public IUnknown
{
public:
	// ISubtract specific method declarations
	virtual HRESULT __stdcall SubtractionOfTwoIntegers(int, int, int*) = 0; // pure virtual function
};

// CLSID of SumSubtract Component - {CBF3DB51-550D-4954-8164-B7DF97CDD3CB}
const CLSID CLSID_SumSubtract = { 0xcbf3db51, 0x550d, 0x4954, 0x81, 0x64, 0xb7, 0xdf, 0x97, 0xcd, 0xd3, 0xcb };

// IID of Isum Inteface - {D264F1E3-C95F-45F5-A488-1A64CC8A9E57}
const IID IID_ISum = { 0xd264f1e3, 0xc95f, 0x45f5, 0xa4, 0x88, 0x1a, 0x64, 0xcc, 0x8a, 0x9e, 0x57 };

// IID of ISubtract Interface - {94C4C760-7A5B-45E8-B28E-DCD84F9AFFCA}
const IID IID_ISubtract = { 0x94c4c760, 0x7a5b, 0x45e8, 0xb2, 0x8e, 0xdc, 0xd8, 0x4f, 0x9a, 0xff, 0xca };

extern "C" HRESULT __stdcall DllGetClassObject(REFCLSID rclsid, REFIID riid, void** ppv);

extern "C" HRESULT __stdcall DllCanUnloadNow(void);

