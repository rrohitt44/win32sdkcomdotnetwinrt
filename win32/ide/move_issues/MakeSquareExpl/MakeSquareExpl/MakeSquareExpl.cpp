//Headers
#include<windows.h>
#include"MyMath.h"


//Global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");

	//code
	//inialize WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	//Register above class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindow(szAppName,
		TEXT("My First Window Application"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	//message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Code
	HINSTANCE hDll = NULL;
	typedef int (*MakeSquareFN)(int);
	MakeSquareFN pFN = NULL;
	TCHAR str[255];
	int i, j;

	switch (iMsg)
	{
	case WM_CREATE:
	{
		//Load DLL explicitely
		hDll = LoadLibrary(TEXT("MyMathEXPL.dll"));
		
		if (hDll == NULL)
		{
			wsprintf(str, TEXT("Could not Load MyMathEXPL.dll"));
			MessageBox(hwnd, str, TEXT("Message"), MB_OK);
			DestroyWindow(hwnd);
		}

		//Assign address of a funtion to a function pointer 
		pFN = (MakeSquareFN)GetProcAddress(hDll,"MakeSquare");
		
		if (pFN == NULL)
		{
			wsprintf(str, TEXT("Could not find Procedure MakeSquare in MyMathEXPL.dll"));
			MessageBox(hwnd, str, TEXT("Error"), MB_OK);
			DestroyWindow(hwnd);
		}
		i = 25;
		j = pFN(i);
		wsprintf(str, TEXT("Square of %d is %d"), i, j);
		MessageBox(hwnd, str, TEXT("Message"), MB_OK);
		FreeLibrary(hDll);
		break;
	}
	
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		break;
	}
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}







