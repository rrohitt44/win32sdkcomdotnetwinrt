// headers
#include<Windows.h>

#include <iostream>
#include"resource.h"
// text-to-speech - for enabling com
#define _ATL_APARTMENT_THREADED
#include<atlbase.h>
extern CComModule _Module;
#include<atlcom.h>

// com initialization
#include<sapi.h>
#pragma warning(disable : 4996) // to disable GetVersionExW warning
#include<sphelper.h> // CSpStreamFormat 
#include <strsafe.h>             // StringCchCopy, StringCchCopyN
#include<string>
#include<initguid.h>
#include<cassert>

// for audio and video
#include<dshow.h>
#pragma comment(lib, "strmiids")

#include<mmsystem.h> // for audio capturing
#pragma comment(lib, "winmm.lib")
#include <cstdlib>
#include <fstream>

#define WM_RECOEVENT WM_USER+1
#define TIMER_ID 102 // timer id

using namespace std;

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
BOOL initAudioCapture(void);
BOOL recordAudio(void);
void showAudioCommand(void);
void speakText(LPCWSTR inputString);
void setColorFlagForWindow(LPWSTR colorName);

ISpVoice* pVoice = NULL; // voice setup
WCHAR theString[30];
WCHAR resultString[200];
LPWSTR fromWaveBuffer; // from wave file
HWND ghwnd;
const int NUMPTS = 44100 * 2;
static int sampleRate = 44100;//11025;
short int waveIn[NUMPTS];
const char* fileName = "temp.wav";
bool gbDone = false;
UINT iPaintFlag = 0;
BOOL gbStartListening = FALSE;

// audio file related
	// long pointer to wide char string - L"temp.wav\0");//
LPCWSTR MY_WAVE_AUDIO_FILE_NAME = LPCWSTR(L"temp.wav\0");//L"helloworld.wav\0"); // filename
//LPCWSTR TEST_CFG = LPCWSTR(L"temp.xml\0");
CComPtr<ISpStream>  cpInputStream;
CComPtr<ISpRecognizer> cpRecognizer;
CComPtr<ISpRecoContext> cpRecoContext;
CComPtr<ISpRecoGrammar> cpRecoGrammer;
CComPtr<ISpRecoResult> cpResult;
CComPtr<ISpObjectToken>        cpVoiceToken;
CComPtr<IEnumSpObjectTokens>   cpEnum;
ULONG ulCount = 0;

//CComPtr<ISpRecognizer> cpRecognizer;
CComPtr<ISpObjectToken> cpObjectToken;
CComPtr<ISpAudio> cpAudio;

BOOL isAudioInitSuccessful = FALSE;
bool fEndStreamReached = FALSE;
bool fFoundSomething = FALSE;
HRESULT hr;
RECT grc;

void SaveWavFile(const char* FileName, WAVEHDR WaveHeader);

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("MyApp");
	HWND hwnd; // handle to window
	MSG msg; // message
	

	// COM initialization: COM must be available before any SAPI specific code is implemented
	if (FAILED(::CoInitialize(NULL)))
	{
		MessageBox(NULL, TEXT("COM is not initialized"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}
	

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // callback function
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, TEXT("MYICON"));
	wndclass.hCursor = LoadCursor(hInstance, TEXT("CUSTCURSOR"));
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, TEXT("CUSTICONSM"));

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(
		szAppName, // lpClassName
		TEXT("Voice Recognizer - Zira (inactive)"), // lpWindowName
		WS_OVERLAPPEDWINDOW, // dwStyle
		CW_USEDEFAULT, // x
		CW_USEDEFAULT, // y
		CW_USEDEFAULT, // width
		CW_USEDEFAULT, // height
		NULL, // hWndParent
		NULL, // hMenu
		hInstance,
		NULL // lpParam
	);
	ghwnd = hwnd;

	// show window
	ShowWindow(hwnd, iCmdShow);

	// update window
	UpdateWindow(hwnd);

	/*hr = cpRecoContext->SetNotifyWin32Event();
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Error while setting notify event of win32"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}
	
	//LPWSTR fromWaveBuffer; // from wave file
	// handle SR event logic; Do stuff
	while (!fEndStreamReached && S_OK == cpRecoContext->WaitForNotifyEvent(INFINITE))
	{
		showAudioCommand();	
	}*/
	
	// message loop
	/*while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}*/

	/*
		Since GetMessage takes only Hardware Messages,
		we must use PeekMessage for OpenGL/Direct3D
	*/
	while (gbDone == false)
	{
		if (PeekMessage(&msg,
			NULL, //retrieves messages for any window that belongs to the calling thread
			0, //specifies the value of the first message in the rangle of messages to be examined
			0, //specifies the value of the last message in the rangle of messages to be examined
			PM_REMOVE //remove the message from the message queue; exception is WM_PAINT which is not removed
		))
		{
			if (msg.message == WM_QUIT)
			{
				gbDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			while (!fEndStreamReached && S_OK == cpRecoContext->WaitForNotifyEvent(INFINITE));
		}
	}

	//MessageBox(NULL, TEXT(fFoundSomething), TEXT("ERROR"), MB_ICONERROR);
	// clean up time
	//hr = cpInputStream->Close();
	//hr = cpRecoGrammer->SetDictationState(SPRS_INACTIVE);
	//hr = cpRecoGrammer->UnloadDictation();
	hr = cpRecoGrammer->SetGrammarState(SPGS_DISABLED);
	hr = cpRecoGrammer->SetRuleState(L"Greeting", NULL, SPRS_INACTIVE);
	hr = cpRecoGrammer->SetRuleState(L"exiting", NULL, SPRS_INACTIVE);
	hr = cpRecoGrammer->SetRuleState(L"hello", NULL, SPRS_INACTIVE);
	hr = cpRecognizer->SetRecoState(SPRST_INACTIVE);
	hr = cpRecoContext->SetContextState(SPCS_DISABLED);
	
	if (pVoice)
	{
		pVoice->Release();
		pVoice = NULL;
	}

	::CoUninitialize(); // uninitializating COM
	return ((int)msg.wParam);
}

// WndProc: callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// local variable declarations
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rc;
	int wmId, wmEvent;
	static TCHAR str[] = TEXT("Welcome to ASTROMEDICOMP!!!");
	WCHAR tempString[30];
	// for painting window
	static HBRUSH hBrush;

	// for loading bitmap
	HINSTANCE hInstance;
	static HBITMAP hbitSS;
	BITMAP bitSS;
	HDC cdc;

	// code
	switch (iMsg)
	{
	case WM_CREATE: // on window creation; entered only once and at the first time only here
		
		// load bitmat
		hInstance = GetModuleHandle(NULL);
		hbitSS = LoadBitmap(hInstance, MAKEINTRESOURCE(MYBITMAP));
		if (hbitSS == NULL)
		{
			MessageBox(hwnd, TEXT("Could not load bitmap"), TEXT("Error"), MB_OK | MB_ICONINFORMATION);
			DestroyWindow(hwnd);
		}

		// initialize audio capture
		isAudioInitSuccessful = initAudioCapture();
		if (!isAudioInitSuccessful)
		{
			MessageBox(NULL, TEXT("Error while initializing audio capture"), TEXT("ERROR"), MB_ICONERROR);
			return FALSE;
		}

		// make our window access the reco messages
		hr = cpRecoContext->SetNotifyWindowMessage(hwnd, WM_RECOEVENT, 0, 0);
		if (FAILED(hr))
		{
			MessageBox(hwnd, TEXT("Wiring up win32 event failed"), TEXT("ERROR"), MB_ICONERROR);
			return FALSE;
		}

		// Setting-up voice
		hr = CoCreateInstance(CLSID_SpVoice, NULL, CLSCTX_ALL, IID_ISpVoice, (void**)& pVoice);
		if (SUCCEEDED(hr))
		{
			// set interests
			//pVoice->SetInterest(SPFEI(SPEI_WORD_BOUNDARY), SPFEI(SPEI_WORD_BOUNDARY));
			// set notify message
			//pVoice->SetNotifyWindowMessage(ghwnd, WM_USER, 0, 0);
			cpVoiceToken.Release();
			hr = cpEnum->Next(1, &cpVoiceToken, NULL); // set to david
			cpVoiceToken.Release();
			hr = cpEnum->Next(1, &cpVoiceToken, NULL); // set to zira ( and next will be mark)
			if (FAILED(hr))
			{
				MessageBox(ghwnd, TEXT("error while getting the voice"), TEXT("ERROR"), MB_ICONERROR);
			}

			pVoice->SetVoice(cpVoiceToken);

			//pVoice->Speak((LPCWSTR)waveIn, SPF_DEFAULT, NULL);
			//pVoice->Speak(w, SPF_DEFAULT, NULL);

			//pVoice->Speak(L"Many Many Happy Returns of the Day!", SPF_ASYNC, NULL);
		}

		speakText(L"Welcome to Astromedicomp. How may I help you.");
		break;
	case WM_LBUTTONDOWN: // mouse left click

		break;
	case WM_RECOEVENT:
		showAudioCommand();
		
		break;
	case WM_COMMAND:
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
			//case IDM_SPE
		default:
			break;
		}
		break;
	case WM_USER: // SAPI message
		SPEVENT eventItem;
		memset(&eventItem, 0, sizeof(SPEVENT));
		while (pVoice->GetEvents(1, &eventItem, NULL) == S_OK)
		{
			switch (eventItem.eEventId)
			{
			case SPEI_WORD_BOUNDARY:
				SPVOICESTATUS eventStatus;
				pVoice->GetStatus(&eventStatus, NULL);

				ULONG start, end;
				start = eventStatus.ulInputWordPos;
				end = eventStatus.ulInputWordLen;

				StringCchCopyN(tempString, 30, fromWaveBuffer + start, end);
				//StringCchCopy(tempString,  30, theString+start);
				tempString[end] = ' ';
				tempString[end] = '\0';
				StringCchCat(resultString, 200, tempString); // concat the result
				break;
			default:

				break;
			}
		}

		SpClearEvent(&eventItem);

		break;
	case WM_PAINT: // to draw anything
	{
		GetClientRect(hwnd, &rc); // get client area
		grc = rc;
		// start drawing
		hdc = BeginPaint(hwnd, &ps);
		SetTextColor(hdc, RGB(255, 255, 255));
		// change color
		switch (iPaintFlag)
		{
		case 1: // red
			SetBkColor(hdc, RGB(255, 0, 0)); // set text color
			hBrush = CreateSolidBrush(RGB(255, 0, 0));
			SelectObject(hdc, hBrush);
			FillRect(hdc, &rc, hBrush);
			break;
		case 2: // green
			SetBkColor(hdc, RGB(0, 255, 0)); // set text color
			hBrush = CreateSolidBrush(RGB(0, 255, 0));
			SelectObject(hdc, hBrush);
			FillRect(hdc, &rc, hBrush);
			break;
		case 3: // blue
			SetBkColor(hdc, RGB(0, 0, 255)); // set text color
			hBrush = CreateSolidBrush(RGB(0, 0, 255));
			SelectObject(hdc, hBrush);
			FillRect(hdc, &rc, hBrush);
			break;
		case 4: // cyan
			SetBkColor(hdc, RGB(0, 255, 255)); // set text color
			hBrush = CreateSolidBrush(RGB(0, 255, 255));
			SelectObject(hdc, hBrush);
			FillRect(hdc, &rc, hBrush);
			break;
		case 5: // mangenta
			SetBkColor(hdc, RGB(255, 0, 255)); // set text color
			hBrush = CreateSolidBrush(RGB(255, 0, 255));
			SelectObject(hdc, hBrush);
			FillRect(hdc, &rc, hBrush);
			break;
		case 6: // yellow
			SetBkColor(hdc, RGB(255, 255, 0)); // set text color
			hBrush = CreateSolidBrush(RGB(255, 255, 0));
			SelectObject(hdc, hBrush);
			FillRect(hdc, &rc, hBrush);
			break;
		case 7: // white
			SetBkColor(hdc, RGB(255, 255, 255)); // set text color
			hBrush = CreateSolidBrush(RGB(255, 255, 255));
			SelectObject(hdc, hBrush);
			FillRect(hdc, &rc, hBrush);
			break;
		case 9:
			SetTimer(ghwnd, TIMER_ID, 0, NULL); // again set the timer
			break;
		case 8: // black
		case 0:
			SetBkColor(hdc, RGB(0,0,0)); // set text color
			hBrush = CreateSolidBrush(RGB(0, 0, 0));
			SelectObject(hdc, hBrush);
			FillRect(hdc, &rc, hBrush);
			break;
		}

		// show bitmap
		cdc = CreateCompatibleDC(NULL);
		if(gbStartListening==FALSE)
		{
		
		SelectObject(cdc, hbitSS);
		GetObject(hbitSS, sizeof(BITMAP), &bitSS);
		StretchBlt(
			//destination
			hdc, rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top,
			// source
			cdc, 0, 0, bitSS.bmWidth, bitSS.bmHeight,
			// raster operation
			SRCCOPY);
		}

		// Draw welcome message
		DrawText(hdc, str, -1, &rc, DT_SINGLELINE | DT_CENTER | DT_VCENTER); // Draw text
		
		DeleteObject(cdc);
		// end painting
		EndPaint(hwnd, &ps);
	}
	break;
	case WM_TIMER:
		KillTimer(hwnd, TIMER_ID); // kill the timer first as it may cause recursion when this handler take time to execute
		iPaintFlag++; // increament paint flag every time timer elapses
		// check if timer flat goes beyond our color range codes; 0 and 8 are for black
		if (iPaintFlag > 7)
		{
			iPaintFlag = 0; // return to black
		}
		InvalidateRect(hwnd, &rc, TRUE); // post WM_PAINT
		SetTimer(hwnd, TIMER_ID, 2000, NULL); // again set the timer
		break;

	case WM_KEYDOWN: // on any key down
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE: // on escape destroy the window
			DestroyWindow(hwnd);
			break;
		case VK_SPACE:
			if (gbStartListening == FALSE)
			{
				SetWindowText(hwnd, TEXT("Voice Recognizer - Zira (Active)"));
				gbStartListening = TRUE;
			}else
			{
				SetWindowText(hwnd, TEXT("Voice Recognizer - Zira (Inactive)"));
				gbStartListening = FALSE;
			}
			break;
		}
		break;
	case WM_DESTROY:
		MessageBox(hwnd, TEXT("Thank you for Visiting!!!"), TEXT("GUI app"), MB_OK | MB_ICONINFORMATION); // display what is spoken
		PostQuitMessage(0);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}


void SaveWavFile(const char* FileName, WAVEHDR WaveHeader)
{
	fstream myFile(FileName, fstream::out | fstream::binary);

	int chunksize, pcmsize, NumSamples, subchunk1size;
	int audioFormat = 1;
	int numChannels = 1;
	int bitsPerSample = 8;



	NumSamples = ((long)(NUMPTS / sampleRate) * 1000);
	pcmsize = sizeof(PCMWAVEFORMAT);


	subchunk1size = 16;
	int byteRate = sampleRate * numChannels * bitsPerSample / 8;
	int blockAlign = numChannels* bitsPerSample / 8;
	int subchunk2size = WaveHeader.dwBufferLength * numChannels;
	chunksize = (36 + subchunk2size);
	// write the wav file per the wav file format
	myFile.seekp(0, ios::beg);
	myFile.write("RIFF", 4);					// chunk id
	myFile.write((char*)& chunksize, 4);	        	// chunk size (36 + SubChunk2Size))
	myFile.write("WAVE", 4);					// format
	myFile.write("fmt ", 4);					// subchunk1ID
	myFile.write((char*)& subchunk1size, 4);			// subchunk1size (16 for PCM)
	myFile.write((char*)& audioFormat, 2);//2			// AudioFormat (1 for PCM)
	myFile.write((char*)& numChannels, 2);			// NumChannels
	myFile.write((char*)& sampleRate, 4); //4			// sample rate
	myFile.write((char*)& byteRate, 4);			// byte rate (SampleRate * NumChannels * BitsPerSample/8)
	myFile.write((char*)& blockAlign, 2); //2			// block align (NumChannels * BitsPerSample/8)
	myFile.write((char*)& bitsPerSample, 2); //2			// bits per sample
	myFile.write("data", 4);					// subchunk2ID
	myFile.write((char*)& subchunk2size, 4);			// subchunk2size (NumSamples * NumChannels * BitsPerSample/8)

	myFile.write(WaveHeader.lpData, WaveHeader.dwBufferLength);	// data
	myFile.close();
}

BOOL initAudioCapture(void)
{
	// ***************Load data from wave file starts*************************** //


	// code
	/*
	// create basic sapi stream object
	hr = cpInputStream.CoCreateInstance(CLSID_SpStream);
	if (FAILED(::CoInitialize(NULL)))
	{
		MessageBox(NULL, TEXT("Error creating input stream object"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}
	// initialize input stream and bind it to the file
	CSpStreamFormat iInputFormat;
	hr = iInputFormat.AssignFormat(SPSF_11kHz8BitMono); // SPSF_22kHz8BitStereo &pFormat
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Input formatting failed"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}

	// bind input stream to file
	hr = cpInputStream->BindToFile(MY_WAVE_AUDIO_FILE_NAME, SPFM_OPEN_READONLY,
		&iInputFormat.FormatId(), iInputFormat.WaveFormatExPtr(), SPFEI_ALL_EVENTS);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("binding to file failed. check file value, etc"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}*/
	//MessageBox(ghwnd, TEXT("Audio recorder starting"), TEXT("message"), MB_OK);
	// initialize the recognizer and configure it's input
	// initialize the recognition engine
	hr = cpRecognizer.CoCreateInstance(CLSID_SpInprocRecognizer); //CLSID_SpInprocRecognizer
	if (FAILED(hr))
	{
		MessageBox(ghwnd, TEXT("error while initializing recognizer"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}

	// input audio code starts

	// get the default audio input token
	hr = SpGetDefaultTokenFromCategoryId(SPCAT_AUDIOIN, &cpObjectToken);
	if (SUCCEEDED(hr))
	{
		// set audio input to our token
		hr = cpRecognizer->SetInput(cpObjectToken, TRUE);
	}

	if (SUCCEEDED(hr))
	{
		// create default audio input object
		hr = SpCreateDefaultObjectFromCategoryId(SPCAT_AUDIOIN, &cpAudio);
	}

	if (SUCCEEDED(hr))
	{
		// set audio input to our token
		hr = cpRecognizer->SetInput(cpAudio, TRUE);
	}

	if (SUCCEEDED(hr))
	{
		if (hr == SPERR_ENGINE_BUSY)
		{
			MessageBox(ghwnd, TEXT("err"), TEXT("Error"), MB_OK);
			return FALSE;
		}
	}
	//

	// create recognition context
	hr = cpRecognizer->CreateRecoContext(&cpRecoContext);
	if (FAILED(hr))
	{
		MessageBox(ghwnd, TEXT("Error creating Reco context"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}

	// hookup wave input to the recognizer
	//hr = cpRecognizer->SetInput(cpInputStream, TRUE);

	// input audio code ends
	/*if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Linking wave to recognizer failed"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}*/


	// create , load, and activate reco grammer

	// check for the things recognized and the end of the stream
	// Specifically, the interest level in this example is to �All SR Events�. 
	// This means that a SR event will fire for everything that the recognizer thinks it can parse accurately.
	hr = cpRecoContext->SetInterest(SPFEI_ALL_SR_EVENTS | SPFEI(SPEI_END_SR_STREAM),
		SPFEI_ALL_SR_EVENTS | SPFEI(SPEI_END_SR_STREAM));
	if (FAILED(hr))
	{
		MessageBox(ghwnd, TEXT("Error setting interests"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}

	// finish up configuring the SR objects by creating the grammer, setting interest level 
	// & wiring up the SR win32 event

	// create grammer
	hr = cpRecoContext->CreateGrammar(0, &cpRecoGrammer);
	if (FAILED(hr))
	{
		MessageBox(ghwnd, TEXT("Error creating grammer"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}

	hr = cpRecoContext->Pause(0);
	if (FAILED(hr))
	{
		MessageBox(ghwnd, TEXT("Error while pausing context"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}
	// load dictation
	//hr = cpRecoGrammer->LoadDictation(NULL, SPLO_STATIC);
	hr = cpRecoGrammer->LoadCmdFromFile(L"testGrammer.grxml", SPLO_STATIC);
	if (FAILED(hr))
	{
		MessageBox(ghwnd, TEXT("Error loading grammer file"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}


	hr = cpRecoContext->Resume(0);
	if (FAILED(hr))
	{
		MessageBox(ghwnd, TEXT("Error while resuming context"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}

	// activate
	hr = cpRecognizer->SetRecoState(SPRST_ACTIVE);
	if (FAILED(hr))
	{
		MessageBox(ghwnd, TEXT("Error while setting recognizer state active"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}
	hr = cpRecoContext->SetContextState(SPCS_ENABLED);
	if (FAILED(hr))
	{
		MessageBox(ghwnd, TEXT("Error while setting context state active"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}
	hr = cpRecoGrammer->SetGrammarState(SPGS_ENABLED);
	if (FAILED(hr))
	{
		MessageBox(ghwnd, TEXT("Error while setting grammer state active"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}
	hr = cpRecoGrammer->SetRuleState(L"Greeting", NULL, SPRS_ACTIVE);
	hr = cpRecoGrammer->SetRuleState(L"hello", NULL, SPRS_ACTIVE);
	hr = cpRecoGrammer->SetRuleState(L"exiting", NULL, SPRS_ACTIVE);
	if (FAILED(hr))
	{
		MessageBox(ghwnd, TEXT("Error while setting rule state active"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}

	// enumerate list of voices
	hr = SpEnumTokens(SPCAT_VOICES, NULL, NULL, &cpEnum);
	if (FAILED(hr))
	{
		MessageBox(ghwnd, TEXT("Error while enumerating the voices"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}

	// get number of voices
	hr = cpEnum->GetCount(&ulCount);

	return TRUE;
}

BOOL recordAudio(void)
{
	// **********************Record audio start
	MessageBox(NULL, TEXT("Audio recorder starting"), TEXT("message"), MB_OK);



	HWAVEIN hWaveIn;
	WAVEHDR WaveInHDR;
	MMRESULT result;

	// specify recording parameters
	WAVEFORMATEX pFormat;
	pFormat.wFormatTag = WAVE_FORMAT_PCM; // simple uncompressed format
	pFormat.nChannels = 1; // 1-mono, 2-sterio
	pFormat.nSamplesPerSec = sampleRate;
	pFormat.nAvgBytesPerSec = sampleRate;
	pFormat.nBlockAlign = 1;
	pFormat.wBitsPerSample = 8; // 16 for high quality, 8 for telephone grade
	pFormat.cbSize = 0;
	//MessageBox(NULL, TEXT("before waveInOpen"), TEXT("Message"), MB_ICONERROR);
	result = waveInOpen(&hWaveIn, WAVE_MAPPER, &pFormat, 0L, 0L, WAVE_FORMAT_DIRECT);
	//MessageBox(NULL, TEXT("after waveInOpen"), TEXT("Message"), MB_ICONERROR);
	if (result)
	{
		char fault[256];
		//waveInGetErrorText(result, fault, 256);
		MessageBox(NULL, TEXT("wave in open error"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}

	// setup and prepare header for input
	WaveInHDR.lpData = (LPSTR)waveIn;
	WaveInHDR.dwBufferLength = NUMPTS * 2;
	WaveInHDR.dwBytesRecorded = 0;
	WaveInHDR.dwUser = 0L;
	WaveInHDR.dwFlags = 0L;
	WaveInHDR.dwLoops = 0L;

	waveInPrepareHeader(hWaveIn, &WaveInHDR, sizeof(WAVEHDR));
	//MessageBox(NULL, TEXT("waveinprepare header set"), TEXT("Message"), MB_ICONERROR);
	// insert a wave input buffer
	result = waveInAddBuffer(hWaveIn, &WaveInHDR, sizeof(WAVEHDR));
	//MessageBox(NULL, TEXT("after wavein add buffer"), TEXT("Message"), MB_ICONERROR);
	if (result)
	{
		char fault[256];
		//waveInGetErrorText(result, fault, 256);
		MessageBox(NULL, TEXT("failed to read blocks from device"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}


	// commense sampling input
	//MessageBox(NULL, TEXT("before waveinstart"), TEXT("Message"), MB_ICONERROR);
	result = waveInStart(hWaveIn);
	//MessageBox(NULL, TEXT("after waveInstart"), TEXT("Message"), MB_ICONERROR);
	if (result)
	{
		char fault[256];
		//waveInGetErrorText(result, fault, 256);
		MessageBox(NULL, TEXT("failed to start the recording"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}

	// wait until finish recording
	//MessageBox(NULL, TEXT("before while loop"), TEXT("Message"), MB_ICONERROR);
	do
	{

	} while (waveInUnprepareHeader(hWaveIn, &WaveInHDR, sizeof(WAVEHDR)) == WAVERR_STILLPLAYING);
	//MessageBox(NULL, TEXT("after while loop"), TEXT("Message"), MB_ICONERROR);
	waveInClose(hWaveIn);
	//MessageBox(NULL, TEXT("wave in closed"), TEXT("Message"), MB_ICONERROR);


	//pWaveHdr = &WaveInHDR;
	SaveWavFile(fileName, WaveInHDR);
	// play recorded audio
	HWAVEOUT hWaveOut = NULL;
	result = waveOutOpen(&hWaveOut, WAVE_MAPPER, &pFormat, 0L, 0L, WAVE_FORMAT_DIRECT);
	if (result)
	{
		char fault[256];
		//waveInGetErrorText(result, fault, 256);
		MessageBox(NULL, TEXT("wave out open error"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}
	//MessageBox(NULL, TEXT("after waveOutOpen"), TEXT("Message"), MB_ICONERROR);
	//WaveInHDR.lpData = CA2A("This is honey"); //L"This is honey";
	result = waveOutPrepareHeader(hWaveOut, &WaveInHDR, sizeof(WAVEHDR));
	if (result)
	{
		char fault[256];
		//waveInGetErrorText(result, fault, 256);
		MessageBox(NULL, TEXT("wave out prepared header error"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}

	result = waveOutWrite(hWaveOut, &WaveInHDR, sizeof(WAVEHDR));
	//waveOutGetT
	if (result)
	{
		char fault[256];
		//waveInGetErrorText(result, fault, 256);
		MessageBox(NULL, TEXT("wave out write error"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}
	waveOutClose(hWaveOut);
	MessageBox(NULL, TEXT("Audio recorder ends"), TEXT("message"), MB_OK);
	return TRUE;
	// **********************Record audio ends **************************
}

void showAudioCommand(void)
{
	CSpEvent spEvent;
	// extract queued events from reco context's event queue
	while (!fEndStreamReached && S_OK == spEvent.GetFrom(cpRecoContext))
	{
		if (gbStartListening == TRUE)
		{
			// figure out the event
			switch (spEvent.eEventId)
			{
				// recognized something
			case SPEI_RECOGNITION:

				fFoundSomething = TRUE;

				cpResult = spEvent.RecoResult();
				cpResult->GetText(SP_GETWHOLEPHRASE, SP_GETWHOLEPHRASE, TRUE, &fromWaveBuffer, NULL);
				//MessageBox(ghwnd, fromWaveBuffer, TEXT("message"), MB_OK);
				setColorFlagForWindow(fromWaveBuffer);

				break;
			case SPEI_FALSE_RECOGNITION:
				//speakText(L"Your Query is not understood. Please try again.");
				break;
			case SPEI_END_SR_STREAM:
				fEndStreamReached = TRUE;
				break;
			}
		}
		
		// clear Event
		spEvent.Clear();
	}
}

void setColorFlagForWindow(LPWSTR cname)
{
	KillTimer(ghwnd, TIMER_ID); // kill the timer first as it may cause recursion when this handler take time to execute
	/*if (_tcsstr(cname, L"hi zira") != NULL)
	{
		speakText(L"Yes rohit");
		//speakText(L"hi rohit, I am fine, how may i help you?");
	}else */if (_tcsstr(cname, L"hi zira how are you") != NULL)
	{
		speakText(L"I am fine Rohit, I hope you are also fine");
		//speakText(L"hi rohit, I am fine, how may i help you?");
	}
	else if (_tcsstr(cname, L"color to red") != NULL)
	{
		speakText(L"Changing window color to red");
		iPaintFlag = 1;
		InvalidateRect(ghwnd, &grc, TRUE);
	}else if (_tcsstr(cname, L"color to green") != NULL)
	{
		speakText(L"Changing window color to green");
		iPaintFlag = 2;
		InvalidateRect(ghwnd, &grc, TRUE);
	}else if (_tcsstr(cname, L"color to blue") != NULL)
	{
		speakText(L"Changing window color to blue");
		iPaintFlag = 3;
		InvalidateRect(ghwnd, &grc, TRUE);
	}else if (_tcsstr(cname, L"color to cyan") != NULL)
	{
		speakText(L"Changing window color to cyan");
		iPaintFlag = 4;
		InvalidateRect(ghwnd, &grc, TRUE);
	}else if (_tcsstr(cname, L"color to mangenta") != NULL)
	{
		speakText(L"Changing window color to mangenta");
		iPaintFlag = 5;
		InvalidateRect(ghwnd, &grc, TRUE);
	}else if (_tcsstr(cname, L"color to yellow") != NULL)
	{
		speakText(L"Changing window color to yellow");
		iPaintFlag = 6;
		InvalidateRect(ghwnd, &grc, TRUE);
	}else if (_tcsstr(cname, L"color to white") != NULL)
	{
		speakText(L"Changing window color to white");
		iPaintFlag = 7;
		InvalidateRect(ghwnd, &grc, TRUE);
	}else if (_tcsstr(cname, L"color to black") != NULL)
	{
		speakText(L"Changing window color to black");
		iPaintFlag = 8;
		InvalidateRect(ghwnd, &grc, TRUE);
	}else if (_tcsstr(cname, L"good bye zira see you again") != NULL)
	{
		speakText(L"It was nice talking to you Rohit, See you again, Good bye");
		PostQuitMessage(0);
	}else if (_tcsstr(cname, L"wish sir happy birthday") != NULL)
	{
		speakText(L"Many Many Happy returns of the day to you Sir");
	}
	else if (_tcsstr(cname, L"colors automatically") != NULL)
	{
		speakText(L"as you wish for rohit");
		iPaintFlag = 9;
		InvalidateRect(ghwnd, &grc, TRUE);
	}
	else if (_tcsstr(cname, L"thank you zira") != NULL)
	{
		speakText(L"it's completely my pleasure rohit");
	}
	/*else
	{
		speakText(L"Query not understood. Setting the window color to default.");
		iPaintFlag = 0;
	}*/
}
void speakText(LPCWSTR inputString)
{
	pVoice->Speak(inputString, SPF_IS_NOT_XML, NULL);
}