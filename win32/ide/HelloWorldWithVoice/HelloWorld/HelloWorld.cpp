// headers
#include<Windows.h>

// text-to-speech - for enabling com
#define _ATL_APARTMENT_THREADED
#include<atlbase.h>
extern CComModule _Module;
#include<atlcom.h>

// com initialization

#include<sapi.h>
#pragma warning(disable : 4996) // to disable GetVersionExW warning
#include<sphelper.h> // CSpStreamFormat 
#include <strsafe.h>             // StringCchCopy, StringCchCopyN
#include<string>


// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

ISpVoice* pVoice = NULL; // voice setup
WCHAR theString[30];
WCHAR resultString[200];
LPWSTR fromWaveBuffer; // from wave file
// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("MyApp");
	HWND hwnd; // handle to window
	MSG msg; // message
	HRESULT hr;

	// audio file related
	// long pointer to wide char string
	LPCWSTR MY_WAVE_AUDIO_FILE_NAME = LPCWSTR(L"helloworld.wav\0"); // filename
	LPCWSTR TEST_CFG = LPCWSTR(L"helloworld.xml\0");
	CComPtr<ISpStream>  cpInputStream;
	CComPtr<ISpRecognizer> cpRecognizer;
	CComPtr<ISpRecoContext> cpRecoContext;
	CComPtr<ISpRecoGrammar> cpRecoGrammer;
	CComPtr<ISpRecoResult> cpResult;

	// code
	// COM initialization: COM must be available before any SAPI specific code is implemented
	if (FAILED(::CoInitialize(NULL)))
	{
		MessageBox(NULL, TEXT("COM is not initialized"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}
	// create basic sapi stream object
	hr = cpInputStream.CoCreateInstance(CLSID_SpStream);
	if (FAILED(::CoInitialize(NULL)))
	{
		MessageBox(NULL, TEXT("Error creating input stream object"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}
	// initialize input stream and bind it to the file
	CSpStreamFormat iInputFormat;
	hr = iInputFormat.AssignFormat(SPSF_22kHz16BitStereo);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Input formatting failed"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}

	// bind input stream to file
	hr = cpInputStream->BindToFile(MY_WAVE_AUDIO_FILE_NAME, SPFM_OPEN_READONLY,
		&iInputFormat.FormatId(), iInputFormat.WaveFormatExPtr(), SPFEI_ALL_EVENTS);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("binding to file failed. check file value, etc"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}

	// initialize the recognizer and configure it's input
	// initialize the recognition engine
	hr = cpRecognizer.CoCreateInstance(CLSID_SpInprocRecognizer);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("error while initializing recognizer"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}

	// hookup wave input to the recognizer
	hr = cpRecognizer->SetInput(cpInputStream, TRUE);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Linking wave to recognizer failed"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}

	// finish up configuring the SR objects by creating the grammer, setting interest level & wiring up the SR win32 event
	// create recognition context
	hr = cpRecognizer->CreateRecoContext(&cpRecoContext);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Error creating Reco context"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}

	// create grammer
	hr = cpRecoContext->CreateGrammar(NULL, &cpRecoGrammer);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Error creating grammer"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}

	// load dictation
	hr = cpRecoGrammer->LoadDictation(NULL, SPLO_STATIC);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Error loading dictation"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}

	// check for the things recognized and the end of the stream
	// Specifically, the interest level in this example is to �All SR Events�. 
	// This means that a SR event will fire for everything that the recognizer thinks it can parse accurately.
	hr = cpRecoContext->SetInterest(SPFEI_ALL_SR_EVENTS |SPFEI(SPEI_END_SR_STREAM),
		SPFEI_ALL_SR_EVENTS | SPFEI(SPEI_END_SR_STREAM));
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Error setting interests"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}

	// hook up win32 events
	hr = cpRecoContext->SetNotifyWin32Event();
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Wiring up win32 event failed"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}

	// activate dictation
	hr = cpRecoGrammer->SetDictationState(SPRS_ACTIVE);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("Error while setting dictation"), TEXT("ERROR"), MB_ICONERROR);
		return FALSE;
	}

	bool fEndStreamReached = FALSE;
	bool fFoundSomething = FALSE;
	
	// handle SR event logic; Do stuff
	while (!fEndStreamReached && S_OK==cpRecoContext->WaitForNotifyEvent(10000))
	{
		
		CSpEvent spEvent;
		// extract queued events from reco context's event queue
		while (!fEndStreamReached && S_OK == spEvent.GetFrom(cpRecoContext))
		{
			
			// figure out the event
			switch (spEvent.eEventId)
			{
				// recognized something
			case SPEI_RECOGNITION:
				fFoundSomething = TRUE;
				
				cpResult = spEvent.RecoResult();
				cpResult->GetText(SP_GETWHOLEPHRASE, SP_GETWHOLEPHRASE, TRUE, &fromWaveBuffer, NULL);
				break;

			case SPEI_END_SR_STREAM:
				fEndStreamReached = TRUE;
				break;
			}

			// clear Event
			spEvent.Clear();
		}
	}

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // callback function
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(
		szAppName, // lpClassName
		TEXT("My Application"), // lpWindowName
		WS_OVERLAPPEDWINDOW, // dwStyle
		CW_USEDEFAULT, // x
		CW_USEDEFAULT, // y
		CW_USEDEFAULT, // width
		CW_USEDEFAULT, // height
		NULL, // hWndParent
		NULL, // hMenu
		hInstance,
		NULL // lpParam
	);

	// show window
	ShowWindow(hwnd, iCmdShow);
	
	// update window
	UpdateWindow(hwnd);

	
	// Setting-up voice
	hr = CoCreateInstance(CLSID_SpVoice, NULL, CLSCTX_ALL, IID_ISpVoice, (void **) &pVoice);
	if (SUCCEEDED(hr))
	{
		// set interests
		pVoice->SetInterest(SPFEI(SPEI_WORD_BOUNDARY), SPFEI(SPEI_WORD_BOUNDARY));
		// set notify message
		pVoice->SetNotifyWindowMessage(hwnd, WM_USER, 0, 0);
		//I am glad to speak.
		StringCchCopy(theString, sizeof(theString), TEXT("Many Many happy returns of the day!"));

		// theString
		pVoice->Speak(fromWaveBuffer, SPF_ASYNC, NULL);

		//pVoice->Speak(L"Many Many Happy Returns of the Day!", SPF_ASYNC, NULL);
	}

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	//MessageBox(NULL, TEXT(fFoundSomething), TEXT("ERROR"), MB_ICONERROR);
	// clean up time
	hr = cpRecoGrammer->SetDictationState(SPRS_INACTIVE);
	hr = cpRecoGrammer->UnloadDictation();
	hr = cpInputStream->Close();

	return ((int) msg.wParam);
}

// WndProc: callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// local variable declarations
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rc;
	int wmId, wmEvent;
	TCHAR str[] = TEXT("Hello World!!!");
	WCHAR tempString[30];

	// code
	switch (iMsg)
	{
	case WM_CREATE: // on window creation; entered only once and at the first time only here
		
		break;
	case WM_LBUTTONDOWN: // mouse left click
		
		break;
	case WM_COMMAND:
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
			//case IDM_SPE
		default:
			break;
		}
		break;
	case WM_USER: // SAPI message
		SPEVENT eventItem;
		memset(&eventItem , 0, sizeof(SPEVENT));
		while (pVoice->GetEvents(1, &eventItem, NULL) == S_OK)
		{
			switch (eventItem.eEventId)
			{
			case SPEI_WORD_BOUNDARY:
				SPVOICESTATUS eventStatus;
				pVoice->GetStatus(&eventStatus, NULL);

				ULONG start, end;
				start = eventStatus.ulInputWordPos;
				end = eventStatus.ulInputWordLen;
				
				StringCchCopyN(tempString, 30, fromWaveBuffer +start, end);
				//StringCchCopy(tempString,  30, theString+start);
				tempString[end] = ' ';
				tempString[end+1] = '\0';
				StringCchCat(resultString, 200, tempString); // concat the result
				break;
			default:

				break;
			}
		}
		
		SpClearEvent(&eventItem);
		
		break;
	case WM_PAINT: // to draw anything
	{
		GetClientRect(hwnd, &rc); // get client area

		// start drawing
		hdc = BeginPaint(hwnd, &ps);
		SetTextColor(hdc, RGB(0,255,0)); // set text color
		SetBkColor(hdc, RGB(0,0,0)); // set background color
		DrawText(hdc, str, -1, &rc, DT_SINGLELINE | DT_CENTER | DT_VCENTER); // Draw text

		// end painting
		EndPaint(hwnd, &ps);
	}
		break;
	case WM_KEYDOWN: // on any key down
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE: // on escape destroy the window
			DestroyWindow(hwnd);
			break;
		}
		break;
	case WM_DESTROY:
		MessageBox(hwnd, resultString, TEXT("GUI app"), MB_OK | MB_ICONWARNING); // display what is spoken
		if (pVoice)
		{
			pVoice->Release();
			pVoice = NULL;
		}

		::CoUninitialize(); // uninitializating COM


		PostQuitMessage(0);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}