// headers
#include<Windows.h>

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("MyApp");
	HWND hwnd; // handle to window
	MSG msg; // message
	TCHAR str[255] = TEXT("Entering into the app");

	// code
	MessageBox(NULL, str, TEXT("Message"), MB_OK);

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // callback function
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(
		szAppName, // lpClassName
		TEXT("My Application"), // lpWindowName
		WS_OVERLAPPEDWINDOW, // dwStyle
		CW_USEDEFAULT, // x
		CW_USEDEFAULT, // y
		CW_USEDEFAULT, // width
		CW_USEDEFAULT, // height
		NULL, // hWndParent
		NULL, // hMenu
		hInstance,
		NULL // lpParam
	);

	// show window
	ShowWindow(hwnd, iCmdShow);
	
	// update window
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return ((int) msg.wParam);
}

// WndProc: callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// local variable declarations
	TCHAR str[255];
	INT xPos = 0;
	INT yPos = 0;

	// code
	switch (iMsg)
	{
	case WM_CREATE: // on window creation; entered only once and at the first time only here
		// fill the str
		wsprintf(str, TEXT("WM_CREATE is arived."));
		MessageBox(hwnd, str, TEXT("Message"), MB_OK | MB_ICONINFORMATION);
		break;
	case WM_LBUTTONDOWN:
		xPos = LOWORD(lParam);
		yPos = HIWORD(lParam);
		wsprintf(str, TEXT("WM_LBUTTONDOWN: mouse clicked on (%d, %d).",xPos, yPos));
		MessageBox(hwnd, str, TEXT("Message"), MB_OK| MB_ICONINFORMATION);
		break;
	case WM_KEYDOWN: // on any key down
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE: // on escape destroy the window
			DestroyWindow(hwnd);
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}