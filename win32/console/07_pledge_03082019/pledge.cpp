﻿// headers
#include<Windows.h>
#include"resources.h"

#define WIN_WIDTH 800 // window width
#define WIN_HEIGHT 600 // window height
 
// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("MyApp");
	HWND hwnd; // handle to window
	MSG msg; // message

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // callback function
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, TEXT("MYICON"));
	wndclass.hCursor = LoadCursor(hInstance, TEXT("CUSTCURSOR"));
	wndclass.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, TEXT("CUSTICONSM"));

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(
		szAppName, // lpClassName
		TEXT("National Pledge: INDIA"), // lpWindowName
		WS_OVERLAPPEDWINDOW, // dwStyle
		CW_USEDEFAULT, // x
		CW_USEDEFAULT, // y
		WIN_WIDTH, // width
		WIN_HEIGHT, // height
		NULL, // hWndParent
		NULL, // hMenu
		hInstance,
		NULL // lpParam
	);

	// show window
	ShowWindow(hwnd, iCmdShow);
	
	// update window
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return ((int) msg.wParam);
}

// WndProc: callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// local variable declarations
	HDC hdc;
	PAINTSTRUCT ps;
	static RECT rc;
	static BOOL togglePledge = FALSE;

	int xPos = 0;
	int yPos = 0;
	int incPos = 20;
	// heading
	TCHAR str[] = TEXT("***NATIONAL PLEDGE: INDIA***");
	TCHAR strMarathi[] = TEXT("***राष्ट्रीय प्रतिज्ञा (भारत)***");

	// section 1
	TCHAR str1[] = TEXT("India is my country.");
	TCHAR str2[] = TEXT("All indians are my brothers and sisters.");
	TCHAR str3[] = TEXT("I love my country");
	TCHAR str4[] = TEXT("and I am proud of its rich and varied heritage.");
	TCHAR str1Marathi[] = TEXT("भारत माझा देश आहे.");
	TCHAR str2Marathi[] = TEXT("सारे भारतीय माझे बांधव आहेत. ");
	TCHAR str3Marathi[] = TEXT("माझ्या देशावर माझे प्रेम आहे.");
	TCHAR str4Marathi[] = TEXT("माझ्या देशातल्या समृद्ध आणि विविधतेने नटलेल्या परंपरांचा मला अभिमान आहे.");

	// section 2
	TCHAR str5[] = TEXT("I shall always strive to be worthy of it.");
	TCHAR str6[] = TEXT("I shall give my parents, teachers");
	TCHAR str7[] = TEXT("and all elders respect");
	TCHAR str8[] = TEXT("and treat everyone with courtesy.");
	TCHAR str5Marathi[] = TEXT("त्या परंपरांचा पाईक होण्याची पात्रता माझ्या अंगी यावी म्हणून मी सदैव प्रयत्न करीन.");
	TCHAR str6Marathi[] = TEXT("मी माझ्या पालकांचा, गुरुजनांचा");
	TCHAR str7Marathi[] = TEXT("आणि वडीलधार्‍या माणसांचा मान ठेवीन");
	TCHAR str8Marathi[] = TEXT("आणि प्रत्येकाशी सौजन्याने वागेन.");

	// section 3
	TCHAR str9[] = TEXT("To my country and my people,");
	TCHAR str10[] = TEXT("I pledge my devotion.");
	TCHAR str11[] = TEXT("In their well being and prosperity alone,");
	TCHAR str12[] = TEXT("lies my happyness.");
	TCHAR str9Marathi[] = TEXT("माझा देश आणि माझे देशबांधव");
	TCHAR str10Marathi[] = TEXT("यांच्याशी निष्ठा राखण्याची मी प्रतिज्ञा करीत आहे.");
	TCHAR str11Marathi[] = TEXT("त्यांचे कल्याण आणि त्यांची समृद्धी ह्यांतच माझे ");
	TCHAR str12Marathi[] = TEXT("सौख्य सामावले आहे.");

	// for bitmaps
	HDC mdc; // memory dc
	static HBITMAP hBitmap; // handle to bitmap
	BITMAP bitmap; // actual bitmap
	HINSTANCE hInstance;

	// code
	switch (iMsg)
	{
	case WM_CREATE: // on window creation; entered only once and at the first time only here
		// get application instance
		hInstance = GetModuleHandle(NULL);

		// load bitmap
		hBitmap = LoadBitmap(hInstance, TEXT("MYBITMAP"));
		if (hBitmap == NULL)
		{
			// show error message
			MessageBox(hwnd, TEXT("Couldnot load bitmap"),TEXT("ERROR"), MB_OK | MB_ICONINFORMATION);
			DestroyWindow(hwnd); // destroy the window
		}
		break;
	case WM_LBUTTONDOWN: // mouse left click
		
		break;
	case WM_PAINT: // to draw anything
	{
		GetClientRect(hwnd, &rc); // get client area
		// start drawing
		hdc = BeginPaint(hwnd, &ps);
		
		// get memory dc
		mdc = CreateCompatibleDC(NULL);
		SelectObject(mdc, hBitmap); // select the bitmap object
		GetObject(hBitmap, sizeof(BITMAP), &bitmap); // get bitmap information
		// copy data from memdc to hdc
		/*StretchBlt(
		// destination params
			hdc, WIN_WIDTH / 3, WIN_HEIGHT / 4, WIN_WIDTH / 4, WIN_HEIGHT / 4,
			// source params
			mdc, 0,0, bitmap.bmWidth, bitmap.bmHeight,
			// raster operation
			SRCCOPY
		);*/
		// need to add /D "UNICODE" while using command line
		// heading
		SetTextColor(hdc, RGB(0, 0, 255)); // blue
		SetBkColor(hdc, RGB(0,0,0)); // black
		SetTextAlign(hdc, TA_CENTER);

		// set initial positions
		xPos = (WIN_WIDTH / 2);// +wcslen(str4);
		yPos += incPos;
		if (togglePledge == FALSE) // english
		{
			TextOut(hdc, xPos, yPos, str, wcslen(str)); // wcslen: unicode version of strlen

			// section 1
			yPos += incPos;
			yPos += incPos;
			SetTextColor(hdc, RGB(255, 128, 0)); // orange
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str1, wcslen(str1));
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str2, wcslen(str2));
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str3, wcslen(str3));
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str4, wcslen(str4));

			// section 2
			SetTextColor(hdc, RGB(255, 255, 255)); // white
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str5, wcslen(str5));
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str6, wcslen(str6));
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str7, wcslen(str7));
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str8, wcslen(str8));

			// section 2
			SetTextColor(hdc, RGB(0, 255, 0)); // green
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str9, wcslen(str9));
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str10, wcslen(str10));
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str11, wcslen(str11));
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str12, wcslen(str12));
		}
		else // marathi
		{
			TextOut(hdc, xPos, yPos, strMarathi, wcslen(strMarathi)); // wcslen: unicode version of strlen

			// section 1
			yPos += incPos;
			yPos += incPos;
			SetTextColor(hdc, RGB(255, 128, 0)); // orange
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str1Marathi, wcslen(str1Marathi));
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str2Marathi, wcslen(str2Marathi));
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str3Marathi, wcslen(str3Marathi));
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str4Marathi, wcslen(str4Marathi));

			// section 2
			SetTextColor(hdc, RGB(255, 255, 255)); // white
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str5Marathi, wcslen(str5Marathi));
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str6Marathi, wcslen(str6Marathi));
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str7Marathi, wcslen(str7Marathi));
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str8Marathi, wcslen(str8Marathi));

			// section 2
			SetTextColor(hdc, RGB(0, 255, 0)); // green
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str9Marathi, wcslen(str9Marathi));
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str10Marathi, wcslen(str10Marathi));
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str11Marathi, wcslen(str11Marathi));
			yPos += incPos; // increament y position
			TextOut(hdc, xPos, yPos, str12Marathi, wcslen(str12Marathi));
		}
		// delete mdc
		DeleteObject(mdc);
		// end painting
		EndPaint(hwnd, &ps);
	}
		break;
	case WM_KEYDOWN: // on any key down
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE: // on escape destroy the window
			DestroyWindow(hwnd);
			break;
		case VK_SPACE: // on space press
			if (togglePledge == TRUE)
			{
				togglePledge = FALSE;
			}
			else
			{
				togglePledge = TRUE;
			}
			InvalidateRect(hwnd, &rc, true); // post WM_PAINT messages
			break;
		}
		break;
	
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}