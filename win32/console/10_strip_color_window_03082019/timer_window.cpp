// headers
#include<Windows.h>
#include"resources.h"
 
#define WIN_WIDTH 800 // window width
#define WIN_HEIGHT 600 // window height
#define TIMER_ID 102 // timer id

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("MyApp");
	HWND hwnd; // handle to window
	MSG msg; // message

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // callback function
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, TEXT("MYICON"));
	wndclass.hCursor = LoadCursor(hInstance, TEXT("CUSTCURSOR"));
	wndclass.hbrBackground = (HBRUSH) GetStockObject(LTGRAY_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, TEXT("CUSTICONSM"));

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(
		szAppName, // lpClassName
		TEXT("Window with changing colors"), // lpWindowName
		WS_OVERLAPPEDWINDOW, // dwStyle
		CW_USEDEFAULT, // x
		CW_USEDEFAULT, // y
		WIN_WIDTH, // width
		WIN_HEIGHT, // height
		NULL, // hWndParent
		NULL, // hMenu
		hInstance,
		NULL // lpParam
	);

	// show window
	ShowWindow(hwnd, iCmdShow);
	
	// update window
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return ((int) msg.wParam);
}

// WndProc: callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// local variable declarations
	HDC hdc;
	PAINTSTRUCT ps;
	static UINT iPaintFlag=0; // black by default
	static RECT rc;
	static RECT rcTemp;

	// for painting window
	static HBRUSH hBrush;

	// code
	switch (iMsg)
	{
	case WM_CREATE: // on window creation; entered only once and at the first time only here
		// set the timer for first time
		SetTimer(hwnd, TIMER_ID, 1000, NULL);
		break;
	case WM_TIMER:
		KillTimer(hwnd, TIMER_ID); // kill the timer first as it may cause recursion when this handler take time to execute
		// check if timer flat goes beyond our color range codes; 0 and 8 are for black
		if (iPaintFlag > 7)
		{
			iPaintFlag = 0; // return to black
		}
		iPaintFlag++; // increament paint flag every time timer elapses
		
		// bottom and right will be remain as it is
		// only change will be in left and top
		rcTemp.right = rc.right / 8;
		rcTemp.bottom = rc.bottom;
		rcTemp.top = rc.top;

		if (rcTemp.left > rc.right)
		{
			rcTemp.left = rc.left;
		}
		else
		{
			rcTemp.left = rcTemp.left + rcTemp.right;
		}

		InvalidateRect(hwnd, &rcTemp, TRUE); // post WM_PAINT
		SetTimer(hwnd, TIMER_ID, 1000, NULL); // again set the timer
		break;
	case WM_LBUTTONDOWN: // mouse left click
		
		break;
	case WM_PAINT: // to draw anything
	{
		GetClientRect(hwnd, &rc); // get client area
		// start drawing
		hdc = BeginPaint(hwnd, &ps);

		switch (iPaintFlag)
		{
		case 1: // red
			hBrush = CreateSolidBrush(RGB(255,0,0));
			
			break;
		case 2: // green
			hBrush = CreateSolidBrush(RGB(0, 255, 0));
			
			break;
		case 3: // blue
			hBrush = CreateSolidBrush(RGB(0, 0, 255));
			
			break;
		case 4: // cyan
			hBrush = CreateSolidBrush(RGB(0, 255, 255));
			
			break;
		case 5: // mangenta
			hBrush = CreateSolidBrush(RGB(255, 0, 255));
			
			break;
		case 6: // yellow
			hBrush = CreateSolidBrush(RGB(255, 255, 0));
			
			break;
		case 7: // white
			hBrush = CreateSolidBrush(RGB(255, 255, 255));
			
			break;
		case 8: // black
			hBrush = CreateSolidBrush(RGB(0, 0, 0));
			
			break;
		}

		

		SelectObject(hdc, hBrush);
		FillRect(hdc, &rcTemp, hBrush);

		// end painting
		EndPaint(hwnd, &ps);
	}
	break;
	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;
	
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}