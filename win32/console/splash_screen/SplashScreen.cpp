// headers
#include<Windows.h>
#include"SplashScreen.h"
 
// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("MyApp");
	HWND hwnd; // handle to window
	MSG msg; // message

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // callback function
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, TEXT("MYICON"));
	wndclass.hCursor = LoadCursor(hInstance, TEXT("CUSTCURSOR"));
	wndclass.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, TEXT("CUSTICONSM"));

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(
		szAppName, // lpClassName
		TEXT("My Application"), // lpWindowName
		WS_OVERLAPPEDWINDOW, // dwStyle
		CW_USEDEFAULT, // x
		CW_USEDEFAULT, // y
		CW_USEDEFAULT, // width
		CW_USEDEFAULT, // height
		NULL, // hWndParent
		NULL, // hMenu
		hInstance,
		NULL // lpParam
	);

	// show window
	ShowWindow(hwnd, iCmdShow);
	
	// update window
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return ((int) msg.wParam);
}

// WndProc: callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// local variable declarations
	HDC hdc;
	PAINTSTRUCT ps;
	RECT rc;
	TCHAR str[] = TEXT("Hello World!!!");

	// for bitmap loading
	HDC mdc;
	static HBITMAP hBitmap;
	BITMAP bitmap;
	HINSTANCE hInstance;

	// code
	switch (iMsg)
	{
	case WM_CREATE: // on window creation; entered only once and at the first time only here
		hInstance = GetModuleHandle(NULL); // get the instance of the currently executing program
		// load the bitmap now
		hBitmap = LoadBitmap(hInstance, TEXT("MYBITMAP"));
		if (hBitmap == NULL)
		{
			// show the error message
			MessageBox(hwnd, TEXT("Couldnot load the bitmap"), TEXT("ERROR"), MB_OK | MB_ICONINFORMATION);
			// destroy window now
			DestroyWindow(hwnd);
		}
		break;
	case WM_LBUTTONDOWN: // mouse left click
		
		break;
	case WM_PAINT: // to draw anything
	{
		GetClientRect(hwnd, &rc); // get client area

		// start drawing
		hdc = BeginPaint(hwnd, &ps);

		// get the memDC for bitmap
		mdc = CreateCompatibleDC(NULL); // create the memory dc
		SelectObject(mdc, hBitmap);
		GetObject(hBitmap, sizeof(BITMAP), &bitmap);

		StretchBlt(
			// destination params
			hdc, rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top,
			// source params
			mdc, 0, 0, bitmap.bmWidth, bitmap.bmHeight,
			// raster operation
			SRCCOPY
		);
		DeleteObject(mdc);
		// end painting
		EndPaint(hwnd, &ps);
	}
		break;
	case WM_KEYDOWN: // on any key down
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE: // on escape destroy the window
			DestroyWindow(hwnd);
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}