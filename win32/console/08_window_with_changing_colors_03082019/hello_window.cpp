// headers
#include<Windows.h>
#include"resources.h"
 
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("MyApp");
	HWND hwnd; // handle to window
	MSG msg; // message

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // callback function
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, TEXT("MYICON"));
	wndclass.hCursor = LoadCursor(hInstance, TEXT("CUSTCURSOR"));
	wndclass.hbrBackground = (HBRUSH) GetStockObject(WHITE_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, TEXT("CUSTICONSM"));

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(
		szAppName, // lpClassName
		TEXT("Window with changing colors"), // lpWindowName
		WS_OVERLAPPEDWINDOW, // dwStyle
		CW_USEDEFAULT, // x
		CW_USEDEFAULT, // y
		WIN_WIDTH, // width
		WIN_HEIGHT, // height
		NULL, // hWndParent
		NULL, // hMenu
		hInstance,
		NULL // lpParam
	);

	// show window
	ShowWindow(hwnd, iCmdShow);
	
	// update window
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return ((int) msg.wParam);
}

// WndProc: callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// local variable declarations
	HDC hdc;
	PAINTSTRUCT ps;
	static UINT iPaintFlag=0; // black by default
	static RECT rc;

	// for painting window
	static HBRUSH hBrush;

	// code
	switch (iMsg)
	{
	case WM_CREATE: // on window creation; entered only once and at the first time only here
		
		break;
	case WM_LBUTTONDOWN: // mouse left click
		
		break;
	case WM_PAINT: // to draw anything
	{
		GetClientRect(hwnd, &rc); // get client area
		// start drawing
		hdc = BeginPaint(hwnd, &ps);
		
		switch (iPaintFlag)
		{
		case 1: // red
			hBrush = CreateSolidBrush(RGB(255,0,0));
			SelectObject(hdc, hBrush);
			FillRect(hdc, &rc, hBrush);
			break;
		case 2: // green
			hBrush = CreateSolidBrush(RGB(0, 255, 0));
			SelectObject(hdc, hBrush);
			FillRect(hdc, &rc, hBrush);
			break;
		case 3: // blue
			hBrush = CreateSolidBrush(RGB(0, 0, 255));
			SelectObject(hdc, hBrush);
			FillRect(hdc, &rc, hBrush);
			break;
		case 4: // cyan
			hBrush = CreateSolidBrush(RGB(0, 255, 255));
			SelectObject(hdc, hBrush);
			FillRect(hdc, &rc, hBrush);
			break;
		case 5: // mangenta
			hBrush = CreateSolidBrush(RGB(255, 0, 255));
			SelectObject(hdc, hBrush);
			FillRect(hdc, &rc, hBrush);
			break;
		case 6: // yellow
			hBrush = CreateSolidBrush(RGB(255, 255, 0));
			SelectObject(hdc, hBrush);
			FillRect(hdc, &rc, hBrush);
			break;
		case 7: // white
			hBrush = CreateSolidBrush(RGB(255, 255, 255));
			SelectObject(hdc, hBrush);
			FillRect(hdc, &rc, hBrush);
			break;
		case 8: // black
		case 0:
			hBrush = CreateSolidBrush(RGB(0, 0, 0));
			SelectObject(hdc, hBrush);
			FillRect(hdc, &rc, hBrush);
			break;
		}
		// end painting
		EndPaint(hwnd, &ps);
	}
		break;
	case WM_CHAR: // on any key down
		switch (LOWORD(wParam))
		{
		case 'R': // red
		case 'r':
			iPaintFlag = 1;
			break;

		case 'G': // green
		case 'g':
			iPaintFlag = 2;
			break;

		case 'B': // blue
		case 'b':
			iPaintFlag = 3;
			break;

		case 'C': // cyan
		case 'c':
			iPaintFlag = 4;
			break;

		case 'M': // mangenta
		case 'm':
			iPaintFlag = 5;
			break;

		case 'Y': // yellow
		case 'y':
			iPaintFlag = 6;
			break;

		case 'W': // white
		case 'w':
			iPaintFlag = 7;
			break;
		case 'Q':
		case 'q': // quit
			DestroyWindow(hwnd);
			break;
		case 'K': // black
		case 'k':
		default:
			iPaintFlag = 8;
			break;
		}
		InvalidateRect(hwnd, &rc, TRUE);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}