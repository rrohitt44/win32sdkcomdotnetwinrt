// headers
#include<Windows.h>
#include"resources.h"
 
#define WIN_WIDTH 800 // window width
#define WIN_HEIGHT 600 // window height
#define TIMER_ID 102 // timer id

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
DWORD WINAPI ThreadProcOne(LPVOID);
DWORD WINAPI ThreadProcTwo(LPVOID);

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("MyApp");
	HWND hwnd; // handle to window
	MSG msg; // message

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // callback function
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, TEXT("MYICON"));
	wndclass.hCursor = LoadCursor(hInstance, TEXT("CUSTCURSOR"));
	wndclass.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, TEXT("CUSTICONSM"));

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(
		szAppName, // lpClassName
		TEXT("Multithreaded Window"), // lpWindowName
		WS_OVERLAPPEDWINDOW, // dwStyle
		CW_USEDEFAULT, // x
		CW_USEDEFAULT, // y
		WIN_WIDTH, // width
		WIN_HEIGHT, // height
		NULL, // hWndParent
		NULL, // hMenu
		hInstance,
		NULL // lpParam
	);

	// show window
	ShowWindow(hwnd, iCmdShow);
	
	// update window
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return ((int) msg.wParam);
}

// WndProc: callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// local variable declarations
	HDC hdc;
	PAINTSTRUCT ps;
	static RECT rc;
	
	TCHAR str[255]=TEXT("Hello INDIA");
	int xPos = 400;
	int yPos = 400;
	int incPos = 20;

	// threads
	HANDLE hThreadOne = NULL;
	HANDLE hThreadTwo = NULL;

	// code
	switch (iMsg)
	{
	case WM_CREATE: // on window creation; entered only once and at the first time only here
		// set the timer for first time
		SetTimer(hwnd, TIMER_ID, 1000, NULL);
		hThreadOne = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)ThreadProcOne, (LPVOID) hwnd, 0, NULL);
		hThreadTwo = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)ThreadProcTwo, (LPVOID)hwnd, 0, NULL);
		break;
	case WM_TIMER:
		KillTimer(hwnd, TIMER_ID); // kill the timer first as it may cause recursion when this handler take time to execute
		break;
	case WM_LBUTTONDOWN: // mouse left click
		MessageBox(hwnd, TEXT("left mouse button clicked"), TEXT("Message"), MB_OK | MB_ICONINFORMATION);
		break;
	case WM_PAINT: // to draw anything
	{
		GetClientRect(hwnd, &rc); // get client area
		// start drawing
		hdc = BeginPaint(hwnd, &ps);
		
		SetTextColor(hdc, RGB(0, 255, 0)); // green
		SetBkColor(hdc, RGB(0, 0, 0)); // black
		SetTextAlign(hdc, TA_CENTER);
		yPos += incPos;
		TextOut(hdc, xPos, yPos, str, lstrlen(str));
		
		ReleaseDC(hwnd, hdc);
		// end painting
		EndPaint(hwnd, &ps);
	}
	break;
	case WM_KEYDOWN:
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;
	
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

DWORD WINAPI ThreadProcOne(LPVOID param)
{
	HDC hdc;
	long int i;
	TCHAR str[255];
	hdc = GetDC((HWND)param);
	SetTextColor(hdc, RGB(0, 255, 0)); // green
	SetBkColor(hdc, RGB(0, 0, 0)); // black
	for (i = 0; i < 429495729; i++)
	{
		wsprintf(str, TEXT("Thread one -> increasing order: %ld"), i);
		TextOut(hdc, 5, 5, str, lstrlen(str));
	}
	
	ReleaseDC((HWND)param, hdc);
	return 0;
}

DWORD WINAPI ThreadProcTwo(LPVOID param)
{
	HDC hdc;
	long int i;
	TCHAR str[255];
	hdc = GetDC((HWND)param);
	SetTextColor(hdc, RGB(0, 255, 0)); // green
	SetBkColor(hdc, RGB(0, 0, 0)); // black
	for(i = 429495729; i > 0; i--)
	{
		wsprintf(str, TEXT("Thread two -> decreasing order: %ld"), i);
		TextOut(hdc, 5, 25, str, lstrlen(str));
	}

	ReleaseDC((HWND)param, hdc);
	return 0;
}