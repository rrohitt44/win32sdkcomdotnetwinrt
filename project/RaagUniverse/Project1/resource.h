//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SplashScreen.rc

// Next default values for new objects
// 
#define CUSTICONSM 101 // icon on caption bar
#define MYBITMAP 103 // splash screen
#define IDM_APP_ABOUT 104
#define IDC_STATIC -1

#define ID_RAAGNAMEID 201
#define ID_THAATID 202
#define ID_VARJYASWAR		203
#define ID_JATI		204
#define ID_VADISWAR	205
#define ID_SAMWADISWAR		206
#define ID_SAMAY	207
#define ID_AAROH 208
#define ID_AVROH 209
#define ID_PAKAD	210
#define ID_DESC	211
#define ID_SWAR_VISTAR_STHAYI 212
#define ID_SWAR_VISTAR_ANTARA 213
#define ID_AALAPI_STHAYI 214
#define ID_AALAPI_ANTARA 215
#define ID_TAANA_STHAYI 216
#define ID_TAANA_ANTARA 217

#define ID_RAAGNAME_LBL_VAL 301
#define ID_THAAT_LBL_VAL 302
#define ID_VARJYASWAR_LBL_VAL		303
#define ID_JATI_LBL_VAL		304
#define ID_VADISWAR_LBL_VAL	305
#define ID_SAMWADISWAR_LBL_VAL		306
#define ID_SAMAY_LBL_VAL	307
#define ID_AAROH_LBL_VAL 308
#define ID_AVROH_LBL_VAL 309
#define ID_PAKAD_LBL_VAL	310
#define ID_DESC_LBL_VAL	311
#define ID_SWAR_VISTAR_STHAYI_LBL_VAL 312
#define ID_SWAR_VISTAR_ANTARA_LBL_VAL 313
#define ID_AALAPI_STHAYI_LBL_VAL 314
#define ID_AALAPI_ANTARA_LBL_VAL 315
#define ID_TAANA_STHAYI_LBL_VAL 316
#define ID_TAANA_ANTARA_LBL_VAL 317


#define ID_SHOW_RECORDS 225
#define ID_ADD_RECORD  226

// ids of columns in list-view
#define C_COLUMNS 11
#define IDS_FIRSTCOLUMN 401
#define IDS_LABEL_RAAG_NAME   401
#define IDS_LABEL_THAAT  402
#define IDS_LABEL_JAATI   403
#define IDS_LABEL_SAMAY  404
#define IDS_LABEL_VARJYASWAR   405
#define IDS_LABEL_VADISWAR  406
#define IDS_LABEL_SAMVADISWAR   407
#define IDS_LABEL_AAROH  408
#define IDS_LABEL_AVROH   409
#define IDS_LABEL_PAKAD  410
#define IDS_LABEL_DESC   411
#define IDS_LABEL_SWARVISTAR_STHAYI  412
#define IDS_LABEL_SWARVISTAR_ANTARA   413
#define IDS_LABEL_AALAP_STHAYI  414
#define IDS_LABEL_AALAP_ANTARA   415
#define IDS_LABEL_TAANA_STHAYI  416
#define IDS_LABEL_TAANA_ANTARA  417


#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define MYICON 100 // icon on explorer/taskmenu
#define CUSTCURSOR 102 // cursor

#endif
#endif
