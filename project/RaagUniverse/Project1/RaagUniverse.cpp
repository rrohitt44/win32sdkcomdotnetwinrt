﻿// headers
#include<Windows.h>

#include"resource.h"
#include<CommCtrl.h>
#include<io.h>
#include<stdio.h>
#include <tchar.h>
#define UNICODE
#define BUFFERSIZE 5000
#define NUMBER_OF_THAATS 10
#define NUMBER_OF_SWARAS 8
#define NUMBER_OF_JATIS 3
#define NUMBER_OF_SAMAYAS 4
#define NUMBER_OF_RAAGAS 20

#pragma comment(lib, "comctl32.lib")

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK AboutDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK AddRaagDlg(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK ShowRaagDlg(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK ShowRaagListViewDlg(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK ShowRecordsDlg(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
VOID CALLBACK FileIOCompletionRoutine(__in DWORD dwErrorCode, __in DWORD dwNumberOfBytesTransffered, __in LPOVERLAPPED lpOverlapped);
VOID PerformFileIO(LPCWSTR fileName, DWORD opType, DWORD operation, char *DataBuffer);

HWND hDlgAddRaag;
HWND hDlgShowRaag;
HWND hDlgShowRaagList;
HWND hDlgViewDetails;
HWND hwndList;
HINSTANCE ghInstance;
DWORD g_BytesTransferred = 0;
DWORD  dwBytesRead = 0;

struct RaagSur
{
	char sthayi[500];
	char antara[500];
};

struct RaagData
{
	char name[10]; // name of the raaga
	char thaat[10]; // thaat
	char jaati[20]; // jati of raag
	char samay[20]; // raag samay
	char varjyaSwar[20]; // varjya swar
	char vadiSwar[10]; // vadi swar
	char samvadiSwar[10]; // samvadi swar
	char aaroh[50]; // aaroh
	char avroh[50]; // avroh
	char pakad[200]; // pakad
	char desc[500]; // description of raag
	RaagSur swarVistar; // swar vistar
	RaagSur aalap; // aalap
	RaagSur taana; // taana
};
static RaagData data[1];
static RaagData raagData[NUMBER_OF_RAAGAS] = { 0 };
char SEP_RAAG[] = "&";
int itemNumberFromListView = 0;
BOOL gbPerformValidation = TRUE;
BOOL gbMarkRed = FALSE;
HWND ghRedWindowHandle;

/*
Do the file operation as stated in the parameters
*/
int PerformFileIOForRaag(LPCWSTR fileName, DWORD opType, DWORD operation, RaagData* RaagBuffer)
{
	// function prototype declarations
	VOID PerformFileIO(LPCWSTR fileName, DWORD opType, DWORD operation, char* DataBuffer);
	int extractRaagDataFromBuffer(char *szRaagBuffer, RaagData * DataBuffer); // for read operation
	
	// local variables
	char szRaagBuffer[5000];
	int counter = 0;
	//char raagFormatString[100] = "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s";
	//char SEP_RAAG[] = "&";
	// code
	if (opType == 1)
	{
		RaagData DataBuffer = RaagBuffer[0];
		// get data into buffer
		sprintf_s(szRaagBuffer,
			//strcat_s(raagFormatString,
				//sizeof(raagFormatString),
				//SEP_RAAG),
			"%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s&",
			DataBuffer.name,
			DataBuffer.thaat,
			DataBuffer.jaati,
			DataBuffer.samay,
			DataBuffer.varjyaSwar,
			DataBuffer.vadiSwar,
			DataBuffer.samvadiSwar,
			DataBuffer.aaroh,
			DataBuffer.avroh,
			DataBuffer.pakad,
			DataBuffer.desc,
			DataBuffer.swarVistar.sthayi,
			DataBuffer.swarVistar.antara,
			DataBuffer.aalap.sthayi,
			DataBuffer.aalap.antara,
			DataBuffer.taana.sthayi,
			DataBuffer.taana.antara
		);
	}
	// do file operation
	// for read operation, the buffer will go empty and comes with the data filled in it
	// for write operation, the buffer is already filled in above step and is passed
	// for writing the data to the database
	PerformFileIO(fileName, opType, operation, szRaagBuffer);

	if(opType == 0) // for read operation
	{
		// extract the data in RaagData structure
		counter = extractRaagDataFromBuffer(szRaagBuffer, RaagBuffer);
	}
	
	// empty buffer
	// szRaagBuffer = {  };
	return counter;
}

struct {
	int     iStyle;
	const TCHAR* szText;
} button[] =
{ BS_PUSHBUTTON,      TEXT("Show Records"),
BS_DEFPUSHBUTTON,   TEXT("Add Record"),
//BS_CHECKBOX,        TEXT("CHECKBOX"),      
//BS_AUTOCHECKBOX,    TEXT("AUTOCHECKBOX"),     
//BS_RADIOBUTTON,     TEXT("RADIOBUTTON"),     
//BS_3STATE,          TEXT("3STATE"),     
//BS_AUTO3STATE,      TEXT("AUTO3STATE"),     
//BS_GROUPBOX,        TEXT("GROUPBOX"),     
//BS_AUTORADIOBUTTON, TEXT("AUTORADIO"),     
//BS_OWNERDRAW,       TEXT("OWNERDRAW") 
};

#define NUM (sizeof button / sizeof button[0])

// file IO
LPCWSTR fileName = TEXT("raag_records.txt");

void ShowError(HWND hwnd, LPCWSTR message);
void ShowInfo(HWND hwnd, LPCWSTR message);

static TCHAR Thaatas[NUMBER_OF_THAATS][10] =
{
	TEXT("Kalyan"), TEXT("Bilawal"), TEXT("Khamaj"), TEXT("Bhairav"),
	TEXT("Purvi"), TEXT("Marwa"), TEXT("Kafi"), TEXT("Aasawari"),
	TEXT("Bhairavi"), TEXT("Todi")
};

static TCHAR Swaras[NUMBER_OF_SWARAS][10] =
{
	TEXT("Sa"), TEXT("Re"), TEXT("Ga"), TEXT("Ma"),
	TEXT("Pa"), TEXT("Dha"), TEXT("Ni"), TEXT("Sa.")
};

static TCHAR Jatis[NUMBER_OF_JATIS][10] =
{
	TEXT("Aud-Aud"), TEXT("Aud-Shavd"), TEXT("Sampoorn")
};

static TCHAR Samayas[NUMBER_OF_SAMAYAS][10] =
{
	TEXT("Sakal"), TEXT("Dupar"), TEXT("Sandhya"), TEXT("Ratra")
};

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	//static TCHAR szAppName[] = TEXT("MyApp");
	static TCHAR szAppName[] = TEXT("About1");
	HWND hwnd; // handle to window
	MSG msg; // message

	// code
	ghInstance = hInstance;
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // callback function
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, szAppName);
	wndclass.hCursor = LoadCursor(hInstance, TEXT("CUSTCURSOR"));
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = szAppName;
	wndclass.hIconSm = LoadIcon(hInstance, TEXT("CUSTICONSM"));

	// register above class
	RegisterClassEx(&wndclass);

	

	// ********************** stop testing *********
	// create window
	hwnd = CreateWindow(
		szAppName, // lpClassName
		TEXT("Raag Universe"), // lpWindowName
		WS_OVERLAPPEDWINDOW, // dwStyle
		CW_USEDEFAULT, // x
		CW_USEDEFAULT, // y
		CW_USEDEFAULT, // width
		CW_USEDEFAULT, // height
		NULL, // hWndParent
		NULL, // hMenu
		hInstance,
		NULL // lpParam
	);

	
	// show window
	ShowWindow(hwnd, iCmdShow); //iCmdShow

	// update window
	UpdateWindow(hwnd);

	//hDlgModeless = CreateDialog(hInstance, TEXT("ColorScrDlg"), hwnd, ColorScrDlg);

	// start combo
	int xpos = 100;            // Horizontal position of the window.
	int ypos = 100;            // Vertical position of the window.
	int nwidth = 200;          // Width of the window
	int nheight = 200;         // Height of the window
	HWND hwndParent = hwnd; // Handle to the parent window

	HWND hWndComboBox = CreateWindow(WC_COMBOBOX, TEXT(""),
		CBS_DROPDOWN | CBS_HASSTRINGS | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE,
		xpos, ypos, nwidth, nheight, hwndParent, NULL, hInstance,
		NULL);
	// load the combobox with item list.  
// Send a CB_ADDSTRING message to load each item

	//TCHAR A[16];
	int  k = 0;

	//memset(&A, 0, sizeof(A));
	for (k = 0; k < NUMBER_OF_THAATS; k += 1)
	{
		//wcscpy_s(A, sizeof(A) / sizeof(TCHAR), (TCHAR*)Planets[k]);

		// Add string to combobox.
		SendMessage(hWndComboBox, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)Thaatas[k]);
	}

	// Send the CB_SETCURSEL message to display an initial item 
	//  in the selection field  
	SendMessage(hWndComboBox, CB_SETCURSEL, (WPARAM)2, (LPARAM)0);
	// end combo

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return ((int)msg.wParam);
}

// WndProc: callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	HWND CreateListView(HWND hwndParent);
	BOOL InitListViewColumns(HWND hWndListView);
	BOOL InitListViewImageLists(HWND hWndListView);
	BOOL InsertListViewItems(HWND hWndListView, int cItems);
	void HandleWM_NOTIFY(HWND hwnd, LPARAM lParam);

	// local variable declarations
	HDC hdc, cdc;
	PAINTSTRUCT ps;
	RECT rc;
	TCHAR str[] = TEXT("Hello World!!!");
	static int   cxChar, cyChar;
	static HWND  hwndButton[NUM];
	LPSTR tempD = NULL;
	// for loading bitmap
	HINSTANCE hInstance;
	static HBITMAP hbitSS;
	BITMAP bitSS;
	static HINSTANCE hInstanceDialog;
	int i;
	static RECT  rect;
	
	// local variables
	int rowsAffected = 0;

	// code
	switch (iMsg)
	{
	case WM_CREATE: // on window creation; entered only once and at the first time only here

		hInstanceDialog = ((LPCREATESTRUCT)lParam)->hInstance;
		hInstance = GetModuleHandle(NULL);
		hbitSS = LoadBitmap(hInstance, MAKEINTRESOURCE(MYBITMAP));
		if (hbitSS == NULL)
		{
			MessageBox(hwnd, TEXT("Could not load bitmap"), TEXT("Error"), MB_OK | MB_ICONINFORMATION);
			DestroyWindow(hwnd);
		}

		// GetDialogBaseUnits to obtain the width and height of the characters in the default font
		// This is the function that dialog boxes use to obtain text dimension.
		// this is similar with GetTextMetrics
		cxChar = LOWORD(GetDialogBaseUnits()); // width 
		cyChar = HIWORD(GetDialogBaseUnits()); // height

		for (i = 0; i < NUM; i++)
		{
			hwndButton[i] = CreateWindow(
				TEXT("button"), // this is predefined
				button[i].szText,
				WS_CHILD | WS_VISIBLE | button[i].iStyle,
				400, cyChar * (1 + 2 * i), // upper-left corner of this window
				20 * cxChar, // width
				7 * cyChar / 4, // height - A push button looks best when its height is 7/4 times the height of a text character
				hwnd,
				(HMENU)i, // child window id; it is used in WM_COMMAND to identify messages from this window
				// this should be unique id
				hInstanceDialog,
				NULL
			);
		}
		break;
	case WM_SIZE:
		rect.left = 24 * cxChar;
		rect.top = 2 * cyChar;
		rect.right = LOWORD(lParam);
		rect.bottom = HIWORD(lParam);
		return 0;
	case WM_DRAWITEM:
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDM_APP_ABOUT:
			DialogBox(hInstanceDialog, TEXT("AboutBox"), hwnd, AboutDlgProc);
			break;
		case IDOK:
		case IDCANCEL:
			EndDialog(hwnd, 
				0 // this value is returned to the point where this dialog box is called
			);
			break;
		}

		if (LOWORD(wParam) == 0) // show records
		{	
			
			//MessageBox(hwnd, TEXT("Show Records"), TEXT("Message"), MB_OK | MB_ICONINFORMATION);
			
			//hDlgShowRaag = CreateDialog(hInstanceDialog, TEXT("ShowRaagDlg"), hwnd, ShowRaagDlg);

			hDlgShowRaagList = CreateListView(hwnd);
			// initialize the list view columns
			InitListViewColumns(hDlgShowRaagList);
			// initialize list view icons
			InitListViewImageLists(hDlgShowRaagList);
			// load data to fill in the list view
			// read data in buffer
			// 0 for read
			rowsAffected = PerformFileIOForRaag(fileName, 0, GENERIC_READ, raagData);
			// fill the list view with data
			InsertListViewItems(hDlgShowRaagList, rowsAffected);


		}
		else if (LOWORD(wParam) == 1) // add record
		{
			//char dataBufferItem[] = "This is some test data to write to the file.";
			// write date to file
			//PerformFileIO(fileName, LOWORD(wParam), FILE_APPEND_DATA, dataBufferItem);
			// open dialog box
			hDlgAddRaag = CreateDialog(hInstanceDialog, TEXT("AddRaagDlg"), hwnd, AddRaagDlg);
		}
		
		break;

	case WM_NOTIFY:
		HandleWM_NOTIFY(hwnd, lParam);
		break;
	case WM_LBUTTONDOWN: // mouse left click

		break;
	case WM_PAINT: // to draw anything
	{
		InvalidateRect(hwnd, &rect, TRUE);
		GetClientRect(hwnd, &rc); // get client area

		// start drawing
		hdc = BeginPaint(hwnd, &ps);
		cdc = CreateCompatibleDC(NULL);
		SelectObject(cdc, hbitSS);
		GetObject(hbitSS, sizeof(BITMAP), &bitSS);
		StretchBlt(
			//destination
			hdc, rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top,
			// source
			cdc, 0, 0, bitSS.bmWidth, bitSS.bmHeight,
			// raster operation
			SRCCOPY);
		SetTextColor(hdc, RGB(0, 255, 0)); // set text color
		SetBkColor(hdc, RGB(0, 0, 0)); // set background color
		DrawText(hdc, str, -1, &rc, DT_SINGLELINE | DT_CENTER | DT_VCENTER); // Draw text

		DeleteObject(cdc);
		// end painting
		EndPaint(hwnd, &ps);
	}
	break;
	case WM_KEYDOWN: // on any key down
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE: // on escape destroy the window
			DestroyWindow(hwnd);
			break;
		}
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

INT_PTR CALLBACK AboutDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG: return TRUE;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK: case IDCANCEL: EndDialog(hDlg, 0); return TRUE;
		}
		break;
	}
	return FALSE;
}

INT_PTR CALLBACK AddRaagDlg(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	// function prototype declarations
	void SD_OnHVScroll(HWND hwnd, int bar, UINT code);
	BOOL FilterCombo(HWND hwnd, TCHAR filterList[][10], TCHAR * filterCriteria, BOOL showDropdown, int iterationLimit);
	BOOL ValidateAndPopulateRaag(HWND hDlg, RaagData * raagData);

	// local variables
	TCHAR szBufferGlobal[1000];
	// get resource ids;
	static RaagData raagData = { 0 };
	static HWND thaatResourceId = 0;
	static HWND jatisResourceId = 0;
	static HWND vadiSwarResourceId = 0;
	static HWND samvadiSwarResourceId = 0;
	static HWND samayResourceId = 0;
	DWORD dataLen = 0;
	BOOL isSuccess = FALSE;
	static HBRUSH hBrushGreenBg;
	static HBRUSH hBrushWhiteBg;
	static HBRUSH hBrushRedBg;
	static HWND controlWindowId = 0;

	switch (message)
	{
	case WM_INITDIALOG:
		// ShowInfo(hDlg, TEXT("AddRaagDlg: first arrived dialog"));
		// populate thats
		// get thaat id and populate thaatas
		thaatResourceId = GetDlgItem(hDlg, ID_THAATID);
		FilterCombo(thaatResourceId, Thaatas, NULL, FALSE, NUMBER_OF_THAATS);

		// get Jatis id and populate jatis
		jatisResourceId = GetDlgItem(hDlg, ID_JATI);
		FilterCombo(jatisResourceId, Jatis, NULL, FALSE, NUMBER_OF_JATIS);

		// get wadi swar id and populate vadi swaras
		vadiSwarResourceId = GetDlgItem(hDlg, ID_VADISWAR);
		FilterCombo(vadiSwarResourceId, Swaras, NULL, FALSE, NUMBER_OF_SWARAS);

		// get samwadi swar id and populate samvadi swaras
		samvadiSwarResourceId = GetDlgItem(hDlg, ID_SAMWADISWAR);
		FilterCombo(samvadiSwarResourceId, Swaras, NULL, FALSE, NUMBER_OF_SWARAS);

		// get samaya resource id and populate samayas
		samayResourceId = GetDlgItem(hDlg, ID_SAMAY);
		FilterCombo(samayResourceId, Samayas, NULL, FALSE, NUMBER_OF_SAMAYAS);

		// set red color
		hBrushGreenBg = CreateSolidBrush(RGB(152, 251, 152));
		hBrushWhiteBg = CreateSolidBrush(RGB(255, 255, 255));
		hBrushRedBg = CreateSolidBrush(RGB(255, 99, 71));
		return TRUE;
	//case WM_MOUSEWHEEL:
	case WM_VSCROLL:
		//ShowInfo(hDlg, TEXT("Sample Scroll"));
		SD_OnHVScroll(hDlg, SB_VERT, LOWORD(wParam));
		break;
	
	case WM_CTLCOLOREDIT:
		SetBkMode((HDC)wParam, // handle to internal device context (HDC) Window is using to draw the control
			TRANSPARENT);
		controlWindowId = (HWND)lParam; // get control id
		// don't choose bg color for dropdowns
		if (gbMarkRed == TRUE && controlWindowId==ghRedWindowHandle)
		{
			return (LRESULT)hBrushRedBg;
		}
		
		if (controlWindowId == thaatResourceId
			|| controlWindowId == vadiSwarResourceId
			|| controlWindowId == samvadiSwarResourceId
			|| controlWindowId == samayResourceId
			|| controlWindowId == jatisResourceId)
		{
			return (LRESULT)hBrushWhiteBg;
		}else
		{
			return (LRESULT)hBrushGreenBg; // TODO - delete this object later
		}
		break;
	case WM_COMMAND:
		switch (HIWORD(wParam))
		{
		/*case EN_KILLFOCUS:
		{
			switch (LOWORD(wParam))
			{
			case ID_RAAGNAMEID:
				//ShowInfo(hDlg, TEXT("ID_RAAGNAMEID"));
				isSuccess = ValidateAndPopulateRaag(hDlg, &raagData);

				if (!isSuccess)
				{
					return FALSE;
				}
				SendMessage(
					(HWND)hDlg,
					WM_SETREDRAW,
					(WPARAM)wParam,
					(LPARAM)lParam
				);
				//SendMessage(hDlg, WM_NEXTDLGCTL, (WPARAM)GetDlgItem(hDlg, ID_RAAGNAMEID), TRUE);
				break;
			}
		}
		break;*/
		case CBN_SELCHANGE:
			// If the user makes a selection from the list:
			//   Send CB_GETCURSEL message to get the index of the selected list item.
			//   Send CB_GETLBTEXT message to get the item.
			//   Display the item in a messagebox.
		{
			int ItemIndex = SendMessage((HWND)lParam, (UINT)CB_GETCURSEL,
				(WPARAM)0, (LPARAM)0);
			TCHAR  ListItem[256];
			(TCHAR)SendMessage((HWND)lParam, (UINT)CB_GETLBTEXT,
				(WPARAM)ItemIndex, (LPARAM)ListItem);
			//MessageBox(hDlg, (LPCWSTR)ListItem, TEXT("Item Selected"), MB_OK);
		}
		break;
		case CBN_EDITUPDATE:
			// perform auto filter
			// get the data in the selection field
			TCHAR  ListItem[256];
			GetWindowText((HWND)lParam, ListItem, 256);

			// filter thaatas
			if((HWND)lParam == thaatResourceId)
			{
				FilterCombo((HWND)lParam, Thaatas, ListItem, TRUE, NUMBER_OF_THAATS);
			}
			else if ((HWND)lParam == jatisResourceId) // filter jati
			{
				FilterCombo((HWND)lParam, Jatis, ListItem, TRUE, NUMBER_OF_JATIS);
			}
			else if ((HWND)lParam == vadiSwarResourceId) // filter vadi swar
			{
				FilterCombo((HWND)lParam, Swaras, ListItem, TRUE, NUMBER_OF_SWARAS);
			}
			else if ((HWND)lParam == samvadiSwarResourceId) // filter samvadi swar
			{
				FilterCombo((HWND)lParam, Swaras, ListItem, TRUE, NUMBER_OF_SWARAS);
			}
			else if ((HWND)lParam == samayResourceId) // filter samayas
			{
				FilterCombo(samayResourceId, Samayas, ListItem, TRUE, NUMBER_OF_SAMAYAS);
			}

			break;
		case CBN_EDITCHANGE: // this message is called after editupdate message
			
			break;
		}
		switch (LOWORD(wParam))
		{
		case ID_THAATID:
			//ShowInfo(hDlg, TEXT("thaat"));
			
			break;
		case IDOK:
			// here RaagData structure will get filled with the dialog data
			isSuccess = ValidateAndPopulateRaag(hDlg, &raagData);
			
			if (!isSuccess)
			{
				return FALSE;
			}

			data[0] = { raagData };
			// 1 for write
			PerformFileIOForRaag(fileName, 1, FILE_APPEND_DATA, data);
			// re-init raag data to null now
			raagData = { 0 };
			
			// delete objects
			DeleteObject(hBrushGreenBg);
			DeleteObject(hBrushRedBg);
			DeleteObject(hBrushWhiteBg);
			// close the window now
			DestroyWindow(hDlg);
			break;
		case IDCANCEL:
			// delete objects
			DeleteObject(hBrushGreenBg);
			DeleteObject(hBrushRedBg);
			DeleteObject(hBrushWhiteBg);

			EndDialog(hDlg, 0);
			return TRUE;
		}
		break;

	
	}
	return FALSE;
}


INT_PTR CALLBACK ShowRecordsDlg(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG:
		// open records file
		//MessageBox(NULL, TEXT("first arrived dialog"), TEXT("Error"), MB_OK | MB_ICONINFORMATION);
		return TRUE;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			// get first name
			EndDialog(hDlg, 0);
			break;
		case IDCANCEL:
			EndDialog(hDlg, 0);
			return TRUE;
		}
		break;
	}
	return FALSE;
}

INT_PTR CALLBACK ShowRaagDlg(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	// function prototype declarations
	void SD_OnHVScroll(HWND hwnd, int bar, UINT code);

	// local variables
	RaagData raag = { 0 };
	int rowsAffected = 0;

	switch (message)
	{
	case WM_INITDIALOG:
		//if(raagData != NULL)
		{
			raag = raagData[itemNumberFromListView];
			SetDlgItemTextA(hwnd, ID_RAAGNAME_LBL_VAL, raag.name);
			SetDlgItemTextA(hwnd, ID_THAAT_LBL_VAL, raag.thaat);
			SetDlgItemTextA(hwnd, ID_JATI_LBL_VAL, raag.jaati);
			SetDlgItemTextA(hwnd, ID_SAMAY_LBL_VAL, raag.samay);
			SetDlgItemTextA(hwnd, ID_VARJYASWAR_LBL_VAL, raag.varjyaSwar);
			SetDlgItemTextA(hwnd, ID_VADISWAR_LBL_VAL, raag.vadiSwar);
			SetDlgItemTextA(hwnd, ID_SAMWADISWAR_LBL_VAL, raag.samvadiSwar);
			SetDlgItemTextA(hwnd, ID_AAROH_LBL_VAL, raag.aaroh);
			SetDlgItemTextA(hwnd, ID_AVROH_LBL_VAL, raag.avroh);
			SetDlgItemTextA(hwnd, ID_PAKAD_LBL_VAL, raag.pakad);
			SetDlgItemTextA(hwnd, ID_DESC_LBL_VAL, raag.desc);
			SetDlgItemTextA(hwnd, ID_SWAR_VISTAR_STHAYI_LBL_VAL, raag.swarVistar.sthayi);
			SetDlgItemTextA(hwnd, ID_SWAR_VISTAR_ANTARA_LBL_VAL, raag.swarVistar.antara);
			SetDlgItemTextA(hwnd, ID_AALAPI_STHAYI_LBL_VAL, raag.aalap.sthayi);
			SetDlgItemTextA(hwnd, ID_AALAPI_ANTARA_LBL_VAL, raag.aalap.antara);
			SetDlgItemTextA(hwnd, ID_TAANA_STHAYI_LBL_VAL, raag.taana.sthayi);
			SetDlgItemTextA(hwnd, ID_TAANA_ANTARA_LBL_VAL, raag.taana.antara);
		}
		//MessageBox(NULL, TEXT("ShowRaagDlg: first arrived dialog"), TEXT("Message"), MB_OK | MB_ICONINFORMATION);
		return TRUE;
	//case WM_MOUSEWHEEL:
	case WM_VSCROLL:
		//ShowInfo(hDlg, TEXT("Sample Scroll"));
		SD_OnHVScroll(hwnd, SB_VERT, LOWORD(wParam));
		break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			
			EndDialog(hwnd, 0);
			break;
		case IDCANCEL:
			
			EndDialog(hwnd, 0);
			return TRUE;
		}
		break;
	}
	return FALSE;
}

void OpenRecordsFile(HANDLE *lhFile, DWORD operation)
{
	*lhFile = CreateFile(
		fileName, // name of the file
		operation, // open for writing - FILE_APPEND_DATA
		0, // do not share
		NULL, // default security
		OPEN_ALWAYS, // create new file if doesnot exists
		FILE_ATTRIBUTE_NORMAL,// normal file
		NULL // no attribute template
	);

	if (*lhFile == INVALID_HANDLE_VALUE)
	{
		// show error
		ShowError(NULL, TEXT("Can't open file for reading"));
		exit(0); // terminate the program
	}
	//ShowInfo(NULL, TEXT("File Opened Successfully"));
}

/*
	ShowError:
	Show message box for error
*/
void ShowError(HWND hwnd, LPCWSTR message)
{
	MessageBox(hwnd, message, TEXT("Error"), MB_OK | MB_ICONERROR);
}

/*
	ShowInfo:
	Show message box for information
*/
void ShowInfo(HWND hwnd, LPCWSTR message)
{
	MessageBox(hwnd, message, TEXT("Message"), MB_OK | MB_ICONINFORMATION);
}

/*
	FileIOCompletionRoutine:
	This is a callback function for file reading completion
*/
VOID CALLBACK FileIOCompletionRoutine(__in DWORD dwErrorCode, __in DWORD dwNumberOfBytesTransffered, __in LPOVERLAPPED lpOverlapped)
{
	//ShowError(NULL, (LPCWSTR)dwErrorCode);
	//ShowError(NULL, (LPCWSTR)dwNumberOfBytesTransffered);
	g_BytesTransferred = dwNumberOfBytesTransffered;
}

/*
	Do the file operation as stated in the parameters
*/
VOID PerformFileIO(LPCWSTR fileName, DWORD opType, DWORD operation, char* DataBuffer)
{
	// function prototype declarations
	void OpenRecordsFile(HANDLE * lhFile, DWORD operation);
	VOID WriteDataA(HANDLE lhFile, char* DataBuffer);
	VOID ReadDataA(HANDLE lhFile, char* DataBuffer);
	VOID CloseRecordFile(HANDLE lhFile);

	// local variables
	HANDLE lhFile = NULL;
	

	// code
	OpenRecordsFile(&lhFile, operation);

	if(opType==1) // write
	{
		WriteDataA(lhFile, DataBuffer);
	}
	else if (opType == 0) // read
	{
		ReadDataA(lhFile, DataBuffer);
	}

	// close handle once you are done with the operation
	CloseRecordFile(lhFile);
}

VOID WriteDataA(HANDLE lhFile, char* DataBuffer)
{
	// local variables
	BOOL lbErrorFlag = FALSE;
	DWORD dwBytesToWrite = strlen(DataBuffer);
	DWORD dwBytesWritten = 0;

	// write data to file
	lbErrorFlag = WriteFile(
		lhFile, // file handle
		DataBuffer, // data to write
		dwBytesToWrite, // number of bytes to write
		&dwBytesWritten, // number of bytes that were written
		NULL // no overlap structure
	);
	// check if data is written successfully
	if (lbErrorFlag == FALSE)
	{
		ShowError(NULL, TEXT("Data not written successfully"));
	}
	else
	{
		if (dwBytesWritten != dwBytesToWrite)
		{
			ShowError(NULL, TEXT("Data not correctly written"));
		}
		else
		{
			ShowInfo(NULL, TEXT("Data written successfully"));
		}

	}
}


VOID WriteDataW(HANDLE lhFile, TCHAR* DataBuffer)
{
	// local variables
	BOOL lbErrorFlag = FALSE;
	DWORD dwBytesToWrite = wcslen(DataBuffer);
	DWORD dwBytesWritten = 0;

	// write data to file
	lbErrorFlag = WriteFile(
		lhFile, // file handle
		DataBuffer, // data to write
		dwBytesToWrite, // number of bytes to write
		&dwBytesWritten, // number of bytes that were written
		NULL // no overlap structure
	);
	// check if data is written successfully
	if (lbErrorFlag == FALSE)
	{
		ShowError(NULL, TEXT("Data not written successfully"));
	}
	else
	{
		if (dwBytesWritten != dwBytesToWrite)
		{
			ShowError(NULL, TEXT("Data not correctly written"));
		}
		else
		{
			ShowInfo(NULL, TEXT("Data written successfully"));
		}

	}
}

VOID ReadDataA(HANDLE lhFile, char* DataBuffer)
{
	// local variables
	OVERLAPPED ol = { 0 };

	// code
	/*
		The Windows API function ReadFile() reads bytes, an unsigned char,
		and not the Windows UNICODE sized TCHAR which in modern Windows is a two byte
		and not a one byte as in Windows 95
	*/
	if (ReadFileEx(lhFile, DataBuffer, BUFFERSIZE - 1, &ol, FileIOCompletionRoutine) == FALSE)
	{
		ShowError(NULL, TEXT("Read Error Occured"));
		CloseHandle(lhFile);
		return;
	}

	SleepEx(5000, TRUE);
	dwBytesRead = g_BytesTransferred;
	if (dwBytesRead > 0 && dwBytesRead <= BUFFERSIZE - 1)
	{
		DataBuffer[dwBytesRead] = '\0';
		// MessageBoxA(NULL, DataBuffer, "Message", MB_OK | MB_ICONINFORMATION);
	}
	else if (dwBytesRead == 0)
	{
		ShowInfo(NULL, TEXT("No data read from file"));
	}
	else
	{
		ShowError(NULL, TEXT("Unexpected value read in dwBytesRead"));
	}
}

VOID ReadDataW(HANDLE lhFile, TCHAR* DataBuffer)
{
	// local variables
	OVERLAPPED ol = { 0 };

	// code
	/*
		The Windows API function ReadFile() reads bytes, an unsigned char, 
		and not the Windows UNICODE sized TCHAR which in modern Windows is a two byte 
		and not a one byte as in Windows 95
	*/
	if (ReadFileEx(lhFile, DataBuffer, BUFFERSIZE - 1, &ol, FileIOCompletionRoutine) == FALSE)
	{
		ShowError(NULL, TEXT("Read Error Occured"));
		CloseHandle(lhFile);
		return;
	}

	SleepEx(5000, TRUE);
	dwBytesRead = g_BytesTransferred;
	if (dwBytesRead > 0 && dwBytesRead <= BUFFERSIZE - 1)
	{
		DataBuffer[dwBytesRead] = '\0';
		MessageBoxW(NULL, DataBuffer, L"Message", MB_OK | MB_ICONINFORMATION);
	}
	else if (dwBytesRead == 0)
	{
		ShowInfo(NULL, TEXT("No data read from file"));
	}
	else
	{
		ShowError(NULL, TEXT("Unexpected value read in dwBytesRead"));
	}
}

VOID CloseRecordFile(HANDLE lhFile)
{
	CloseHandle(lhFile);
}

/*
	Filters the combobox as per the filter criteria
*/
BOOL FilterCombo(HWND hwnd, TCHAR filterList[][10], TCHAR* filterCriteria, BOOL showDropdown, int iterationLimit)
{
	// clear existing contents of the dropdown using  CB_RESETCONTENT
	SendMessage(hwnd, (UINT)CB_RESETCONTENT, (WPARAM)0, (LPARAM)0);
	
	// add contents from the list to dropdown
	for (int k = 0; k < iterationLimit; k += 1)
	{
		
		// compare the string in filter and add contents accordingly
		if (filterCriteria == NULL)
		{
			// Add string to combobox.
			SendMessage(hwnd, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)((TCHAR*)filterList[k]));
		}else if (wcsstr((TCHAR *)filterList[k], filterCriteria) != NULL)
		{
			// Add string to combobox.
			SendMessage(hwnd, (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM) ((TCHAR*)filterList[k]));
		}
	}
	// Send the CB_SETCURSEL message to display an initial item 
	//  in the selection field  
	SendMessage(hwnd, CB_SETCURSEL, (WPARAM)0, (LPARAM)0);

	// open dropdown
	SendMessage(hwnd, CB_SHOWDROPDOWN, (WPARAM)showDropdown, (LPARAM)0);
	return(TRUE);
}

void SD_OnHScroll(HWND hwnd, HWND /*hwndCtl*/, UINT code, int /*pos*/)
{
	void SD_OnHVScroll(HWND hwnd, int bar, UINT code);

	SD_OnHVScroll(hwnd, SB_HORZ, code);
}

void SD_OnVScroll(HWND hwnd, HWND /*hwndCtl*/, UINT code, int /*pos*/)
{
	void SD_OnHVScroll(HWND hwnd, int bar, UINT code);

	SD_OnHVScroll(hwnd, SB_VERT, code);
}

void SD_OnHVScroll(HWND hwnd, int bar, UINT code)
{
	void SD_ScrollClient(HWND hwnd, int bar, int pos);
	int SD_GetScrollPos(HWND hwnd, int bar, UINT code);

	const int scrollPos = SD_GetScrollPos(hwnd, bar, code);

	if (scrollPos == -1)
		return;

	SetScrollPos(hwnd, bar, scrollPos, TRUE);
	SD_ScrollClient(hwnd, bar, scrollPos);
}

void SD_ScrollClient(HWND hwnd, int bar, int pos)
{
	static int s_prevx = 1;
	static int s_prevy = 1;

	int cx = 0;
	int cy = 0;

	int& delta = (bar == SB_HORZ ? cx : cy);
	int& prev = (bar == SB_HORZ ? s_prevx : s_prevy);

	delta = prev - pos;
	prev = pos;

	if (cx || cy)
	{
		ScrollWindow(hwnd, cx, cy*4, NULL, NULL);
	}
}

int SD_GetScrollPos(HWND hwnd, int bar, UINT code)
{
	SCROLLINFO si = {};
	si.cbSize = sizeof(SCROLLINFO);
	si.fMask = SIF_PAGE | SIF_POS | SIF_RANGE | SIF_TRACKPOS;
	GetScrollInfo(hwnd, bar, &si);

	const int minPos = si.nMin;
	const int maxPos = si.nMax - (si.nPage - 1);
	
	int result = -1;

	switch (code)
	{
	case SB_LINEUP /*SB_LINELEFT*/:
		result = max(si.nPos - 1, minPos);
		break;

	case SB_LINEDOWN /*SB_LINERIGHT*/:
		result = min(si.nPos + 1, maxPos);
		break;

	case SB_PAGEUP /*SB_PAGELEFT*/:
		result = max(si.nPos - (int)si.nPage, minPos);
		break;

	case SB_PAGEDOWN /*SB_PAGERIGHT*/:
		result = min(si.nPos + (int)si.nPage, maxPos);
		break;

	case SB_THUMBPOSITION:
		// do nothing
		break;

	case SB_THUMBTRACK:
		result = si.nTrackPos;
		break;

	case SB_TOP /*SB_LEFT*/:
		result = minPos;
		break;

	case SB_BOTTOM /*SB_RIGHT*/:
		result = maxPos;
		break;

	case SB_ENDSCROLL:
		// do nothing
		break;
	}

	return result;
}

BOOL ValidateAndPopulateRaag(HWND hDlg, RaagData* raagData)
{
	// function prototype declaration
	BOOL validateSwaras(char* input, TCHAR lSwaras[][10], int iterationLimit);

	// code
	// populate data
	// get raag name
	GetDlgItemTextA(hDlg, ID_RAAGNAMEID, raagData->name, 10);
	// get thaat
	GetDlgItemTextA(hDlg, ID_THAATID, raagData->thaat, 10);
	// get jati
	GetDlgItemTextA(hDlg, ID_JATI, raagData->jaati, 20);
	// get samay
	GetDlgItemTextA(hDlg, ID_SAMAY, raagData->samay, 20);
	// get varjya swar
	GetDlgItemTextA(hDlg, ID_VARJYASWAR, raagData->varjyaSwar, 20);
	// get vadi swar
	GetDlgItemTextA(hDlg, ID_VADISWAR, raagData->vadiSwar, 10);
	// get samvadi swar
	GetDlgItemTextA(hDlg, ID_SAMWADISWAR, raagData->samvadiSwar, 10);
	// get aaroh
	GetDlgItemTextA(hDlg, ID_AAROH, raagData->aaroh, 50);
	// get avroh
	GetDlgItemTextA(hDlg, ID_AVROH, raagData->avroh, 50);
	// get pakad
	GetDlgItemTextA(hDlg, ID_PAKAD, raagData->pakad, 200);
	//MessageBoxA(NULL, raagData->pakad, "Pakad", MB_OK);
	// get description
	GetDlgItemTextA(hDlg, ID_DESC, raagData->desc, 500);
	// get swarvistar
	GetDlgItemTextA(hDlg, ID_SWAR_VISTAR_STHAYI, raagData->swarVistar.sthayi, 500);
	GetDlgItemTextA(hDlg, ID_SWAR_VISTAR_ANTARA, raagData->swarVistar.antara, 500);
	// get aalapi
	GetDlgItemTextA(hDlg, ID_AALAPI_STHAYI, raagData->aalap.sthayi, 500);
	GetDlgItemTextA(hDlg, ID_AALAPI_ANTARA, raagData->aalap.antara, 500);
	// get taana
	GetDlgItemTextA(hDlg, ID_TAANA_STHAYI, raagData->taana.sthayi, 500);
	GetDlgItemTextA(hDlg, ID_TAANA_ANTARA, raagData->taana.antara, 500);
	// perform validations
	if (gbPerformValidation)
	{
		if (strlen(raagData->name) == 0)
		{
			gbMarkRed = TRUE;
			ghRedWindowHandle = GetDlgItem(hDlg, ID_RAAGNAMEID);
			ShowError(hDlg, TEXT("Name is mandatory"));
			return FALSE;
		}

		if (strlen(raagData->thaat) == 0)
		{
			ShowError(hDlg, TEXT("Thaat is mandatory"));
			return FALSE;
		}

		if (strlen(raagData->jaati) == 0)
		{
			ShowError(hDlg, TEXT("Jaati is mandatory"));
			return FALSE;
		}
		if (strlen(raagData->samay) == 0)
		{
			ShowError(hDlg, TEXT("Samay is mandatory"));
			return FALSE;
		}
		if (strlen(raagData->varjyaSwar) == 0)
		{
			ShowError(hDlg, TEXT("Varjya Swar is mandatory"));
			return FALSE;
		}
		else if (validateSwaras(raagData->varjyaSwar, Swaras, NUMBER_OF_SWARAS) == FALSE)
		{
			gbMarkRed = TRUE;
			ghRedWindowHandle = GetDlgItem(hDlg, ID_VARJYASWAR);
			ShowError(hDlg, TEXT("Enter Proper Varjya Swaras"));
			return FALSE;
		}
		if (strlen(raagData->vadiSwar) == 0)
		{
			ShowError(hDlg, TEXT("Vadi Swar is mandatory"));
			return FALSE;
		}
		if (strlen(raagData->samvadiSwar) == 0)
		{
			ShowError(hDlg, TEXT("Samvadi Swar is mandatory"));
			return FALSE;
		}

		if (strlen(raagData->aaroh) == 0)
		{
			gbMarkRed = TRUE;
			ghRedWindowHandle = GetDlgItem(hDlg, ID_AAROH);
			ShowError(hDlg, TEXT("Aaroh is mandatory"));
			return FALSE;
		}
		else if (validateSwaras(raagData->aaroh, Swaras, NUMBER_OF_SWARAS) == FALSE)
		{
			gbMarkRed = TRUE;
			ghRedWindowHandle = GetDlgItem(hDlg, ID_AAROH);
			ShowError(hDlg, TEXT("Enter Proper Aaroh Swaras"));
			return FALSE;
		}

		if (strlen(raagData->avroh) == 0)
		{
			gbMarkRed = TRUE;
			ghRedWindowHandle = GetDlgItem(hDlg, ID_AVROH);
			ShowError(hDlg, TEXT("Avroh is mandatory"));
			return FALSE;
		}
		else if (validateSwaras(raagData->avroh, Swaras, NUMBER_OF_SWARAS) == FALSE)
		{
			gbMarkRed = TRUE;
			ghRedWindowHandle = GetDlgItem(hDlg, ID_AVROH);
			ShowError(hDlg, TEXT("Enter Proper Avroh Swaras"));
			return FALSE;
		}
		if (strlen(raagData->pakad) == 0)
		{
			gbMarkRed = TRUE;
			ghRedWindowHandle = GetDlgItem(hDlg, ID_PAKAD);
			ShowError(hDlg, TEXT("Pakad is mandatory"));
			return FALSE;
		}
		else if (validateSwaras(raagData->pakad, Swaras, NUMBER_OF_SWARAS) == FALSE)
		{
			gbMarkRed = TRUE;
			ghRedWindowHandle = GetDlgItem(hDlg, ID_PAKAD);
			ShowError(hDlg, TEXT("Enter Proper Pakad Swaras"));
			return FALSE;
		}
		if (strlen(raagData->desc) == 0)
		{
			ShowError(hDlg, TEXT("Description is mandatory"));
			return FALSE;
		}
		if (strlen(raagData->swarVistar.sthayi) == 0)
		{
			gbMarkRed = TRUE;
			ghRedWindowHandle = GetDlgItem(hDlg, ID_SWAR_VISTAR_STHAYI);
			ShowError(hDlg, TEXT("SwarVistar: Sthayi is mandatory"));
			return FALSE;
		}
		else if (validateSwaras(raagData->swarVistar.sthayi, Swaras, NUMBER_OF_SWARAS) == FALSE)
		{
			gbMarkRed = TRUE;
			ghRedWindowHandle = GetDlgItem(hDlg, ID_SWAR_VISTAR_STHAYI);
			ShowError(hDlg, TEXT("SwarVishar: Enter Proper Sthayi Swaras"));
			return FALSE;
		}
		if (strlen(raagData->swarVistar.antara) == 0)
		{
			gbMarkRed = TRUE;
			ghRedWindowHandle = GetDlgItem(hDlg, ID_SWAR_VISTAR_ANTARA);
			ShowError(hDlg, TEXT("SwarVistar: Antara is mandatory"));
			return FALSE;
		}
		else if (validateSwaras(raagData->swarVistar.antara, Swaras, NUMBER_OF_SWARAS) == FALSE)
		{
			gbMarkRed = TRUE;
			ghRedWindowHandle = GetDlgItem(hDlg, ID_SWAR_VISTAR_ANTARA);
			ShowError(hDlg, TEXT("SwarVishar: Enter Proper Antara Swaras"));
			return FALSE;
		}
		if (strlen(raagData->aalap.sthayi) == 0)
		{
			gbMarkRed = TRUE;
			ghRedWindowHandle = GetDlgItem(hDlg, ID_AALAPI_STHAYI);
			ShowError(hDlg, TEXT("Aalapi: Sthayi is mandatory"));
			return FALSE;
		}
		else if (validateSwaras(raagData->aalap.sthayi, Swaras, NUMBER_OF_SWARAS) == FALSE)
		{
			gbMarkRed = TRUE;
			ghRedWindowHandle = GetDlgItem(hDlg, ID_AALAPI_STHAYI);
			ShowError(hDlg, TEXT("Aalap: Enter Proper Sthayi Swaras"));
			return FALSE;
		}
		if (strlen(raagData->aalap.antara) == 0)
		{
			gbMarkRed = TRUE;
			ghRedWindowHandle = GetDlgItem(hDlg, ID_AALAPI_ANTARA);
			ShowError(hDlg, TEXT("Aalapi: Antara is mandatory"));
			return FALSE;
		}
		else if (validateSwaras(raagData->aalap.antara, Swaras, NUMBER_OF_SWARAS) == FALSE)
		{
			gbMarkRed = TRUE;
			ghRedWindowHandle = GetDlgItem(hDlg, ID_AALAPI_ANTARA);
			ShowError(hDlg, TEXT("Aalap: Enter Proper Antara Swaras"));
			return FALSE;
		}
		if (strlen(raagData->taana.sthayi) == 0)
		{
			gbMarkRed = TRUE;
			ghRedWindowHandle = GetDlgItem(hDlg, ID_TAANA_STHAYI);
			ShowError(hDlg, TEXT("Taana: Sthayi is mandatory"));
			return FALSE;
		}
		else if (validateSwaras(raagData->swarVistar.sthayi, Swaras, NUMBER_OF_SWARAS) == FALSE)
		{
			gbMarkRed = TRUE;
			ghRedWindowHandle = GetDlgItem(hDlg, ID_TAANA_STHAYI);
			ShowError(hDlg, TEXT("Taana: Enter Proper Sthayi Swaras"));
			return FALSE;
		}
		if (strlen(raagData->taana.antara) == 0)
		{
			gbMarkRed = TRUE;
			ghRedWindowHandle = GetDlgItem(hDlg, ID_TAANA_ANTARA);
			ShowError(hDlg, TEXT("Taana: Antara is mandatory"));
			return FALSE;
		}
		else if (validateSwaras(raagData->taana.antara, Swaras, NUMBER_OF_SWARAS) == FALSE)
		{
			gbMarkRed = TRUE;
			ghRedWindowHandle = GetDlgItem(hDlg, ID_TAANA_ANTARA);
			ShowError(hDlg, TEXT("Taana: Enter Proper Antara Swaras"));
			return FALSE;
		}

	}
	return TRUE;
}

int extractRaagDataFromBuffer(char* szRaagBuffer, RaagData* RaagDataBuffer)
{
	char delim_bar[] = "|";
	char* bufferLine;
	int counter = 0;
	// First tokenize on line
	char* eachLine = strtok_s(szRaagBuffer, // actual data
		SEP_RAAG, // delimeter
		&bufferLine // remaining data
	);

	while (eachLine != NULL)
	{
		RaagData DataBuffer = { 0 };
		char* buffer;
		// now tokenize each line on bar
		char * token = strtok_s(eachLine, // actual data
			delim_bar, // delimeter
			&buffer // remaining data
		);
		// copy raag name
		strcpy_s(DataBuffer.name, token);

		// thaat
		token = strtok_s(NULL, // actual data
			delim_bar, // delimeter
			&buffer // remaining data
		);
		strcpy_s(DataBuffer.thaat, token);

		// jaati
		token = strtok_s(NULL, // actual data
			delim_bar, // delimeter
			&buffer // remaining data
		);
		strcpy_s(DataBuffer.jaati, token);

		// samay
		token = strtok_s(NULL, // actual data
			delim_bar, // delimeter
			&buffer // remaining data
		);
		strcpy_s(DataBuffer.samay, token);

		// varjya swar
		token = strtok_s(NULL, // actual data
			delim_bar, // delimeter
			&buffer // remaining data
		);
		strcpy_s(DataBuffer.varjyaSwar, token);

		// vadi swar
		token = strtok_s(NULL, // actual data
			delim_bar, // delimeter
			&buffer // remaining data
		);
		strcpy_s(DataBuffer.vadiSwar, token);

		// samvadi swar
		token = strtok_s(NULL, // actual data
			delim_bar, // delimeter
			&buffer // remaining data
		);
		strcpy_s(DataBuffer.samvadiSwar, token);

		// aaroh
		token = strtok_s(NULL, // actual data
			delim_bar, // delimeter
			&buffer // remaining data
		);
		strcpy_s(DataBuffer.aaroh, token);

		// avroh
		token = strtok_s(NULL, // actual data
			delim_bar, // delimeter
			&buffer // remaining data
		);
		strcpy_s(DataBuffer.avroh, token);

		// pakad
		token = strtok_s(NULL, // actual data
			delim_bar, // delimeter
			&buffer // remaining data
		);
		strcpy_s(DataBuffer.pakad, token);

		// description
		token = strtok_s(NULL, // actual data
			delim_bar, // delimeter
			&buffer // remaining data
		);
		strcpy_s(DataBuffer.desc, token);

		// swarVistar - sthayi
		token = strtok_s(NULL, // actual data
			delim_bar, // delimeter
			&buffer // remaining data
		);
		strcpy_s(DataBuffer.swarVistar.sthayi, token);

		// swarVistar - antara
		token = strtok_s(NULL, // actual data
			delim_bar, // delimeter
			&buffer // remaining data
		);
		strcpy_s(DataBuffer.swarVistar.antara, token);

		// aalapi - sthayi
		token = strtok_s(NULL, // actual data
			delim_bar, // delimeter
			&buffer // remaining data
		);
		strcpy_s(DataBuffer.aalap.sthayi, token);

		// aalapi - antara
		token = strtok_s(NULL, // actual data
			delim_bar, // delimeter
			&buffer // remaining data
		);
		strcpy_s(DataBuffer.aalap.antara, token);

		// taana - sthayi
		token = strtok_s(NULL, // actual data
			delim_bar, // delimeter
			&buffer // remaining data
		);
		strcpy_s(DataBuffer.taana.sthayi, token);

		// taana - antara
		token = strtok_s(NULL, // actual data
			delim_bar, // delimeter
			&buffer // remaining data
		);
		strcpy_s(DataBuffer.taana.antara, token);

		RaagDataBuffer[counter] = DataBuffer;
		counter++;
		eachLine = strtok_s(NULL, // actual data
			SEP_RAAG, // delimeter
			&bufferLine // remaining data
		);
	}
	return counter;
}

INT_PTR CALLBACK ShowRaagListViewDlg(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG:
		MessageBox(hwnd, TEXT("ShowRaagListViewDlg: first arrived dialog"), TEXT("Message"), MB_OK | MB_ICONINFORMATION);
		return TRUE;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			// get first name
			EndDialog(hwnd, 0);
			break;
		case IDCANCEL:
			EndDialog(hwnd, 0);
			return TRUE;
		}
		break;
	}
	return FALSE;
}

// CreateListView: Creates a list-view control in report view.
// Returns the handle to the new control
// TO DO:  The calling procedure should determine whether the handle is NULL, in case 
// of an error in creation.
//
// HINST hInst: The global handle to the applicadtion instance.
// HWND  hWndParent: The handle to the control's parent window. 
//
HWND CreateListView(HWND hwndParent)
{
	INITCOMMONCONTROLSEX icex;           // Structure for control initialization.
	icex.dwICC = ICC_LISTVIEW_CLASSES;
	InitCommonControlsEx(&icex);

	RECT rcClient;                       // The parent window's client area.

	GetClientRect(hwndParent, &rcClient);

	// Create the list-view window in report view with label editing enabled.
	HWND hWndListView = CreateWindow(WC_LISTVIEW,
		L"Raag List",
		WS_CHILD | LVS_REPORT | LVS_EDITLABELS | WS_VISIBLE,
		//WS_CHILD | WS_VISIBLE | LVS_REPORT | LVS_SINGLESEL,
		0, 0,
		rcClient.right - rcClient.left,
		rcClient.bottom - rcClient.top,
		hwndParent,
		NULL,
		ghInstance,
		NULL);

	return (hWndListView);
}

// SetView: Sets a list-view's window style to change the view.
// hWndListView: A handle to the list-view control. 
// dwView:       A value specifying the new view style.
//
VOID SetView(HWND hWndListView, DWORD dwView)
{
	// Retrieve the current window style. 
	DWORD dwStyle = GetWindowLong(hWndListView, GWL_STYLE);

	// Set the window style only if the view bits changed.
	if ((dwStyle & LVS_TYPEMASK) != dwView)
	{
		SetWindowLong(hWndListView,
			GWL_STYLE,
			(dwStyle & ~LVS_TYPEMASK) | dwView);
	}               // Logical OR'ing of dwView with the result of 
}                   // a bitwise AND between dwStyle and 
					// the Unary complenent of LVS_TYPEMASK.

// InitListViewColumns: Adds columns to a list-view control.
// hWndListView:        Handle to the list-view control. 
// Returns TRUE if successful, and FALSE otherwise. 
BOOL InitListViewColumns(HWND hWndListView)
{
	WCHAR szText[256];     // Temporary buffer.
	LVCOLUMN lvc;
	int iCol;

	// Initialize the LVCOLUMN structure.
	// The mask specifies that the format, width, text,
	// and subitem members of the structure are valid.
	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;

	// Add the columns.
	for (iCol = 0; iCol < C_COLUMNS; iCol++)
	{
		lvc.iSubItem = iCol;
		lvc.pszText = szText;
		lvc.cx = 100;               // Width of column in pixels.

		if (iCol < 2)
			lvc.fmt = LVCFMT_LEFT;  // Left-aligned column.
		else
			lvc.fmt = LVCFMT_RIGHT; // Right-aligned column.

		// Load the names of the column headings from the string resources.
		LoadString(ghInstance,
			IDS_FIRSTCOLUMN + iCol,
			szText,
			sizeof(szText) / sizeof(szText[0]));

		// Insert the columns into the list view.
		if (ListView_InsertColumn(hWndListView, iCol, &lvc) == -1)
			return FALSE;
	}

	return TRUE;
}


// InitListViewImageLists: Creates image lists for a list-view control.
// This function only creates image lists. It does not insert the items into
// the control, which is necessary for the control to be visible.   
//
// Returns TRUE if successful, or FALSE otherwise. 
//
// hWndListView: Handle to the list-view control. 
// global variable g_hInst: The handle to the module of either a 
// dynamic-link library (DLL) or executable (.exe) that contains
// the image to be loaded. If loading a standard or system
// icon, set g_hInst to NULL.
//
BOOL InitListViewImageLists(HWND hWndListView)
{
	HICON hiconItem;     // Icon for list-view items.
	HIMAGELIST hLarge;   // Image list for icon view.
	HIMAGELIST hSmall;   // Image list for other views.

	// Create the full-sized icon image lists. 
	hLarge = ImageList_Create(GetSystemMetrics(SM_CXICON),
		GetSystemMetrics(SM_CYICON),
		ILC_MASK, 1, 1);

	hSmall = ImageList_Create(GetSystemMetrics(SM_CXSMICON),
		GetSystemMetrics(SM_CYSMICON),
		ILC_MASK, 1, 1);

	// Add an icon to each image list.  
	hiconItem = LoadIcon(ghInstance, MAKEINTRESOURCE(CUSTICONSM));

	ImageList_AddIcon(hLarge, hiconItem);
	ImageList_AddIcon(hSmall, hiconItem);

	DestroyIcon(hiconItem);

	// When you are dealing with multiple icons, you can use the previous four lines of 
	// code inside a loop. The following code shows such a loop. The 
	// icons are defined in the application's header file as resources, which 
	// are numbered consecutively starting with IDS_FIRSTICON. The number of 
	// icons is defined in the header file as C_ICONS.
	/*
		for(index = 0; index < C_ICONS; index++)
		{
			hIconItem = LoadIcon (g_hinst, MAKEINTRESOURCE(IDS_FIRSTICON + index));
			ImageList_AddIcon(hSmall, hIconItem);
			ImageList_AddIcon(hLarge, hIconItem);
			Destroy(hIconItem);
		}
	*/

	// Assign the image lists to the list-view control. 
	ListView_SetImageList(hWndListView, hLarge, LVSIL_NORMAL);
	ListView_SetImageList(hWndListView, hSmall, LVSIL_SMALL);

	return TRUE;
}

// InsertListViewItems: Inserts items into a list view. 
// hWndListView:        Handle to the list-view control.
// cItems:              Number of items to insert.
// Returns TRUE if successful, and FALSE otherwise.
BOOL InsertListViewItems(HWND hWndListView, int cItems)
{
	LVITEM lvI;

	// Initialize LVITEM members that are common to all items.
	lvI.pszText = LPSTR_TEXTCALLBACK; // Sends an LVN_GETDISPINFO message.
	lvI.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_STATE;
	lvI.stateMask = 0;
	lvI.iSubItem = 0;
	lvI.state = 0;

	// Initialize LVITEM members that are different for each item.
	for (int index = 0; index < cItems; index++)
	{
		lvI.iItem = index;
		lvI.iImage = index;

		// Insert items into the list.
		if (ListView_InsertItem(hWndListView, &lvI) == -1)
			return FALSE;
	}

	return TRUE;
}

// HandleWM_NOTIFY - Handles the LVN_GETDISPINFO notification code that is 
//         sent in a WM_NOTIFY to the list view parent window. The function 
//        provides display strings for list view items and subitems.
//
// lParam - The LPARAM parameter passed with the WM_NOTIFY message.
void HandleWM_NOTIFY(HWND hwnd, LPARAM lParam)
{
	NMLVDISPINFO* plvdi;
	LPWSTR ptr = NULL;
	char* data = NULL;
	wchar_t wtext[500];
	size_t length = 0;
	LPNMITEMACTIVATE lpnmitem;
	char buf[50];
	switch (((LPNMHDR)lParam)->code)
	{
	case NM_DBLCLK:
		//ShowInfo(NULL, TEXT("NM_DBLCLK"));
		lpnmitem = (LPNMITEMACTIVATE)lParam;
		//sprintf_s(buf, "Item is %d", lpnmitem->iItem);
		itemNumberFromListView = lpnmitem->iItem;
		//MessageBoxA(NULL, buf, "", MB_OK);
		if(itemNumberFromListView >= 0)
			hDlgShowRaag = CreateDialog(ghInstance, TEXT("ShowRaagDlg"), hwnd, ShowRaagDlg);
		break;
	case LVN_GETDISPINFO:

		plvdi = (NMLVDISPINFO*)lParam;

		switch (plvdi->item.iSubItem)
		{
		case 0:
			data = raagData[plvdi->item.iItem].name;
			length = strlen(data) + 1;
			mbstowcs_s(&length, wtext, data, length);//Plus null
			ptr = wtext;
			plvdi->item.pszText = ptr;
			break;

		case 1:
			data = raagData[plvdi->item.iItem].thaat;
			length = strlen(data) + 1;
			mbstowcs_s(&length, wtext, data, length);//Plus null
			ptr = wtext;
			plvdi->item.pszText = ptr;
			break;

		case 2:
			data = raagData[plvdi->item.iItem].jaati;
			length = strlen(data) + 1;
			mbstowcs_s(&length, wtext, data, length);//Plus null
			ptr = wtext;
			plvdi->item.pszText = ptr;
			break;
		case 3:
			data = raagData[plvdi->item.iItem].samay;
			length = strlen(data) + 1;
			mbstowcs_s(&length, wtext, data, length);//Plus null
			ptr = wtext;
			plvdi->item.pszText = ptr;
			break;

		case 4:
			data = raagData[plvdi->item.iItem].varjyaSwar;
			length = strlen(data) + 1;
			mbstowcs_s(&length, wtext, data, length);//Plus null
			ptr = wtext;
			plvdi->item.pszText = ptr;
			break;

		case 5:
			data = raagData[plvdi->item.iItem].vadiSwar;
			length = strlen(data) + 1;
			mbstowcs_s(&length, wtext, data, length);//Plus null
			ptr = wtext;
			plvdi->item.pszText = ptr;
			break;

		case 6:
			data = raagData[plvdi->item.iItem].samvadiSwar;
			length = strlen(data) + 1;
			mbstowcs_s(&length, wtext, data, length);//Plus null
			ptr = wtext;
			plvdi->item.pszText = ptr;
			break;

		case 7:
			data = raagData[plvdi->item.iItem].aaroh;
			length = strlen(data) + 1;
			mbstowcs_s(&length, wtext, data, length);//Plus null
			ptr = wtext;
			plvdi->item.pszText = ptr;
			break;

		case 8:
			data = raagData[plvdi->item.iItem].avroh;
			length = strlen(data) + 1;
			mbstowcs_s(&length, wtext, data, length);//Plus null
			ptr = wtext;
			plvdi->item.pszText = ptr;
			break;

		case 9:
			data = raagData[plvdi->item.iItem].pakad;
			length = strlen(data) + 1;
			mbstowcs_s(&length, wtext, data, length);//Plus null
			ptr = wtext;
			plvdi->item.pszText = ptr;
			break;

		case 10:
			data = raagData[plvdi->item.iItem].desc;
			length = strlen(data) + 1;
			mbstowcs_s(&length, wtext, data, length);//Plus null
			ptr = wtext;
			plvdi->item.pszText = ptr;
			break;
		default:
			break;
		}

		break;

	}
	// NOTE: In addition to setting pszText to point to the item text, you could 
	// copy the item text into pszText using StringCchCopy. For example:
	//
	// StringCchCopy(plvdi->item.pszText, 
	//                         plvdi->item.cchTextMax, 
	//                         rgPetInfo[plvdi->item.iItem].szKind);

	return;
}

BOOL validateSwaras(char* input, TCHAR lSwaras[][10], int iterationLimit)
{
	// local variables
	BOOL result = FALSE;
	wchar_t wtext[10];
	const char* SEP_SPACE = " ";
	const char* SEP_NEW_LINE = "\r\n"; // new line character on windows only

	char* buffer = NULL;
	char* bufferLine = NULL;
	char* token = NULL;
	size_t length = 0;
	// code
	// First tokenize on line
	char* eachLine = strtok_s(input, // actual data
		SEP_NEW_LINE, // delimeter
		&bufferLine // remaining data
	);

	

	while (eachLine != NULL)
	{
		// tokenize on space
		token = strtok_s(eachLine, SEP_SPACE, &buffer);

		// loop till we have tokens
		while (token != NULL)
		{
			//MessageBoxA(NULL, token, "line token", MB_OK);
			// convert char * to wchar_t
			length = strlen(token) + 1;
			mbstowcs_s(&length, wtext, token, length);//Plus null
			// check if token(swar) is in the swaras list or not
			for (int k = 0; k < iterationLimit; k += 1)
			{
				// compare the string in filter and add contents accordingly
				if (wcsstr((TCHAR*)lSwaras[k], wtext) != NULL)
				{
					result = TRUE;
					break;
				}
			}

			if (!result)
			{
				// since we have not got the match of the swara
				// so we are returning FALSE here as it will indicate that the matching swara not found
				return FALSE;
			}
			else
			{
				// this is resetting the flag to FALSE as we are going to get next token for matching
				result = FALSE;
			}
			token = strtok_s(NULL, SEP_SPACE, &buffer);
		}
		eachLine = strtok_s(NULL, // actual data
			SEP_NEW_LINE, // delimeter
			&bufferLine // remaining data
		);
	}
	return TRUE;
}