//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SplashScreen.rc

// Next default values for new objects
// 
#define CUSTICONSM 101 // icon on caption bar
#define MYBITMAP 103 // splash screen
#define IDM_APP_ABOUT 104
#define IDC_STATIC -1

#define ID_ETEMPID 201
#define ID_ETDEPTID 202
#define ID_ETFIRSTNAME		204
#define ID_ETLASTNAME		205
#define ID_ETEMAILADDRESS	206
#define ID_ETCONTACT		207
#define ID_RBMARRIED	210
#define ID_RBUNMARRIED	211
#define ID_PBCONTINUE	212
#define ID_ETDOBDAY 213
#define ID_ETDOBMONTH 214
#define ID_ETDOBYEAR 215
#define ID_ETDOJDAY 216
#define ID_ETDOJMONTH 217
#define ID_ETDOJYEAR 218
#define ID_ETGENDER 210

#define ID_SHOW_RECORDS 219
#define ID_ADD_RECORD  220

#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define MYICON 100 // icon on explorer/taskmenu
#define CUSTCURSOR 102 // cursor

#endif
#endif
