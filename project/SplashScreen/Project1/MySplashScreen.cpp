//Headers
#include <windows.h>
#include "resource.h"



//Global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	//variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("MyApp");
	

	//code
	//inialize WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, TEXT("MYICON"));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance,TEXT("MYICON"));

	
	//Register above class
	RegisterClassEx(&wndclass);

	//Create Window
	hwnd = CreateWindow(szAppName,
		TEXT("Hello World Application"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	
	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	//message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return ((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	//Code
	HINSTANCE hInstanceBM;
	HDC hdc,mDC;
	PAINTSTRUCT ps;
	RECT rc;
	TCHAR str[] = TEXT("Hello World !!!");
	static HBITMAP hBitMap;
	BITMAP bitmap;
	
	switch (iMsg)
	{
	case WM_CREATE:
		hInstanceBM = (HINSTANCE)GetModuleHandle(NULL);
		hBitMap = LoadBitmap(hInstanceBM, MAKEINTRESOURCE(MYBITMAP));
	case WM_PAINT:
	{
		
		//Get changing rectangle
		GetClientRect(hwnd, &rc);
		//Call Specialist i.e. Create hdc
		hdc = BeginPaint(hwnd, &ps);

		mDC=CreateCompatibleDC(NULL);
		SelectObject(mDC, hBitMap); //hBitMap is attached to mDC
		GetObject(hBitMap, sizeof(BITMAP), &bitmap);
		
		StretchBlt(hdc,
			rc.left,
			rc.top,
			rc.right - rc.left,
			rc.bottom - rc.top,
			mDC,
			0,
			0,
			bitmap.bmWidth,
			bitmap.bmHeight,
			SRCCOPY
		);

		DeleteObject(mDC);
		EndPaint(hwnd, &ps);

		//Set Text color
		//SetTextColor(hdc, RGB(0, 255, 0));
		//Set Background color
		//SetBkColor(hdc, RGB(0, 0, 0));
		//Draw actaul Text
		DrawText(hdc, str, -1, &rc, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
		//Send back specialist and stop painting
		EndPaint(hwnd, &ps);
		break;
	}

	case WM_DESTROY:
	{
		PostQuitMessage(0);
		break;
	}
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}


