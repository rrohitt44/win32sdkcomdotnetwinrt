#pragma once

#include<Windows.h>
#include<stdio.h>
struct EmployeeData
{
	TCHAR szBufferEmpId[20];
	TCHAR szBufferDeptId[20];
	TCHAR szBufferFirstName[20];
	TCHAR szBufferLastName[20];
	TCHAR szBufferGender[20];
	TCHAR szBufferContactNumber[20];
	TCHAR szBufferEmailAddress[20];
	TCHAR szBufferDobDay[20];
	TCHAR szBufferDobMonth[20];
	TCHAR szBufferDobYear[20];
	TCHAR szBufferDojDay[20];
	TCHAR szBufferDojMonth[20];
	TCHAR szBufferDojYear[20];
};

class IRecord : public IUnknown
{
public:
	// IRecord specific method declarations
	virtual HRESULT __stdcall WriteRecord(EmployeeData*)=0;
	virtual HRESULT __stdcall ReadRecords(EmployeeData*, int*)=0;
};

// CLSID of RecordIO Component - {E666339E-006C-41FE-AB43-19E4E4C35E7B}
const CLSID CLSID_RecordIO =
{ 0xe666339e, 0x6c, 0x41fe, 0xab, 0x43, 0x19, 0xe4, 0xe4, 0xc3, 0x5e, 0x7b };

// IID of IRecord Component - {58171AB6-CFB9-4C4C-98D2-2DF8D5DB725D}
const IID IID_IRecord =
{ 0x58171ab6, 0xcfb9, 0x4c4c, 0x98, 0xd2, 0x2d, 0xf8, 0xd5, 0xdb, 0x72, 0x5d };

