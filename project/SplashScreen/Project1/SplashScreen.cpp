﻿// headers
#include<Windows.h>

#include"resource.h"
#include<CommCtrl.h>
#include<stdio.h>
#include<fcntl.h>
#include<io.h>
#include"ComServer.h"

#define UNICODE

#pragma comment(lib, "comctl32.lib")

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK AboutDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK ColorScrDlg(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK ShowControlsDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK ShowRecordsDlg(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
void addFileDataToControls(HWND hwnd, HWND hDlg); // to add data from file to the dialog controls
void addFileDataToControls_BKP(HWND hwnd, HWND hDlg);
int Create2ColItem(HWND hwndList, char* Text1, char* Text2);

HWND hDlgModeless;
HWND hDlgShowControls;
HWND hDlgViewDetails;
HWND hwndList;
HINSTANCE ghInstance;
HRESULT hr;
IRecord* iRecord = NULL;
EmployeeData *dataRecords;
int size=0;

#include <windows.h>
struct { 
	int     iStyle;     
	const TCHAR* szText; 
} button[] = 
{ BS_PUSHBUTTON,      TEXT("Show Records"),     
BS_DEFPUSHBUTTON,   TEXT("Add Record"),     
//BS_CHECKBOX,        TEXT("CHECKBOX"),      
//BS_AUTOCHECKBOX,    TEXT("AUTOCHECKBOX"),     
//BS_RADIOBUTTON,     TEXT("RADIOBUTTON"),     
//BS_3STATE,          TEXT("3STATE"),     
//BS_AUTO3STATE,      TEXT("AUTO3STATE"),     
//BS_GROUPBOX,        TEXT("GROUPBOX"),     
//BS_AUTORADIOBUTTON, TEXT("AUTORADIO"),     
//BS_OWNERDRAW,       TEXT("OWNERDRAW") 
};

#define NUM (sizeof button / sizeof button[0])

// file IO

DWORD dwBytesWritten = 0;
HBRUSH g_hbrBackground = CreateSolidBrush(RGB(100, 149, 237));
static int incrementIndex = 0;

// WinMain()
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	//static TCHAR szAppName[] = TEXT("MyApp");
	static TCHAR szAppName[] = TEXT("About1");
	HWND hwnd; // handle to window
	MSG msg; // message

	// COM Initialization
	hr = CoInitialize(NULL);
	if (FAILED(hr))
	{
		MessageBox(NULL, TEXT("COM initialization failed.\nProgram will exit now."), TEXT("Error"), MB_OK);
		exit(0);
	}
	// code
	ghInstance = hInstance;
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc; // callback function
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, szAppName);
	wndclass.hCursor = LoadCursor(hInstance, TEXT("CUSTCURSOR"));
	wndclass.hbrBackground = (HBRUSH) GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = szAppName;
	wndclass.hIconSm = LoadIcon(hInstance, TEXT("CUSTICONSM"));

	// register above class
	RegisterClassEx(&wndclass);

	
	// create window
	hwnd = CreateWindow(
		szAppName, // lpClassName
		TEXT("My Application"), // lpWindowName
		WS_OVERLAPPEDWINDOW, // dwStyle
		CW_USEDEFAULT, // x
		CW_USEDEFAULT, // y
		CW_USEDEFAULT, // width
		CW_USEDEFAULT, // height
		NULL, // hWndParent
		NULL, // hMenu
		hInstance,
		NULL // lpParam
	);


	// show window
	ShowWindow(hwnd, iCmdShow); //iCmdShow
	
	// update window
	UpdateWindow(hwnd);

	//hDlgModeless = CreateDialog(hInstance, TEXT("ColorScrDlg"), hwnd, ColorScrDlg);


	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	// COM uninitialization
	CoUninitialize();
	return ((int) msg.wParam);
}

// WndProc: callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// local variable declarations
	HDC hdc, cdc;
	PAINTSTRUCT ps;
	RECT rc;
	TCHAR str[] = TEXT("Hello World!!!");
	TCHAR batch[] = TEXT("WinRT 2019");
	TCHAR studentName[] = TEXT("Name: Rohit Shamrao Muneshwar");
	TCHAR groupName[] = TEXT("Group: WM_MOVE");
	TCHAR openerMessage[] = TEXT("Press any key to continue");
	static int   cxChar, cyChar;
	static HWND  hwndButton[NUM];

	// for loading bitmap
	HINSTANCE hInstance;
	static HBITMAP hbitSS;
	BITMAP bitSS;
	static HINSTANCE hInstanceDialog;
	int i;
	static RECT  rect;
	static TCHAR szTop[] = TEXT("message            wParam       lParam"), 
		szUnd[] = TEXT("_______            ______       ______"), 
		szFormat[] = TEXT("%−16s%04X−%04X    %04X−%04X"), 
		szBuffer[50];
	

	// code
	switch (iMsg)
	{
	case WM_CREATE: // on window creation; entered only once and at the first time only here

		// fill the str
		hr = CoCreateInstance(CLSID_RecordIO, NULL, CLSCTX_INPROC_SERVER, IID_IRecord, (void**)&iRecord);
		if (FAILED(hr))
		{
			MessageBox(hwnd, TEXT("IRecord Interface can not be obtained"), TEXT("Error"), MB_OK | MB_ICONERROR);
			DestroyWindow(hwnd);
		}

		hInstanceDialog = ((LPCREATESTRUCT)lParam)->hInstance;
		hInstance = GetModuleHandle(NULL);
		hbitSS = LoadBitmap(hInstance, MAKEINTRESOURCE(MYBITMAP));
		if (hbitSS == NULL)
		{
			MessageBox(hwnd, TEXT("Could not load bitmap"), TEXT("Error"), MB_OK | MB_ICONINFORMATION);
			DestroyWindow(hwnd);
		}
		
		/*
		// GetDialogBaseUnits to obtain the width and height of the characters in the default font
		// This is the function that dialog boxes use to obtain text dimension.
		// this is similar with GetTextMetrics
		cxChar = LOWORD(GetDialogBaseUnits()); // width 
		cyChar = HIWORD(GetDialogBaseUnits()); // height

		for (i = 0; i < NUM; i++)   
		{
			hwndButton[i] = CreateWindow(
				TEXT("button"), // this is predefined
				button[i].szText, 
				WS_CHILD | WS_VISIBLE | button[i].iStyle, 
				400, cyChar * (1 + 2 * i), // upper-left corner of this window
				20 * cxChar, // width
				7 * cyChar / 4, // height - A push button looks best when its height is 7/4 times the height of a text character
				hwnd, 
				(HMENU)i, // child window id; it is used in WM_COMMAND to identify messages from this window
				// this should be unique id
				hInstanceDialog,
				NULL
			);
		}*/
		break;
	case WM_SIZE:          
		rect.left = 24 * cxChar;          
		rect.top = 2 * cyChar;          
		rect.right = LOWORD(lParam);          
		rect.bottom = HIWORD(lParam);          
		return 0;
	case WM_DRAWITEM:
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
			case IDM_APP_ABOUT: 
				DialogBox(hInstanceDialog, TEXT("AboutBox"), hwnd, AboutDlgProc);
			break;
			case IDOK: 
			case IDCANCEL: 
				EndDialog(hwnd, 0); 
				break;
		}
		break;
	
	case WM_LBUTTONDOWN: // mouse left click
		
		break;
	case WM_PAINT: // to draw anything
	{
		InvalidateRect(hwnd, &rect, TRUE);
		GetClientRect(hwnd, &rc); // get client area

		// start drawing
		hdc = BeginPaint(hwnd, &ps);
		cdc = CreateCompatibleDC(NULL);
		SelectObject(cdc, hbitSS);
		GetObject(hbitSS, sizeof(BITMAP),&bitSS);
		StretchBlt(
			//destination
			hdc, rc.left, rc.top, rc.right-rc.left, rc.bottom-rc.top, 
			// source
			cdc, 0, 0, bitSS.bmWidth, bitSS.bmHeight, 
			// raster operation
			SRCCOPY);
		SetTextColor(hdc, RGB(0, 0, 0)); // set text color
		//SetBkColor(hdc, RGB(255, 255, 255)); // set background color
		//DrawText(hdc, TEXT("Group: WM_MOVE"), -1, &rc, DT_SINGLELINE | DT_CENTER); // Draw text
		//DrawText(hdc, TEXT("Name: ROHIT SHAMRAO MUNESHWAR"), -1, &rc, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
		//DrawText(hdc, TEXT("Batch: WinRT 2019"), -1, &rc, DT_SINGLELINE| DT_TOP);
		// buttons
		//SelectObject(hdc, GetStockObject(SYSTEM_FIXED_FONT));          
		
		HFONT hFont = CreateFont(40, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Arial");
		SelectObject(hdc, hFont);
		SetBkMode(hdc, TRANSPARENT);
		TextOut(hdc, 50,60, batch, lstrlen(batch));
		TextOut(hdc, 50, 90, groupName, lstrlen(groupName));
		TextOut(hdc, 50, 120, studentName, lstrlen(studentName));
		TextOut(hdc, 500, 320, openerMessage, lstrlen(openerMessage));
		//TextOut(hdc, 24 * cxChar, cyChar, szUnd, lstrlen(szUnd));
		SelectObject(hdc, GetStockObject(SYSTEM_FIXED_FONT));
		DeleteObject(cdc);
		// end painting
		EndPaint(hwnd, &ps);
	}
		break;
	case WM_KEYDOWN: // on any key down
		switch (LOWORD(wParam))
		{
		case VK_ESCAPE: // on escape destroy the window
			DestroyWindow(hwnd);
			break;
		case VK_SPACE: // when spacebar is pressed
			//MessageBox(hwnd, TEXT("space pressed"), TEXT("Message"), MB_OK);
			hDlgShowControls = CreateDialog(hInstanceDialog, 
				TEXT("SHOWCONTROLSBOX"), hwnd, ShowControlsDlgProc);
			break;
		}
		break;
	case WM_DESTROY:
		DestroyWindow(hDlgModeless);
		PostQuitMessage(0);
		break;
	}
	return (DefWindowProc(hwnd, iMsg, wParam, lParam));
}

INT_PTR CALLBACK AboutDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG: return TRUE;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK: case IDCANCEL: EndDialog(hDlg, 0); return TRUE;
		}
		break;
	}
	return FALSE;
}

INT_PTR CALLBACK ShowControlsDlgProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HDC hdc;
	PAINTSTRUCT ps;
	RECT rc;
	static HINSTANCE hInstanceDialog;
	
	switch (message)
	{
	case WM_INITDIALOG: 
		//MessageBox(hwnd, TEXT("Show COntrols"), TEXT("Message"), MB_OK);
		//hInstanceDialog = ((LPCREATESTRUCT)lParam)->hInstance;
		return TRUE;
	case WM_CTLCOLORBTN:
		return (LONG)g_hbrBackground;
	case WM_CTLCOLORDLG:
		return (LONG)g_hbrBackground;
	case WM_CTLCOLORSTATIC:
	{
		HDC hdcStatic = (HDC)wParam;
		SetTextColor(hdcStatic, RGB(255, 255, 255));
		SetBkMode(hdcStatic, TRANSPARENT);
		return (LONG)g_hbrBackground;
	}
	break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case ID_SHOW_RECORDS: // show records
			hDlgViewDetails = CreateDialog(hInstanceDialog, TEXT("ShowRecordsDlg"), hwnd, ShowRecordsDlg);

			InitCommonControls();
			hwndList = CreateWindow(WC_LISTVIEW,
				NULL,
				WS_CHILD | WS_VISIBLE | LVS_REPORT | LVS_SINGLESEL,
				0,
				0,
				850,
				200,
				hDlgViewDetails,
				NULL,//(HMENU)500,
				NULL,
				NULL);
			addFileDataToControls(hDlgViewDetails, hwndList);
			break;

		case ID_ADD_RECORD: // add records
			//MessageBox(hwnd, TEXT("Add Record"), TEXT("Message"), MB_OK | MB_ICONINFORMATION);
			hDlgModeless = CreateDialog(hInstanceDialog, TEXT("ColorScrDlg"), hwnd, ColorScrDlg);
			break;
			
		case IDOK: case IDCANCEL: EndDialog(hwnd, 0); return TRUE;
		}

	}
	return FALSE;
}

INT_PTR CALLBACK ColorScrDlg(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	TCHAR szBufferGlobal[1000];
	TCHAR empIdBuf[100];
	EmployeeData data;
	DWORD dataLen = 0;
	static BOOL isError = FALSE;
	int iCheck = 0;
	switch (message)
	{
	case WM_INITDIALOG: 
		
		
		incrementIndex++; // increment index
		wsprintfW(empIdBuf, TEXT("ASC%d"), incrementIndex);
		// set defaults
		// set emp id
		SetDlgItemText(hDlg, ID_ETEMPID, empIdBuf);
		//SetDlgItemInt(hDlg, ID_ETEMPID,100, TRUE);
		// set dept id
		SetDlgItemText(hDlg, ID_ETDEPTID, TEXT("DPT200"));

		//MessageBox(NULL, TEXT("first arrived dialog"), TEXT("Error"), MB_OK | MB_ICONINFORMATION);
		return TRUE;
	case WM_CTLCOLORDLG:
		return (LONG)g_hbrBackground;
	case WM_CTLCOLORSTATIC:
	{
		HDC hdcStatic = (HDC)wParam;
		SetTextColor(hdcStatic, RGB(255, 255, 255));
		SetBkMode(hdcStatic, TRANSPARENT);
		return (LONG)g_hbrBackground;
	}
	break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK: 
			// get emp id
			GetDlgItemText(hDlg, ID_ETEMPID, data.szBufferEmpId, 20);
			// get dept id
			GetDlgItemText(hDlg, ID_ETDEPTID, data.szBufferDeptId, 20);
			// get first name
			GetDlgItemText(hDlg, ID_ETFIRSTNAME, data.szBufferFirstName, 20);
			if (wcslen(data.szBufferFirstName) == 0)
			{
				MessageBox(hDlg, TEXT("First Name is Mandatory"), TEXT("Error"), MB_ICONERROR | MB_OK);
				return FALSE;
				//isError = TRUE;
			}
			
			// get last name
			GetDlgItemText(hDlg, ID_ETLASTNAME, data.szBufferLastName, 20);
			if (wcslen(data.szBufferLastName) == 0)
			{
				MessageBox(hDlg, TEXT("Last Name is Mandatory"), TEXT("Error"), MB_ICONERROR | MB_OK);
				return FALSE;
				//isError = TRUE;
			}

			// get gender
			//GetDlgItemText(hDlg, ID_ETGENDER, data.szBufferGender, 20);
			iCheck = IsDlgButtonChecked(hDlg, ID_RBMARRIED);
			if (IsDlgButtonChecked(hDlg, ID_RBMARRIED) == 1) // male
			{
				wsprintfW(data.szBufferGender,TEXT("Male"));
			}else if (IsDlgButtonChecked(hDlg, ID_RBUNMARRIED) == 1) // female
			{
				wsprintfW(data.szBufferGender, TEXT("Female"));
			}
			else
			{
				MessageBox(hDlg, TEXT("Gender is Mandatory"), TEXT("Error"), MB_ICONERROR | MB_OK);
				return FALSE;
			}

			// get contact number
			GetDlgItemText(hDlg, ID_ETCONTACT, data.szBufferContactNumber, 20);
			if (wcslen(data.szBufferContactNumber) == 0)
			{
				MessageBox(hDlg, TEXT("Contact Number is Mandatory"), TEXT("Error"), MB_ICONERROR | MB_OK);
				return FALSE;
				//isError = TRUE;
			}

			// get email
			GetDlgItemText(hDlg, ID_ETEMAILADDRESS, data.szBufferEmailAddress, 20);
			if (wcslen(data.szBufferEmailAddress) == 0)
			{
				MessageBox(hDlg, TEXT("Email Address is Mandatory"), TEXT("Error"), MB_ICONERROR | MB_OK);
				return FALSE;
				//isError = TRUE;
			}

			// get dob
			GetDlgItemText(hDlg, ID_ETDOBDAY, data.szBufferDobDay, 20);
			if (wcslen(data.szBufferDobDay) == 0)
			{
				MessageBox(hDlg, TEXT("DOB: Day is Mandatory"), TEXT("Error"), MB_ICONERROR | MB_OK);
				return FALSE;
				//isError = TRUE;
			}
			else if (_wtoi(data.szBufferDobDay) < 1 && _wtoi(data.szBufferDobDay) > 31)
			{
				MessageBox(hDlg, TEXT("DOB: Day must be between 1 and 31"), TEXT("Error"), MB_ICONERROR | MB_OK);
				return FALSE;
				//isError = TRUE;
			}

			GetDlgItemText(hDlg, ID_ETDOBMONTH, data.szBufferDobMonth, 20);
			if (wcslen(data.szBufferDobMonth) == 0)
			{
				MessageBox(hDlg, TEXT("DOB: Month is Mandatory"), TEXT("Error"), MB_ICONERROR | MB_OK);
				return FALSE;
				//isError = TRUE;
			}
			else if (_wtoi(data.szBufferDobMonth) < 1 && _wtoi(data.szBufferDobMonth) > 12)
			{
				MessageBox(hDlg, TEXT("DOB: Month must be between 1 and 12"), TEXT("Error"), MB_ICONERROR | MB_OK);
				return FALSE;
				//isError = TRUE;
			}

			GetDlgItemText(hDlg, ID_ETDOBYEAR, data.szBufferDobYear, 20);
			if (wcslen(data.szBufferDobYear) == 0)
			{
				MessageBox(hDlg, TEXT("DOB: Year is Mandatory"), TEXT("Error"), MB_ICONERROR | MB_OK);
				return FALSE;
				//isError = TRUE;
			}
			else if (_wtoi(data.szBufferDobYear) < 1991)
			{
				MessageBox(hDlg, TEXT("DOB: Year must be greater that 1991."), TEXT("Error"), MB_ICONERROR | MB_OK);
				return FALSE;
				//isError = TRUE;
			}

			// get doj
			GetDlgItemText(hDlg, ID_ETDOJDAY, data.szBufferDojDay, 20);
			if (wcslen(data.szBufferDojDay) == 0)
			{
				MessageBox(hDlg, TEXT("DOJ: Day is Mandatory"), TEXT("Error"), MB_ICONERROR | MB_OK);
				return FALSE;
				//isError = TRUE;
			}
			else if (_wtoi(data.szBufferDojDay) < 1 && _wtoi(data.szBufferDojDay) > 31)
			{
				MessageBox(hDlg, TEXT("DOJ: Day must be between 1 and 31"), TEXT("Error"), MB_ICONERROR | MB_OK);
				return FALSE;
				//isError = TRUE;
			}

			GetDlgItemText(hDlg, ID_ETDOJMONTH, data.szBufferDojMonth, 20);
			if (wcslen(data.szBufferDojMonth) == 0)
			{
				MessageBox(hDlg, TEXT("DOJ: Month is Mandatory"), TEXT("Error"), MB_ICONERROR | MB_OK);
				return FALSE;
				//isError = TRUE;
			}
			else if (_wtoi(data.szBufferDojMonth) < 1 && _wtoi(data.szBufferDojMonth) > 12)
			{
				MessageBox(hDlg, TEXT("DOJ: Month must be between 1 and 12"), TEXT("Error"), MB_ICONERROR | MB_OK);
				return FALSE;
				//isError = TRUE;
			}

			GetDlgItemText(hDlg, ID_ETDOJYEAR, data.szBufferDojYear, 20);

			if (wcslen(data.szBufferDojYear) == 0)
			{
				MessageBox(hDlg, TEXT("DOJ: Year is Mandatory"), TEXT("Error"), MB_ICONERROR | MB_OK);
				return FALSE;
				//isError = TRUE;
			}
			else if(_wtoi(data.szBufferDojYear)<2015)
			{
				MessageBox(hDlg, TEXT("DOJ: Year must be greater that 2015."), TEXT("Error"), MB_ICONERROR | MB_OK);
				return FALSE;
				//isError = TRUE;
			}
			
			/*if (isError)
			{
				isError = FALSE;
				return isError;
			}*/
			// save data
			iRecord->WriteRecord(&data);

			EndDialog(hDlg, 0);
		break;
		case IDCANCEL: 
			EndDialog(hDlg, 0); 
		return TRUE;
		case ID_RBMARRIED:
		case ID_RBUNMARRIED:
			CheckRadioButton(hDlg, ID_RBMARRIED, ID_RBUNMARRIED, LOWORD(wParam));
			break;
		}
		break;
	}
	return FALSE;
}


INT_PTR CALLBACK ShowRecordsDlg(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	// local variables
	//LPDRAWITEMSTRUCT pdis;

	switch (message)
	{
	case WM_INITDIALOG:
		// open records file
		//addFileDataToControls(hDlg, hDlg);
		
		//MessageBox(NULL, TEXT("first arrived dialog"), TEXT("Error"), MB_OK | MB_ICONINFORMATION);
		return TRUE;
	case WM_CTLCOLORDLG:
		return (LONG)g_hbrBackground;
	case WM_CTLCOLORSTATIC:
	{
		HDC hdcStatic = (HDC)wParam;
		SetTextColor(hdcStatic, RGB(255, 255, 255));
		SetBkMode(hdcStatic, TRANSPARENT);
		return (LONG)g_hbrBackground;
	}
	break;
	case WM_DRAWITEM:
		/*pdis = (LPDRAWITEMSTRUCT)lParam;
		// Fill area with white and frame it black
		FillRect(pdis->hDC, & pdis->rcItem, (HBRUSH)GetStockObject(WHITE_BRUSH));
		FrameRect(pdis->hDC, & pdis->rcItem, (HBRUSH)GetStockObject(BLACK_BRUSH));
		*/
		break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			delete [] dataRecords;
			EndDialog(hDlg, 0);
			break;
		case IDCANCEL:
			EndDialog(hDlg, 0);
			return TRUE;
		}
		break;
	}
	return FALSE;
}



void addFileDataToControls(HWND hwnd, HWND hwndList)
{
	LVCOLUMN lvc = { 0 };
	char Text[1024];
	LVITEM   lvi = { 0 };
	dataRecords = new EmployeeData[100];
	printf("----Start----\n");

	ListView_SetExtendedListViewStyle(hwndList, //LVS_EX_CHECKBOXES |
		LVS_EX_FULLROWSELECT |
		LVS_EX_HEADERDRAGDROP |
		LVS_EX_GRIDLINES);


	lvc.mask = LVCF_TEXT | LVCF_SUBITEM | LVCF_WIDTH | LVCF_FMT;
	lvc.fmt = LVCFMT_LEFT;

	/* Add four columns to the list-view (first column contains check box). */
	lvc.iSubItem = 0;
	lvc.cx = 50;
	lvc.pszText = (LPWSTR)TEXT("Emp Id");
	ListView_InsertColumn(hwndList, 0, &lvc);

	lvc.iSubItem = 1;
	lvc.cx = 100;
	lvc.pszText = (LPWSTR)TEXT("Dept Id");
	ListView_InsertColumn(hwndList, 1, &lvc);

	lvc.iSubItem = 2;
	lvc.cx = 70;
	lvc.pszText = (LPWSTR)TEXT("First Name");
	ListView_InsertColumn(hwndList, 2, &lvc);

	lvc.iSubItem = 3;
	lvc.cx = 150;
	lvc.pszText = (LPWSTR)TEXT("Last Name");
	ListView_InsertColumn(hwndList, 3, &lvc);

	lvc.iSubItem = 4;
	lvc.cx = 70;
	lvc.pszText = (LPWSTR)TEXT("Sex");
	ListView_InsertColumn(hwndList, 4, &lvc);

	lvc.iSubItem = 5;
	lvc.cx = 100;
	lvc.pszText = (LPWSTR)TEXT("Contact Number");
	ListView_InsertColumn(hwndList, 5, &lvc);

	lvc.iSubItem = 6;
	lvc.cx = 150;
	lvc.pszText = (LPWSTR)TEXT("Email id");
	ListView_InsertColumn(hwndList, 6, &lvc);

	lvc.iSubItem = 7;
	lvc.cx = 70;
	lvc.pszText = (LPWSTR)TEXT("DOB");
	ListView_InsertColumn(hwndList, 7, &lvc);

	lvc.iSubItem = 8;
	lvc.cx = 70;
	lvc.pszText = (LPWSTR)TEXT("DOJ");
	ListView_InsertColumn(hwndList, 8, &lvc);
	int k = 0;
	
	// data
	// read data
	TCHAR t[100];
	wsprintf(t, L"%d", size);
	iRecord->ReadRecords(dataRecords, &size);
	for(int i=0;i<size; i++)
	{
		EmployeeData data = dataRecords[i];
			lvi.iItem = k;
			ListView_InsertItem(hwndList, &lvi);
			ListView_SetItemText(hwndList, k, 0, data.szBufferEmpId);
			

			// dept id
			//SetDlgItemText(hDlg, ID_ETDEPTID, token);
			ListView_SetItemText(hwndList, k, 1, data.szBufferDeptId);

			// first name
			//(hDlg, ID_ETFIRSTNAME, token);
			ListView_SetItemText(hwndList, k, 2, data.szBufferFirstName);

			// last name
			//SetDlgItemText(hDlg, ID_ETLASTNAME, token);
			ListView_SetItemText(hwndList, k, 3, data.szBufferLastName);
			// sex
			//SetDlgItemText(hDlg, ID_ETGENDER, token);
			ListView_SetItemText(hwndList, k, 4, data.szBufferGender);
			// contact number
			//SetDlgItemText(hDlg, ID_ETCONTACT, token);
			ListView_SetItemText(hwndList, k, 5, data.szBufferContactNumber);
			// email
			//SetDlgItemText(hDlg, ID_ETEMAILADDRESS, token);
			ListView_SetItemText(hwndList, k, 6, data.szBufferEmailAddress);

			// manage dates
			wchar_t* day;
			wchar_t* month;
			wchar_t* year;

			// dob - day
			//SetDlgItemText(hDlg, ID_ETDOBDAY, token);
			
			// dob - month
			//SetDlgItemText(hDlg, ID_ETDOBMONTH, token);
			// dob - year
			//SetDlgItemText(hDlg, ID_ETDOBYEAR, token);

			//wchar_t* result = const_cast<wchar_t*>(L"");
			WCHAR dst[100];
			swprintf_s(dst, 100, L"%s/%s/%s", data.szBufferDobDay, data.szBufferDobMonth, data.szBufferDobYear);
			ListView_SetItemText(hwndList, k, 7, (LPWSTR)dst);

			// doj - day
			//SetDlgItemText(hDlg, ID_ETDOJDAY, token);
			//ListView_SetItemText(hwndList, k, 8, (LPWSTR)token);
			// doj - month
			//SetDlgItemText(hDlg, ID_ETDOJMONTH, token);
			// doj - year
			//SetDlgItemText(hDlg, ID_ETDOJYEAR, token);
			//MessageBox(hwnd, token, TEXT("Message"), MB_OK | MB_ICONINFORMATION);
			swprintf_s(dst, 100, L"%s/%s/%s", data.szBufferDojDay, data.szBufferDojMonth, data.szBufferDojYear);
			ListView_SetItemText(hwndList, k, 8, (LPWSTR)dst);
	}
	printf("\n----End----\n");
}

void addFileDataToControls_BKP(HWND hwnd, HWND hDlg)
{
	char c;

	char temp[100];
	int i = 0;
	int j = 0;
	FILE* fPtr;
	char cFileName[] = "emp_records.txt";
	fopen_s(&fPtr, cFileName, "r");
	if (fPtr == NULL)
	{
		printf("Error while opening the file\n");
		exit(0);
	}
	c = fgetc(fPtr); // read's first character
	printf("----Start----\n");
	while (c != EOF)
	{
		if (i % 2 == 0) // even
		{
			temp[j++] = c;
		}
		i++;
		//printf("%c|", c);
		c = fgetc(fPtr);


		if (c == '\n')
		{
			temp[j] = '\0';
			//printf("%s", temp);

			// convert to LPWSTR
			wchar_t wtext[100];
			size_t count;
			mbstowcs_s(&count, wtext, temp, strlen(temp) + 1);//Plus null
			LPWSTR ptr = wtext;

			//MessageBox(hwnd, ptr, TEXT("Message"), MB_OK | MB_ICONINFORMATION);

			// split by | deliminator
			wchar_t* buffer;
			// emp id
			wchar_t* token = wcstok_s(wtext, L"|", &buffer);
			SetDlgItemText(hDlg, ID_ETEMPID, token);
			// dept id
			token = wcstok_s(NULL, L"|", &buffer);
			SetDlgItemText(hDlg, ID_ETDEPTID, token);
			// first name
			token = wcstok_s(NULL, L"|", &buffer);
			SetDlgItemText(hDlg, ID_ETFIRSTNAME, token);
			// last name
			token = wcstok_s(NULL, L"|", &buffer);
			SetDlgItemText(hDlg, ID_ETLASTNAME, token);
			// sex
			token = wcstok_s(NULL, L"|", &buffer);
			SetDlgItemText(hDlg, ID_ETGENDER, token);
			// contact number
			token = wcstok_s(NULL, L"|", &buffer);
			SetDlgItemText(hDlg, ID_ETCONTACT, token);
			// email
			token = wcstok_s(NULL, L"|", &buffer);
			SetDlgItemText(hDlg, ID_ETEMAILADDRESS, token);
			// dob - day
			token = wcstok_s(NULL, L"|", &buffer);
			SetDlgItemText(hDlg, ID_ETDOBDAY, token);
			// dob - month
			token = wcstok_s(NULL, L"|", &buffer);
			SetDlgItemText(hDlg, ID_ETDOBMONTH, token);
			// dob - year
			token = wcstok_s(NULL, L"|", &buffer);
			SetDlgItemText(hDlg, ID_ETDOBYEAR, token);
			// doj - day
			token = wcstok_s(NULL, L"|", &buffer);
			SetDlgItemText(hDlg, ID_ETDOJDAY, token);
			// doj - month
			token = wcstok_s(NULL, L"|", &buffer);
			SetDlgItemText(hDlg, ID_ETDOJMONTH, token);
			// doj - year
			token = wcstok_s(NULL, L"|", &buffer);
			SetDlgItemText(hDlg, ID_ETDOJYEAR, token);
			//MessageBox(hwnd, token, TEXT("Message"), MB_OK | MB_ICONINFORMATION);
			j = 0;
		}
	}

	if (fPtr)
	{
		fclose(fPtr);
	}
	printf("\n----End----\n");
}

int Create2ColItem(HWND hwndList, char* Text1, char* Text2)
{
	/*LVITEM lvi = { 0 };
	int Ret;

	// Initialize LVITEM members that are common to all items. 
	lvi.mask = LVIF_TEXT;
	lvi.pszText = (LPWSTR)Text1;
	Ret = ListView_InsertItem(hwndList, &lvi);
	if (Ret >= 0) {
		ListView_SetItemText(hwndList, Ret, 1, (LPWSTR)Text2);
	}
	return Ret;*/
	LVCOLUMN lvcDeptId;
	lvcDeptId.mask = LVCF_TEXT;
	lvcDeptId.iSubItem = 1;
	lvcDeptId.pszText = (LPWSTR)Text1;
	lvcDeptId.cx = 100; // width
	lvcDeptId.fmt = LVCFMT_LEFT;
	//lvcDeptId.
	int ret = ListView_InsertColumn(hwndList, 1, &lvcDeptId);
	ListView_SetItemText(hwndList, ret, 1, (LPWSTR)Text2);
	return ret;
}
