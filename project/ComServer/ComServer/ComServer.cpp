#include"ComServer.h"

// class declarations
class CRecordIO : public IRecord
{
private:
	long m_cRef;
	HANDLE hFile; // file handle
	
	void OpenRecordsFile(void);
public:
	// constructor method declarations
	CRecordIO(void);
	// destructor method declarations
	~CRecordIO(void);

	// IUnknown specific method declarations (inherited)
	HRESULT __stdcall QueryInterface(REFIID, void**);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);

	// IRecord specific method declarations (inherited)
	HRESULT __stdcall WriteRecord(EmployeeData*);
	HRESULT __stdcall ReadRecords(EmployeeData*, int*);
};

class CRecordIOClassFactory :public IClassFactory
{
private:
	long m_cRef;
public:
	// constructor method declarations
	CRecordIOClassFactory(void);

	// destructor method declarations
	~CRecordIOClassFactory(void);

	// IUnknown specific method declarations (inherited)
	HRESULT __stdcall QueryInterface(REFIID, void**);
	ULONG __stdcall AddRef(void);
	ULONG __stdcall Release(void);

	// IClassFactory specific method declarations (inherited)
	HRESULT __stdcall CreateInstance(IUnknown*, REFIID, void**);
	HRESULT __stdcall LockServer(BOOL);
};

// global variable declarations
long glNumberOfActiveComponents = 0; // number of active components
long glNumberOfServerLocks = 0; // number of locks on this dll

// DllMain
BOOL APIENTRY DllMain(HMODULE hModule,
	DWORD  ul_reason_for_call,
	LPVOID lpReserved
)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	}
	return(TRUE);
}

// Implementation of CRecordIO's Constructor method
CRecordIO::CRecordIO(void)
{
	// hardcoded initialization to anticipate possible failure of QueryInterface()
	m_cRef = 1;
	
	InterlockedIncrement(&glNumberOfActiveComponents); // increment global counter
	
}

// Implementation of CRecordIO's destructor method
CRecordIO::~CRecordIO(void)
{
	
	InterlockedDecrement(&glNumberOfActiveComponents); // decrement global counter
}

// Implementation of CRecordIO's IUnknown's methods
HRESULT CRecordIO::QueryInterface(REFIID riid, void** ppv)
{
	if (riid == IID_IUnknown)
	{
		*ppv = static_cast<IRecord*>(this);
	}
	else if (riid == IID_IRecord)
	{
		*ppv = static_cast<IRecord*>(this);
	}
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}

	reinterpret_cast<IUnknown*>(*ppv)->AddRef();
	return(S_OK);
}

ULONG CRecordIO::AddRef(void)
{
	InterlockedIncrement(&m_cRef); // increment reference count for this object
	return(m_cRef);
}

ULONG CRecordIO::Release(void)
{
	InterlockedDecrement(&m_cRef); // decrement reference count for this object

	// if reference count is going to be zero now then delete this object and return
	if (m_cRef == 0)
	{
		delete(this);
		return(0);
	}

	return(0);
}

void CRecordIO::OpenRecordsFile(void)
{
	TCHAR fileName[] = L"emp_records.txt";
	hFile = CreateFile(
		fileName, // name of the file
		FILE_APPEND_DATA, // open for writing
		0, // do not share
		NULL, // default security
		OPEN_EXISTING, // create new file only
		FILE_ATTRIBUTE_NORMAL,// normal file
		NULL // no attribute template
	);

	if (hFile == INVALID_HANDLE_VALUE)
	{
		// opens already existing file
		hFile = CreateFile(
			fileName, // name of the file
			FILE_APPEND_DATA, // open for writing
			0, // do not share
			NULL, // default security
			OPEN_EXISTING, // create new file only
			FILE_ATTRIBUTE_NORMAL,// normal file
			NULL // no attribute template
		);

		if (hFile == INVALID_HANDLE_VALUE)
		{
			MessageBox(NULL, TEXT("Can not open file for writing"), TEXT("ERROR"), MB_OK);
			return;
		}
	}

	if (hFile)
	{
		//MessageBox(NULL, TEXT("File Opened"), TEXT("Message"), MB_OK);
	}
}

// Implementation of IRecord methods
HRESULT CRecordIO::WriteRecord(EmployeeData* data)
{
	// local variables
	TCHAR szBufferGlobal[1000];
	DWORD dataLen = 0;
	BOOL bErrorFlage = false;
	DWORD dwBytesWritten = 0;
	OpenRecordsFile(); // open records file for writing
	
	// code
	wsprintf(szBufferGlobal, TEXT("%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n"),
		data->szBufferEmpId, data->szBufferDeptId, // ids 
		data->szBufferFirstName, data->szBufferLastName,  // name
		data->szBufferGender, // gender
		data->szBufferContactNumber, data->szBufferEmailAddress, // contact info
		data->szBufferDobDay, data->szBufferDobMonth, data->szBufferDobYear, // date of birth
		data->szBufferDojDay, data->szBufferDojMonth, data->szBufferDojYear // date of joining
	);

	// show data and write to the file
	//MessageBox(NULL, szBufferGlobal, TEXT("Error"), MB_OK | MB_ICONINFORMATION);

	dataLen = (DWORD)(lstrlenW(szBufferGlobal));

	bErrorFlage = WriteFile(
		hFile, // open file handle
		szBufferGlobal, // start of data to write
		dataLen * 2,// number of bytes to write
		&dwBytesWritten, // number of bytes that were written
		NULL // no overlapped structure
	);

	if (bErrorFlage == FALSE)
	{
		MessageBox(NULL, TEXT("Unable to write to a file."), TEXT("Error"), MB_OK);
		return S_OK;
	}
	else
	{
		MessageBox(NULL, TEXT("Data written successfully to a file"), TEXT("Message"), MB_OK);
	}
	if (hFile)
	{
		CloseHandle(hFile);
	}
	return S_OK;
}

HRESULT CRecordIO::ReadRecords(EmployeeData* recordsData, int* size)
{
	FILE* fPtr;
	char cFileName[] = "emp_records.txt";
	char c;
	int i = 0;
	char temp[100];
	int j = 0;
	int k = 0;
	int lines = 0;
	
	fopen_s(&fPtr, cFileName, "r");
	if (fPtr == NULL)
	{
		printf("Error while opening the file\n");
		exit(0);
	}
	c = fgetc(fPtr); // read's first character

	while (c != EOF)
	{
		if (i % 2 == 0) // even
		{
			temp[j++] = c;
		}
		i++;
		//printf("%c|", c);
		c = fgetc(fPtr);


		if (c == '\n')
		{
			temp[j] = '\0';
			//printf("%s", temp);

			// convert to LPWSTR
			// EmployeeData empData;
			wchar_t wtext[100];
			size_t count;
			mbstowcs_s(&count, wtext, temp, strlen(temp) + 1);//Plus null
			LPWSTR ptr = wtext;
			EmployeeData data;
			//MessageBox(NULL, TEXT("Reading started"), TEXT("Message"), MB_OK);
			//MessageBox(NULL, wtext, TEXT("Message"), MB_OK);
			// split by | deliminator
			wchar_t* buffer;
			// emp id
			wchar_t* token = wcstok_s(wtext, L"|", &buffer);
			//data.szBufferEmpId = token;
			lstrcpyW(data.szBufferEmpId , token);
			
			// dept id
			token = wcstok_s(NULL, L"|", &buffer);
			lstrcpyW(data.szBufferDeptId, token);
			// first name
			token = wcstok_s(NULL, L"|", &buffer);
			lstrcpyW(data.szBufferFirstName, token);
			// last name
			token = wcstok_s(NULL, L"|", &buffer);
			lstrcpyW(data.szBufferLastName, token);
			// sex
			token = wcstok_s(NULL, L"|", &buffer);
			lstrcpyW(data.szBufferGender, token);
			// contact number
			token = wcstok_s(NULL, L"|", &buffer);
			lstrcpyW(data.szBufferContactNumber, token);
			// email
			token = wcstok_s(NULL, L"|", &buffer);
			lstrcpyW(data.szBufferEmailAddress, token);

			// manage dates
			wchar_t* day;
			wchar_t* month;
			wchar_t* year;

			// dob - day
			day = wcstok_s(NULL, L"|", &buffer);
			// dob - month
			month = wcstok_s(NULL, L"|", &buffer);
			// dob - year
			year = wcstok_s(NULL, L"|", &buffer);
			lstrcpyW(data.szBufferDobDay, day);
			lstrcpyW(data.szBufferDobMonth, month);
			lstrcpyW(data.szBufferDobYear, year);

		
			// doj - day
			day = wcstok_s(NULL, L"|", &buffer);
			// doj - month
			month = wcstok_s(NULL, L"|", &buffer);
			
			// doj - year
			year = wcstok_s(NULL, L"|", &buffer);
			
			//MessageBox(hwnd, token, TEXT("Message"), MB_OK | MB_ICONINFORMATION);
			lstrcpyW(data.szBufferDojDay, day);
			lstrcpyW(data.szBufferDojMonth, month);
			lstrcpyW(data.szBufferDojYear, year);
			
			j = 0;
			k++;
			//MessageBox(NULL, TEXT("Reading Ends"), TEXT("Message"), MB_OK);
			if(lines<10)
				recordsData[lines] = data;

			lines++;
		}
		k = 0;
	}

	*size = lines;
	if (fPtr)
	{
		fclose(fPtr);
	}
}
// implementation of CSumSubtractClassFactory constructor
CRecordIOClassFactory::CRecordIOClassFactory(void)
{
	m_cRef = 1; // hardcoded initialization to anticipate possible failure of QueryInterface
}

// implementation of CSumSubtractClassFactory destructor
CRecordIOClassFactory::~CRecordIOClassFactory(void)
{
	// no code
}


// Implementation of CSumSubtractClassFactory's IClassFactory's IUnknown's methods
HRESULT CRecordIOClassFactory::QueryInterface(REFIID riid, void** ppv)
{
	if (riid == IID_IUnknown)
	{
		*ppv = static_cast<IClassFactory*>(this);
	}
	else if (riid == IID_IClassFactory)
	{
		*ppv = static_cast<IClassFactory*>(this);
	}
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}

	reinterpret_cast<IUnknown*>(*ppv)->AddRef(); // increment reference count
	return(S_OK);
}

ULONG CRecordIOClassFactory::AddRef(void)
{
	InterlockedIncrement(&m_cRef);
	return(m_cRef);
}

ULONG CRecordIOClassFactory::Release(void)
{
	InterlockedDecrement(&m_cRef);

	// if reference count is 0 then delete this object
	if (m_cRef == 0)
	{
		delete(this);
		return(0);
	}
	return(m_cRef);
}


// implementation of CSumSubtractClassFactory's IClassFactory's methods
HRESULT CRecordIOClassFactory::CreateInstance(IUnknown* pUnkOuter, REFIID riid, void** ppv)
{
	// variable declarations
	CRecordIO* pCRecordIO = NULL;
	HRESULT hr;

	// code
	if (pUnkOuter != NULL)
	{
		return(CLASS_E_NOAGGREGATION);
	}

	pCRecordIO = new CRecordIO;

	if (pCRecordIO == NULL)
	{
		return(E_OUTOFMEMORY);
	}

	// get the requested interface
	hr = pCRecordIO->QueryInterface(riid, ppv);

	pCRecordIO->Release(); // anticipate possible failure of QueryInterface()
	return(hr);
}

HRESULT CRecordIOClassFactory::LockServer(BOOL fLock)
{
	if (fLock)
	{
		InterlockedIncrement(&glNumberOfServerLocks);
	}
	else
	{
		InterlockedDecrement(&glNumberOfServerLocks);
	}
	return(S_OK);
}


// exported functions - windows 8 and above, extern c is must
extern "C" HRESULT __stdcall DllGetClassObject(REFCLSID rclsid, REFIID riid, void** ppv)
{
	// variable declarations
	CRecordIOClassFactory* pCRecordIOClassFactory = NULL;
	HRESULT hr;

	// code
	if (rclsid != CLSID_RecordIO)
	{
		return(CLASS_E_CLASSNOTAVAILABLE);
	}

	// create class factory
	pCRecordIOClassFactory = new CRecordIOClassFactory;

	if (pCRecordIOClassFactory == NULL)
	{
		return(E_OUTOFMEMORY);
	}

	hr = pCRecordIOClassFactory->QueryInterface(riid, ppv);
	pCRecordIOClassFactory->Release(); // anticipate possible failure of QueryInterface()
	return(hr);
}

extern "C" HRESULT __stdcall DllCanUnloadNow(void)
{
	if ((glNumberOfActiveComponents == 0) && (glNumberOfServerLocks == 0))
	{
		return(S_OK);
	}
	else
	{
		return(S_FALSE);
	}
}
